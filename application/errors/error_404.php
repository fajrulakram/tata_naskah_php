<!DOCTYPE html>
<html>
<head>
  	<title>Page Not Found</title>
	<!-- BEGIN STYLESHEETS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/bootstrap.css@1401441895.css' rel='stylesheet' type='text/css'/>		
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox_responsive.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/font-awesome.min.css@1401441895.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/jquery-ui/jquery-ui-boostbox.css@1401442122.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/fullcalendar/fullcalendar.css@1401442122.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/jquery.dataTables.css@1401442112.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/TableTools.css@1401442112.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/nestable/nestable.css' rel='stylesheet' type='text/css'/>		
    <link href='<?php echo base_url() ?>assets/stylesheets/libs/blueimp-file-upload/jquery.fileupload.css@1401442110.css' 
          rel='stylesheet' type='text/css'/>	
    <link href='<?php echo base_url() ?>assets/stylesheets/userdetail.css' rel='stylesheet' type='text/css'/>	
	<!-- END STYLESHEETS -->	  	
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-1.11.0.min.js"></script>  	
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>  	
</head>
<!-- START 404 MESSAGE -->
<body class="body-striped">
    <section>
        <div class="section-header">
            <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 404 page <small>Broken link</small></h3>
        </div>
        <div class="section-body contain-lg">
            <div class="row">			
                <div class="col-lg-12 text-center">
                    <h1 class="text-huge">404 <i class="fa fa-search-minus text-primary"></i></h1>
                    <h2 class="text-light">This page does not exist</h2>
                </div><!--end .col-lg-12 -->
            </div><!--end .row -->
        </div><!--end .section-body -->
    </section>
    <!-- END 404 MESSAGE -->

</body>