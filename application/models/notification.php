<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class notification extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
        
        public function add($asal,$tujuan,$pesan,$url){
            $this->db->query("INSERT INTO notifications(asal,tujuan,pesan,url) VALUES($asal,$tujuan,'$pesan','$url')");            
        }
        
        public function get($id){
            $query = $this->db->query("SELECT n.*,u.nama_lengkap,u.avatar FROM 
                                        notifications AS n
                                        LEFT JOIN users as u ON n.asal=u.id
                                        WHERE status=0 and n.tujuan=$id
                                        ORDER BY n.created_at DESC
                                        LIMIT 3");
            return $query->result_array();
        }
        
        public function get_all($id){
            $query = $this->db->query("SELECT n.*,u.nama_lengkap,u.avatar FROM 
                                        notifications AS n
                                        LEFT JOIN users as u ON n.asal=u.id   
                                        WHERE n.tujuan=$id
                                        ORDER BY n.created_at DESC");
            return $query->result_array();
        }
        
        public function view($id,$tujuan){
            $this->db->query("UPDATE notifications SET status=1 WHERE id=$id");
            $query = $this->db->query("SELECT * FROM notifications WHERE id=$id");
            $result = $query->result_array();
            if(sizeof($result)>0){
                return $result[0];
            } else {
                return null;
            }
        }
    }
?>