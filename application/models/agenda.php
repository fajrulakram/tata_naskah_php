<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Agenda extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
		
        public function insert($data){
            $this->db->insert("agendas", $data);
            return $this->db->insert_id();
        }
        
        public function delete($id){
            $this->db->where('id_agenda', $id);
            $this->db->delete("agendas");
            //return $this->db->insert_id();
        }
        
        public function edit($id, $data){
            $this->db->where("id", $id);
            $this->db->update("agendas", $data);
            
            return $this->db->affected_rows();
        }
        
        public function getbytanggal($start, $end, $tipe){
            $this->db->where("start", $start);
            $this->db->where("end", $end);
            $this->db->where("tipe", $tipe);
            
            $data=$this->db->get("agendas");
            return $data->row();
        }
        
        public function get(){
            $hasil=$this->db->get("agendas");
            return $hasil->result_array();
        }
        
        public function get_all(){
            $query = $this->db->query("SELECT * FROM agendas ORDER BY start DESC");
            return $query->result_array();
        }
        
        public function get_this_week(){
            date_default_timezone_set('Asia/Jakarta');
            $date = date_create(date('Y-m-d'));
            $d1 = $date->format('Y-m-d');
            $week = $date;
            date_add($week,date_interval_create_from_date_string("7 days"));
            $d2 = $week->format('Y-m-d');
            $query = $this->db->query("SELECT *
                                        FROM agendas 
                                        WHERE start BETWEEN '$d1' AND '$d2'
                                        ORDER BY start ASC");
            return $query->result_array();
        }
        
         public function get_this_month(){
            date_default_timezone_set('Asia/Jakarta');
            $date =date('Y-m');            
            $query = $this->db->query("SELECT *
                                        FROM agendas 
                                        WHERE start LIKE '$date%'
                                        ORDER BY start ASC");
            return $query->result_array();
        }
        
        public function get_detail($id){
            $query = $this->db->query("SELECT *
                                        FROM agendas 
                                        WHERE id_agenda=$id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return $rs[0];
            } else {
                return null;
            }
        }
        
        public function get_user_agenda($id){
            $query = $this->db->query("SELECT u.*
                                        FROM agenda_users AS au
                                        LEFT JOIN users AS u ON au.user_id=u.id
                                        WHERE au.agenda_id=$id");
            return $query->result_array();
        }
        
        public function check_overlap($user, $awal, $akhir){
            $query = $this->db->query("SELECT au.id
                                        FROM agenda_users AS au
                                        LEFT JOIN agendas as ag on au.agenda_id = ag.id_agenda
                                        WHERE au.user_id=$user AND 
                                            (((ag.start BETWEEN '$awal' AND '$akhir') OR (ag.end BETWEEN '$awal' AND '$akhir')) OR 
                                            (('$awal' BETWEEN ag.start AND ag.end) OR ('$akhir' BETWEEN ag.start AND ag.end )))");
            $rs = $query->result_array();
            return sizeof($rs)>0;
        }
        
        public function check_overlap_edit($user, $awal, $akhir, $id){
            $query = $this->db->query("SELECT au.id
                                        FROM agenda_users AS au
                                        LEFT JOIN agendas as ag on au.agenda_id = ag.id_agenda
                                        WHERE au.user_id=$user AND id_agenda <> $id AND 
                                            (((ag.start BETWEEN '$awal' AND '$akhir') OR (ag.end BETWEEN '$awal' AND '$akhir')) OR 
                                            (('$awal' BETWEEN ag.start AND ag.end) OR ('$akhir' BETWEEN ag.start AND ag.end )))");
            $rs = $query->result_array();
            return sizeof($rs)>0;
        }
        
        public function check_overlap_presentasi($awal,$akhir){
            $query = $this->db->query("SELECT ag.id_agenda
                                        FROM agendas as ag
                                        WHERE tipe='presentasi' AND 
                                        (((ag.start BETWEEN '$awal' AND '$akhir') OR (ag.end BETWEEN '$awal' AND '$akhir')) OR 
                                            (('$awal' BETWEEN ag.start AND ag.end) OR ('$akhir' BETWEEN ag.start AND ag.end )))");
            $rs = $query->result_array();
            return sizeof($rs)>0;
        }
        
        public function check_overlap_presentasi_edit($awal,$akhir, $id){
            $query = $this->db->query("SELECT ag.id_agenda
                                        FROM agendas as ag
                                        WHERE tipe='presentasi' AND id_agenda <> $id AND 
                                        (((ag.start BETWEEN '$awal' AND '$akhir') OR (ag.end BETWEEN '$awal' AND '$akhir')) OR 
                                            (('$awal' BETWEEN ag.start AND ag.end) OR ('$akhir' BETWEEN ag.start AND ag.end )))");
            $rs = $query->result_array();
            return sizeof($rs)>0;
        }
        
        public function check_count_presentasi($awal){
             $query = $this->db->query("SELECT ag.id_agenda
                                        FROM agendas as ag
                                        WHERE tipe='presentasi' AND ag.start LIKE '%$awal%'");
            $rs = $query->result_array();
            return sizeof($rs)>=3;
        }
        
        public function insert_pelaksana($id_agenda,$id_user){
            $this->db->query("INSERT INTO agenda_users (agenda_id, user_id) VALUES($id_agenda,$id_user)");
        }   
        
        public function delete_pelaksana($id_agenda){
            $this->db->where("agenda_id", $id_agenda);
            $this->db->delete("agenda_users");
        }
        
        public function get_jadwal_user($id){
            $query = $this->db->query("SELECT ag.*
                                        FROM agendas as ag
                                        LEFT JOIN agenda_users AS au ON au.agenda_id=ag.id_agenda
                                        WHERE au.user_id=$id
                                        GROUP BY ag.id_agenda");
            return $query->result_array();
        }
        
	}
?>