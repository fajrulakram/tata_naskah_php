<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Folder extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}                
					
		public function get($id_user){
			$query = $this->db->query("select * from folders join folder_users on folder_id = folders.id where user_id = $id_user ");
			return $query->result_array();
		}
        
        public function detail($id){
            $query = $this->db->query("SELECT f.*,count(bf.id) as jumlah_file
                                        FROM folders as f
                                        LEFT JOIN berkasfiles as bf on bf.folder_id=f.id
                                        WHERE f.id=$id");
            $hasil = $query->result_array();
            if(sizeof($hasil)>0){
                return $hasil[0];
            } else {
                return null;
            }
        }
        
        public function is_user($folder,$user){
            $query = $this->db->query("SELECT * FROM folder_users WHERE user_id=$user AND folder_id=$folder");
            $hasil = $query->result_array();
            return sizeof($hasil)>0;
        }
    
        public function getfolderbyid($id_folder){
           $query = $this->db->query("select folders.* from folders where id = $id_folder");
           return $query->row();
        }
        
        public function get_all(){
            $query = $this->db->query("select f.*, (SELECT f1.nama_folder FROM folders as f1 WHERE f.parent_id=f1.id) as parent 
                                            from folders as f");	
			return $query->result_array();
        }
        
        public function get_limit(){
            $query = $this->db->query("select f.*, (SELECT f1.nama_folder FROM folders as f1 WHERE f.parent_id=f1.id) as parent 
                                            from folders as f 
                                            order by f.created_at desc
                                            LIMIT 5");	
			return $query->result_array();
        }
        
        public function get_user_folder($id) {
            $query = $this->db->query("SELECT u.* FROM folder_users as fu
                                            LEFT JOIN users as u ON fu.user_id=u.id
                                            WHERE fu.folder_id=$id");
            return $query->result_array();
        }
        
        public function get_all_root(){
            $query = $this->db->query("select distinct folders.* from 
                                        folders 
                                        join folder_users on folder_id = folders.id 
                                        where parent_id IS NULL OR parent_id=0");	
			return $query->result_array();
        }
    
        public function getroot($id_user){
			$query = $this->db->query("select folders.*,(SELECT count(id) FROM folders as f1 WHERE parent_id=folders.id)  as sub
                                        from folders 
                                        join folder_users on folder_id = folders.id 
                                        where user_id = $id_user AND (parent_id IS NULL  OR parent_id=0)
                                        ORDER BY sub DESC, nama_folder ASC");										
			return $query->result_array();
		}
    
        public function getchild($id_user, $id_root){
			$query = $this->db->query("select folders.* from folders join folder_users on folder_id = folders.id 
                                        where user_id = $id_user AND parent_id = $id_root
                                        ORDER BY nama_folder ASC");										
			return $query->result_array();
		}
        
        public function get_metas($id){
            $query = $this->db->query("SELECT * FROM folder_metas WHERE folder_id=$id ORDER BY position ASC");
            return $query->result_array();
        }
    
        public function create($nama,$parent,$hak_akses) {
            $this->db->query("INSERT INTO folders(nama_folder,parent_id,hak_akses)
                                VALUES('$nama',$parent,'$hak_akses')");
            return $this->db->insert_id();
        }
        
        public function set_meta($id,$name,$type,$position=null){
            if($position!=null){
                $this->db->query("INSERT INTO folder_metas(folder_id,nama,type,position) VALUES($id,'$name','$type',$position)");
            } else {
                $this->db->query("INSERT INTO folder_metas(folder_id,nama,type) VALUES($id,'$name','$type')");
            }
        }
        
        public function update($id,$nama,$parent,$hak_akses){
            $this->db->query("UPDATE folders SET nama_folder='$nama',parent_id=$parent,
                                hak_akses='$hak_akses' WHERE id=$id");
        }
        
        public function update_meta($id,$name,$type,$position=NULL){
            if($position != null) {
                $this->db->query("UPDATE folder_metas SET nama='$name', type='$type', position=$position WHERE id=$id");    
            } else {
                $this->db->query("UPDATE folder_metas SET nama='$name', type='$type' WHERE id=$id");
            }
        }
        
        public function delete_meta($id){
            //delete meta folder
            $this->db->query("DELETE FROM folder_metas WHERE id=$id");
            //delete meta file
            $this->db->query("DELETE FROM file_metas WHERE meta_id=$id");
        }
        
        public function set_akses_folder($id_folder,$id_user) {
            $this->db->query("INSERT INTO folder_users(user_id,folder_id) VALUES($id_user,$id_folder)");
        }
        
        public function set_akses_parent($id_folder,$id_user) {
            $q = $this->db->query("SELECT * FROM folder_users WHERE user_id=$id_user AND folder_id=$id_folder");
            if(sizeof($q->result_array())>0) {
                //do nothing
            } else {
                $this->db->query("INSERT INTO folder_users(user_id,folder_id) VALUES($id_user,$id_folder)");
            }
        }
        
        public function remove_akses_folder($id){
            $this->db->query("DELETE FROM folder_users WHERE folder_id=$id");
        }
        
        public function delete($id){            
            $this->db->query("DELETE FROM folders WHERE id=$id");
        }
        
        public function remove_all_meta($id){
            $this->db->query("DELETE FROM folder_metas WHERE folder_id=$id");
        }
    
	}
?>