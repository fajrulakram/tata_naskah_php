<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Integrasi extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
		
        public function get_badan_usaha(){
            $query = $this->db->query("SELECT id_summary,nama_badan_usaha,alamat_badan_usaha,no_izin_usaha,izin_usaha_djm,tgl_pengajuan 
                                        FROM summary_perizinan
                                        ORDER BY id_summary DESC");
            return $query->result_array();
        }
        
        public function get_all(){
            $query = $this->db->query("SELECT * FROM summary_perizinan");
            return $query->result_array();
        }
        
        public function get_id($id){
            $query = $this->db->query("SELECT * FROM summary_perizinan
                                        WHERE id_summary=$id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return $rs[0];
            } else {
                return null;
            }
        }
        
        public function add_revisi($id,$masuk,$keluar,$ket){
            $this->db->query("INSERT INTO revisi_perizinan(summary_id,tanggal_masuk,tanggal_keluar,keterangan)
                                VALUES($id,'$masuk','$keluar','$ket')");
        }
        
        public function get_revisi($id){
            $query = $this->db->query("SELECT * FROM revisi_perizinan WHERE summary_id=$id ORDER BY tanggal_masuk ASC");
            return $query->result_array();
        }
        
        public function detail_revisi($id){
            $query = $this->db->query("SELECT * FROM revisi_perizinan WHERE id=$id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return $rs[0];
            } else {
                return null;
            }
        }
        
        public function edit_revisi($id,$tgl_masuk,$tgl_keluar,$keterangan){
            $this->db->query("UPDATE revisi_perizinan SET tanggal_masuk='$tgl_masuk', tanggal_keluar='$tgl_keluar',
                                        keterangan='$keterangan' WHERE id=$id");
        }
        
        public function delete_revisi($id){
            $this->db->query("DELETE FROM revisi_perizinan WHERE id=$id");
        }
    }
?>