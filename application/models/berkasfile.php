<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Berkasfile extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
					
		public function get($id_folder){
			$query = $this->db->query("select berkasfiles.*, (select u2.nama_lengkap from users as u2 WHERE u2.id=berkasfiles.user_id) as uploader 
                                            from berkasfiles join file_users on berkasfile_id = berkasfiles.id 
                                            where folder_id = $id_folder");            						
			return $query->result_array();
		}
    
        public function getfilefolder($id_user, $id_folder){
            $query = $this->db->query("select berkasfiles.*, (select u2.nama_lengkap from users as u2 WHERE u2.id=berkasfiles.user_id) as uploader 
                                            from berkasfiles
                                            where folder_id = $id_folder");
            return $query->result();
        }
        
        public function get_meta_data($id) {
            $query = $this->db->query("SELECT fl.*,fd.folder_id,fd.nama,fd.type 
                                        FROM file_metas as fl
                                        LEFT JOIN folder_metas as fd ON fl.meta_id = fd.id
                                        WHERE file_id=$id
                                        ORDER BY fd.position ASC");
            return $query->result_array();            
        }
        
        public function getfilebyid($id){
            $query = $this->db->query("SELECT * FROM berkasfiles WHERE id=$id");
            $result = $query->row();
            return $result;
        }
        public function getfilebyid_array($id){
            $query = $this->db->query("SELECT * FROM berkasfiles WHERE id=$id");
            $result = $query->row_array();
            return $result;
        }
        
        public function get_list_meta_user($file_id){
            $query = $this->db->query("SELECT fl.* FROM file_metas as fl
                                        LEFT JOIN folder_metas as fd on fl.meta_id=fd.id
                                        WHERE fl.file_id=$id AND fd.type='user'");
            return $query->result_array();
        }
        
        public function is_meta_user($file_id,$meta_id,$user_id){
            $query = $this->db->query("SELECT * FROM file_metas WHERE file_id=$file_id AND value='$user_id' AND meta_id=$meta_id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return true;
            } else {
                return false;
            }
        }
        
        public function is_meta_checked($file_id,$meta_id,$value_id){
            $query = $this->db->query("SELECT * FROM file_metas WHERE file_id=$file_id AND value='$value_id' AND meta_id=$meta_id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return true;
            } else {
                return false;
            }
        }
        
        public function insertberkas($data){
            $this->db->insert("berkasfiles", $data);
            return $this->db->insert_id();
        }
        
        public function insert_meta($idfile,$idmeta,$value){
            $this->db->query("INSERT INTO file_metas(file_id,meta_id,value) VALUES($idfile,$idmeta,'$value')");
        }
        
        public function insertakses($data){
            $this->db->insert("file_users", $data);
            return $this->db->insert_id();
        }        
        
        public function updateberkas($data, $id){
            $this->db->where('id', $id);
            $this->db->update("berkasfiles", $data);
            return $this->db->insert_id();
        }
        
        public function update_meta($id,$value){
            $this->db->query("UPDATE file_metas SET value='$value' WHERE id=$id");
        }        
        
        public function search($id,$str){
            $str = strtoupper($str);
            $query = $this->db->query("SELECT bf.*, (SELECT u.nama_lengkap FROM users as u WHERE u.id=bf.user_id) as uploader
                                FROM berkasfiles as bf
                                LEFT JOIN folder_users as fu on bf.folder_id=fu.folder_id
                                WHERE fu.user_id=$id AND UPPER(bf.nama_files) LIKE '%$str%'");
            return $query->result_array();
        }
        
        public function advance_search($nama, $nomor,$hal,$tgl,$asal){
            $where = "WHERE";
            if($nomor != "" && $nomor != null){
                $where .= " fm.value LIKE '%$nomor%'";
            } 
            if($hal != "" && $hal != null) {
                if($where != "WHERE"){
                    $where .= " OR fm.value LIKE '%$hal%'";
                } else {
                    $where .= " fm.value LIKE '%$hal%' ";
                }
            }
            if($tgl != "1970-01-01" && $tgl != null && $tgl != ""){
                $format2 = date('m/d/Y', strtotime($tgl));
                if($where != "WHERE"){
                    $where .= " OR fm.value LIKE '%$tgl%' OR fm.value LIKE '%$format2%'";
                } else {
                    $where .= " fm.value LIKE '%$tgl%' OR fm.value LIKE '%$format2%'";
                }
            } 
            if($asal != "" && $asal != null) {
                if($where != "WHERE"){
                    $where .= " OR fm.value LIKE '%$asal%'";
                } else {
                    $where .= " fm.value LIKE '%$asal%' ";
                }
            }
            
            if($nama != "" && $nama != null){
                if($where != "WHERE"){
                    $where .= " AND bf.nama_files LIKE '%$nama%'";
                } else {
                    $where .= " bf.nama_files LIKE '%$nama%'";
                }
            }
            
            if($where == "WHERE"){
                $where = "";
            }
            $query = $this->db->query("SELECT bf.*, u.nama_lengkap as uploader
                                         FROM file_metas AS fm
                                         LEFT JOIN berkasfiles AS bf ON fm.file_id=bf.id
                                         LEFT JOIN folders AS fd ON bf.folder_id=fd.id
                                         LEFT JOIN users AS u on u.id=bf.user_id
                                         $where
                                         GROUP BY bf.id
                                         ORDER BY bf.created_at DESC");
            return $query->result_array();
        }
        
        public function insert_eval($file,$user){
            $this->db->query("INSERT INTO file_evaluators(user_id,file_id) VALUES($user,$file)");
        }
        
        public function remove_eval($id){
            $this->db->query("DELETE FROM file_evaluators WHERE id=$id");
        }
        public function delete_file_eval($file){
            $this->db->query("DELETE FROM file_evaluators WHERE file_id=$file");
        }
        
        public function get_file_evaluator($id){
            $query = $this->db->query("SELECT fe.status, bf.*, (SELECT u.nama_lengkap FROM users as u WHERE u.id=bf.user_id) as uploader
                                FROM file_evaluators as fe 
                                LEFT JOIN berkasfiles as bf ON fe.file_id=bf.id                                
                                WHERE fe.user_id=$id
                                GROUP BY bf.id
                                ORDER BY bf.created_at DESC");
            return $query->result_array();
        }
        
        public function get_eval_list($file){
            $query = $this->db->query("SELECT fe.id, fe.user_id FROM file_evaluators AS fe WHERE file_id=$file");
            return $query->result_array();
        }
        
        public function change_eval_status($user_id,$file_id){
            $this->db->query("UPDATE file_evaluators SET status='read' WHERE file_id=$file_id AND user_id=$user_id");
        }
        
        public function count_file_evaluator($id){
            $query = $this->db->query("SELECT fe.id
                                FROM file_evaluators as fe                                 
                                WHERE fe.user_id=$id and fe.status='unread'
                                GROUP BY fe.file_id");
            $rs = $query->result_array();
            return sizeof($rs);
        }
        
        public function is_eval($file,$user){
            $query = $this->db->query("SELECT fe.* FROM file_evaluators AS fe WHERE user_id=$user AND file_id=$file");
            $rs = $query->result_array();
            return sizeof($rs)>0;
        }
        
        public function delete($id){
            $this->db->where('id', $id);
            $this->db->delete('berkasfiles');
            //return $this->db->insert_id();
        }
        
        public function delete_meta($id){
            $this->db->query("DELETE FROM file_metas WHERE file_id=$id");
        }
	}
?>