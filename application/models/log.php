<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class log extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}		        
        
        public function get(){
            $query = $this->db->query("SELECT l.*, u.nama_lengkap 
                                        FROM logs as l
                                        LEFT JOIN users as u ON l.user=u.id
                                        ORDER BY l.waktu DESC");
            return $query->result_array();
        }
        
        public function get_limit(){
            $query = $this->db->query("SELECT l.*, u.nama_lengkap 
                                        FROM logs as l
                                        LEFT JOIN users as u ON l.user=u.id
                                        ORDER BY l.waktu DESC
                                        LIMIT 5");
            return $query->result_array();
        }
        
        public function get_user(){
            $query = $this->db->query("SELECT l.*, u.nama_lengkap 
                                        FROM logs as l
                                        LEFT JOIN users as u ON l.user=u.id
                                        WHERE l.tipe=1
                                        ORDER BY l.waktu DESC");
            return $query->result_array();
        }
        
        public function get_folder(){
            $query = $this->db->query("SELECT l.*, u.nama_lengkap 
                                        FROM logs as l
                                        LEFT JOIN users as u ON l.user=u.id
                                        WHERE l.tipe=0
                                        ORDER BY l.waktu DESC");
            return $query->result_array();
        }
        
        public function insert($user_id,$kegiatan,$tipe){
            $this->db->query("INSERT INTO logs(user,kegiatan,tipe) VALUES($user_id,'$kegiatan',$tipe)");
        }
        
        public function insert_log_backup($user_id,$kegiatan){
            $this->db->query("INSERT INTO log_backup(user_id,kegiatan) VALUES($user_id,'$kegiatan')");
        }
        
        public function get_log_backup(){
            $query = $this->db->query("SELECT lb.*, u.nama_lengkap FROM log_backup AS lb
                                        LEFT JOIN users AS u ON u.id=lb.user_id
                                        ORDER BY lb.waktu DESC");
            return $query->result_array();
        }
	}
?>