<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class user extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
		
		public function authenticate($username, $password) {
			$query = $this->db->query("SELECT * FROM users WHERE username='$username' && password='$password' LIMIT 1");
			$result = $query->result_array();
			if(sizeof($result) > 0) {
				return $result[0];
			} else {
				return null;
			}
		}
        
        public function is_already_login($username){
            date_default_timezone_set('Asia/Jakarta');
            
            $query = $this->db->query("SELECT ip_address, user_data, last_activity FROM ci_sessions 
                                        WHERE user_data LIKE '%\"$username\"%' ORDER BY last_activity DESC LIMIT 1");
            $result = $query->result_array();
            if(sizeof($result)>0){
                $data = $result[0];
                $ip = $data['ip_address'];
                $time = $data['last_activity'];
                $now = new DateTime();
                
                $last_act = new DateTime("@$time");
                $interval = $now->diff($last_act);
                $elapsed = $interval->format('%i');
                
                if(intval($elapsed) > 15) {
                    $this->db->query("DELETE FROM ci_sessions WHERE ip_address='$ip'");
                    return false;
                } else {
                    return true;
                }
                
                
                $query = $this->db->query("SELECT user_data FROM ci_sessions 
                                        WHERE ip_address = '$ip' ORDER BY last_activity DESC LIMIT 1");
                $res = $query->result_array();
                if(sizeof($result)>0){
                    $d2 = $res[0];
                    $userdata = $d2['user_data'];
                    if($userdata=="" || $userdata==null){
                        return false;
                    } else {
                        if(strpos($userdata, "flash") > -1){
                            return false;
                        } else {
                            return true;
                        }
                    }
                }
            } else {
                return false;
            }
        }
		
		public function get($id = NULL){
          //if no parameter, get all
            if (is_null($id)) {
                $query = $this->db->query("SELECT u.*, o.nama_organisasi
                                            FROM users as u
                                            LEFT JOIN organisasis as o ON u.organisasi_id = o.id");  
            } else {
                $query = $this->db->query("SELECT u.*, o.nama_organisasi 
                                            FROM users as u
                                            LEFT JOIN organisasis as o ON u.organisasi_id = o.id
                        where u.id='$id'"); 
            }
            return $query->result_array();
		}
        
        public function getdatabyid($id){
           $query = $this->db->query("SELECT u.*, o.nama_organisasi 
                                            FROM users as u
                                            LEFT JOIN organisasis as o ON u.organisasi_id = o.id
                        where u.id='$id'"); 
            return $query->row();
        }
        public function get_limit(){
            $query = $this->db->query("SELECT u.*, o.nama_organisasi
                                            FROM users as u
                                            LEFT JOIN organisasis as o ON u.organisasi_id = o.id 
                                            ORDER BY created_at DESC
                                            LIMIT 5");  
            return $query->result_array();
        }
        
        public function create($username,$password,$role,$nama,$email,$organisasi,$nip,$attachment) {
            if($attachment != "") {
                $this->db->query("INSERT INTO users(username,password,role,nama_lengkap,email,organisasi_id,nip,avatar)
                                    VALUES('$username','$password',$role,'$nama','$email','$organisasi','$nip','$attachment')");
            } else {
                $this->db->query("INSERT INTO users(username,password,role,nama_lengkap,email,organisasi_id,nip)
                                    VALUES('$username','$password',$role,'$nama','$email','$organisasi','$nip')");
            }
        }
        
        public function update($id,$username,$password,$role,$nama,$email,$organisasi,$nip,$attachment) {
            if($attachment != "") {
                $this->db->query("UPDATE users SET username='$username',password='$password',role='$role',nama_lengkap='$nama',email='$email',
                                    organisasi_id='$organisasi',nip='$nip',avatar='$attachment' WHERE id=$id");
            } else {
                $this->db->query("UPDATE users SET username='$username',password='$password',role='$role',nama_lengkap='$nama',email='$email',
                                    organisasi_id='$organisasi',nip='$nip' WHERE id=$id");
            }
        }
        
        public function delete($id){
            $this->db->query("DELETE FROM users WHERE id=$id");
        }
        
        public function update_data_by_id($id, $data){
            $this->db->where("id", $id);
            $this->db->update("users", $data);
        }
        
        public function is_exist($username,$email){
            $query = $this->db->query("SELECT * FROM users WHERE username='$username'");
            $rs = $query->result_array();
            return sizeof($rs) > 0;
        }
	}
?>