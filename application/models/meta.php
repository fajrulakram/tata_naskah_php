<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class meta extends CI_Model {		
		public function __construct(){
			parent::__construct();					
		}	
        
        public function get(){
            $query = $this->db->query("SELECT cm.* FROM custom_metas as cm");
            return $query->result_array();
        }
        
        public function get_id($id){
            $query = $this->db->query("SELECT cm.*,(SELECT count(folder_id) FROM folder_metas WHERE nama='$id' AND type='custom') as jumlah_folder
                                        FROM custom_metas as cm WHERE id=$id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return $rs[0];
            } else {
                return null;
            }
        }
        
        public function get_value_id($id){
            $query = $this->db->query("SELECT mv.* FROM custom_meta_values AS mv WHERE mv.id = $id");
            $rs = $query->result_array();
            if(sizeof($rs)>0){
                return $rs[0];
            } else {
                return null;
            }
        }
        
        public function get_value($id){
            $query = $this->db->query("SELECT mv.* FROM custom_meta_values as mv WHERE mv.custom_id=$id");
            return $query->result_array();
        }
        
        public function create($nama){
            $this->db->query("INSERT INTO custom_metas(name) VALUES('$nama')");
            return $this->db->insert_id();
        }
        
        public function insert_value($id,$value){
            $this->db->query("INSERT INTO custom_meta_values(custom_id,value) VALUES($id,'$value')");
        }
        
        public function update($id,$name){
            $this->db->query("UPDATE custom_metas SET name='$name' WHERE id=$id");
        }
        
        public function update_value($id,$value){
            $this->db->query("UPDATE custom_meta_values SET value='$value' WHERE id=$id");
        }
        
        public function delete_value($m_id,$id){            
            $this->db->query("DELETE FROM custom_meta_values WHERE id=$id");
        }
        
        public function delete($id){
            $this->db->query("DELETE FROM custom_meta_values WHERE custom_id=$id");
            $this->db->query("DELETE FROM custom_metas WHERE id=$id");
        }
    }
?>