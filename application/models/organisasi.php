<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class organisasi extends CI_Model {
		
		public function __construct(){
			parent::__construct();					
		}
					
		public function get(){
			$query = $this->db->query("SELECT o.*, (SELECT nama_organisasi FROM organisasis as o2 
                                                WHERE o.organisasi_id=o2.id LIMIT 1)  as parent
                                  FROM organisasis as o
                                  ORDER BY organisasi_id ASC");										
			return $query->result_array();
		}
        
        public function get_root(){
            $query = $this->db->query("SELECT * FROM organisasis WHERE organisasi_id IS NULL");
            return $query->result_array();
        }
        
        public function get_child($id){
            $query = $this->db->query("SELECT * FROM organisasis WHERE organisasi_id=$id");
            return $query->result_array();
        }
        
        public function get_member($id) {
             $query = $this->db->query("SELECT * FROM users WHERE organisasi_id=$id");
             return $query->result_array();
        }        
                
        public function get_id($id) {
            $query = $this->db->query("SELECT * FROM organisasis WHERE id=$id");
            $hasil = $query->result_array();
            if(sizeof($hasil) > 0){
                return $hasil[0];
            } else {
                return null;
            }
        }
        
        public function create($nama,$parent=null){
            if($parent != null) {
                $this->db->query("INSERT INTO organisasis (nama_organisasi,organisasi_id) VALUES ('$nama',$parent)");
            } else {
                $this->db->query("INSERT INTO organisasis (nama_organisasi) VALUES ('$nama')");
            }
        }  
        
        public function update($id,$nama,$parent=null){
            if($parent != null) {
                $this->db->query("UPDATE organisasis SET nama_organisasi='$nama', organisasi_id=$parent WHERE id=$id");
            } else {
                $this->db->query("UPDATE organisasis SET nama_organisasi='$nama' WHERE id=$id");
            }
        }  
        
        public function delete($id){
            $this->db->query("DELETE FROM organisasis WHERE id=$id");
        }
	}
?>