<ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-eye"></i> Log User</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Data<strong>Logs</strong></h4></header>
    		</div>    		
        	<div class="box-body table-responsive">
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>
                            <th>Nomor</th>
        					<th>Waktu</th>
        					<th>User</th>
        					<th>Kegiatan</th>                            
        				</tr>
        			</thead>
        	    	<tbody>
        				<!-- DAFTAR FOLDER -->
                        <?php 
                            $id = 1;
                            foreach($logs as $l) { ?>
                                <tr>
                                    <td><?php echo $id ?></td>
                                    <td><?php echo date('d-M-Y H:i:s', strtotime($l['waktu'])) ?></td>
                                    <td><?php echo $l['nama_lengkap'] ?></td>
                                    <td><?php echo $l['kegiatan'] ?></td>                                    
                                </tr>
                        <?php $id++; } ?>
        			</tbody>
        		</table>
        	</div>
        </div>
	</div>
</div>
<script>
    $("#datatable1").dataTable({
        
    });
</script>