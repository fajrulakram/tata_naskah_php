<!DOCTYPE html>
<html>
<head>
  	<title><?php echo $title ?></title>
	<!-- BEGIN STYLESHEETS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/bootstrap.css@1401441895.css' rel='stylesheet' type='text/css'/>		
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox_responsive.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/font-awesome.min.css@1401441895.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/jquery-ui/jquery-ui-boostbox.css@1401442122.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/fullcalendar/fullcalendar.css@1401442122.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/jquery.dataTables.css@1401442112.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/TableTools.css@1401442112.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/nestable/nestable.css' rel='stylesheet' type='text/css'/>		
    <link href='<?php echo base_url() ?>assets/stylesheets/libs/blueimp-file-upload/jquery.fileupload.css@1401442110.css' 
          rel='stylesheet' type='text/css'/>	
    <link href='<?php echo base_url() ?>assets/stylesheets/userdetail.css' rel='stylesheet' type='text/css'/>	
    <link href='<?php echo base_url() ?>assets/stylesheets/animate.css' rel='stylesheet' type='text/css'/>
	<!-- END STYLESHEETS -->	  	
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-1.11.0.min.js"></script>  	    
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-migrate-1.2.1.min.js"></script>	
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery-ui/jquery-ui.js"></script>
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>
    <?php date_default_timezone_set('Asia/Jakarta'); ?>
  	<style>
  		body {
  			overflow-x : hidden;
  		}
  		
  		.logo {
  			height : 50px;
  			float : left;
  			margin-top : 5px;
  		}
  		
  		.logo img {
  			height : 100%;
  		}
  		
  		.text-logo {
  			margin-left : 60px;
  			width : 600px;
  			vertical-align : middle;
  		}
  		
  		.avatar_aang {
  			height : 60px;
  			float : left;
  			margin-left : 10px;
  		}
  		
  		.avatar_aang img {
  			height : 100%;
  		}
  		
  		.text_avatar {
  			height : 70px;
  			margin-left : 80px;
  		}
  		
  		.text_avatar h4 {
  			color : #ffffff;
  			font-size : 18px;
  			line-height : 40px;
  			margin-bottom : 0px;
  		}
  		
  		.text_avatar p {
  			color : #A5A5A5;
  		}
  		
  	</style>
</head>
<body class="body-striped">

	<!-- BEGIN HEADER-->
	<header id="header">
		<!-- BEGIN NAVBAR -->
		<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<!--
			<div class="navbar-header">
				<a class="btn btn-transparent btn-equal btn-menu" href="javascript:void(0);"><i class="fa fa-bars fa-lg"></i></a>
				<div class="navbar-brand">
					<a class="main-brand" href="#">
						<h3 class="text-light text-white"><span>Tata<strong>Naskah</strong> </span></h3>
					</a>
				</div>
				<a class="btn btn-transparent btn-equal navbar-toggle" data-toggle="collapse" data-target="#header-navbar-collapse"><i class="fa fa-wrench fa-lg"></i></a>
			</div>
			-->
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="header-navbar-collapse" 
                 style="background-color : #ffffff;padding-bottom:5px;border-bottom:solid #d3d3d3 1px;">
				<ul class="nav navbar-nav">
					<li>
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/images/Logo_ESDM.png" />
						</div>
						<h3 class="text-logo">Sub Direktorat Pengangkutan Migas</h3>
					</li>
				</ul><!--end .nav -->
				<ul class="nav navbar-nav navbar-right">					
					<li><span class="navbar-devider"></span></li>					
					
					<li><span class="navbar-devider"></span></li>
					<li class="dropdown">
						<a href="javascript:void(0);" class="navbar-profile dropdown-toggle text-bold" data-toggle="dropdown">
                            <?php echo $this->session->userdata('fullname') ?><i class="fa fa-fw fa-angle-down"></i>
                        <?php
                            $avatar =  base_url()."assets/images/avatar.png";
                            if($this->session->userdata('avatar') != null) {
                                $avatar = $this->session->userdata('avatar');
                            }
                        ?>
                            <img class="img-circle" src="<?php echo $avatar ?>" style="width:40px;"/></a>
						<ul class="dropdown-menu animation-slide">
							<li><a href="#">Profile</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url() ?>index.php/session/destroy">
                                <i class="fa fa-fw fa-power-off text-danger"></i> Logout</a>
                            </li>
						</ul><!--end .dropdown-menu -->
					</li><!--end .dropdown -->
				</ul><!--end .nav -->
			</div><!--end #header-navbar-collapse -->
		</nav>
		<!-- END NAVBAR -->
	</header>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN SIDEBAR-->
		<div id="sidebar">
			<div class="sidebar-back"></div>
			<div class="sidebar-content">
				<div class="nav-brand" style="height : 25px">
					<a class="main-brand" href="#">
						<h3 class="text-light text-white" style="font-size : 36px;">
							<span>&nbsp;<strong>&nbsp;</strong> </span>
						</h3>
					</a>
				</div>

				<!-- BEGIN MENU SEARCH -->
				<!--
				<form class="sidebar-search" role="search">
					<a href="javascript:void(0);"><i class="fa fa-search fa-fw search-icon"></i><i class="fa fa-angle-left fa-fw close-icon"></i></a>
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control navbar-input" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-equal" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
				</form> -->
				<!-- END MENU SEARCH -->
				<!-- USER AVATAR -->
				<div style="height : 80px; margin-top : 20px; margin-bottom : 10px;">
					<div class="avatar_aang">
						<img class="img-circle" src="<?php echo $avatar ?>" style="width: 60px;"/>
					</div>
					<div class="text_avatar">
						<h4><?php echo $this->session->userdata('fullname') ?></h4>						
					</div>
				</div>
				<!-- END OF USER AVATAR -->
				<!-- BEGIN MAIN MENU -->
				<ul class="main-menu">
					<!-- Menu Dashboard -->
					<li>
						<a href="<?php echo base_url() ?>" <?php if($aktif=='dashboard') { echo 'class="active"'; } ?>>
							<i class="fa fa-home fa-fw"></i><span class="title">Dashboard</span>
						</a>
					</li><!--end /menu-item -->
					<!-- Menu UI -->
					<li <?php if($aktif=='organisasi') { echo 'class="expanded"'; } ?>>
						<a href="javascript:void(0);" <?php if($aktif=='organisasi') { echo 'class="active"'; } ?>>
							<i class="fa fa-cogs fa-fw"></i><span class="title">Data Rujukan</span> <span class="expand-sign">+</span>
						</a>
						<!--start submenu -->
						<ul>
							<li><a href="<?php echo base_url() ?>index.php/organisasis/" >Organisasi</a></li>
						</ul><!--end /submenu -->
					</li><!--end /menu-item -->
					<!-- Menu Pages -->
					
					<li <?php if($aktif=='user') { echo 'class="expanded"'; } ?>>
						<a href="javascript:void(0);" <?php if($aktif=='user') { echo 'class="active"'; } ?>>
							<i class="fa fa-group fa-fw"></i><span class="title">Pengguna</span> <span class="expand-sign">+</span>
						</a>
						<!--start submenu -->
						<ul>
							<li><a href="<?php echo base_url() ?>index.php/users/" >Pengguna</a></li>							
						</ul><!--end /submenu -->
					</li><!--end /menu-item -->
					<!-- Menu Pages -->
					
					<li <?php if($aktif=='folder') { echo 'class="expanded"'; } ?>>
						<a href="javascript:void(0);" <?php if($aktif=='folder') { echo 'class="active"'; } ?>>
							<i class="fa fa-folder fa-fw"></i><span class="title">Directory</span> <span class="expand-sign">+</span>
						</a>
						<!--start submenu -->
						<ul>
							<li><a href="<?php echo base_url() ?>index.php/directories/" >Folder</a></li>

							<li><a href="<?php echo base_url() ?>index.php/metas/" >Metadata</a></li>

						</ul><!--end /submenu -->
					</li><!--end /menu-item -->
					<!-- Menu Pages -->
					<li <?php if($aktif=='logs') { echo 'class="expanded"'; } ?>>
						<a href="javascript:void(0);" <?php if($aktif=='logs') { echo 'class="active"'; } ?>>
							<i class="fa fa-eye fa-fw"></i><span class="title">Log</span> <span class="expand-sign">+</span>
						</a>
						<!--start submenu -->
						<ul>
                            <li><a href="<?php echo base_url() ?>index.php/logs/">Log Folder</a></li>
							<li><a href="<?php echo base_url() ?>index.php/logs/folder">Log Admin</a></li>							
						</ul><!--end /submenu -->
					</li><!--end /menu-item -->
								
					<li>
						<a href="<?php echo base_url() ?>index.php/backups/" >
							<i class="fa fa-wrench fa-fw"></i><span class="title">Pemeliharan</span>
						</a>
					</li><!--end /menu-item -->

				</ul><!--end .main-menu -->
				<!-- END MAIN MENU -->
			</div>
		</div><!--end #sidebar-->
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			
			<section style="margin-top : 10px; background-color : #ededed">
				<!-- REAL CONTENT START HERE -->
				<?php $this->load->view($view); ?>
				<!-- END REAL CONTENT -->
			</section>
			
		</div><!--end #content-->		
		<!-- END CONTENT -->

	</div><!--end #base-->

	<!-- SCRIPT -->		
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/BootstrapFixed.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/bootstrap/bootstrap.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/ColVis/js/ColVis.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/App.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/nestable/jquery.nestable.js"></script>	
	<!-- END SCRIPT -->
	<script type = 'text/javascript'>
		$(document).ready(function() {
			$('.nestable-list').nestable({});	
			$('.nestable-list').nestable('expandAll');
		});
	</script>
</body>
</html>
