<!DOCTYPE html>
<html>
<head>
  	<title><?php echo $title; ?></title>
	<!-- BEGIN STYLESHEETS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/bootstrap.css@1401441895.css' rel='stylesheet' type='text/css'/>		
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/boostbox_responsive.css@1401441893.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/font-awesome.min.css@1401441895.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/jquery-ui/jquery-ui-boostbox.css@1401442122.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/fullcalendar/fullcalendar.css@1401442122.css' rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/nestable/nestable.css' rel='stylesheet' type='text/css'/>	
    <link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/jquery.dataTables.css@1401442112.css' 
          rel='stylesheet' type='text/css'/>
	<link href='<?php echo base_url() ?>assets/stylesheets/libs/DataTables/TableTools.css@1401442112.css' rel='stylesheet' type='text/css'/>
	<!-- END STYLESHEETS -->  	
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-1.11.0.min.js"></script>  	  		
    <script type="text/javascript" src="<?Php echo base_url(); ?>/assets/javascripts/pdf.js"></script> 
    <script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/ColVis/js/ColVis.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/TableTools/media/js/TableTools.min.js"></script>

    <style>
        body {
            overflow-x : hidden;
        }
        ::-webkit-input-placeholder {
           font-style : italic;
        }

        :-moz-placeholder { /* Firefox 18- */
           font-style : italic;
        }

        ::-moz-placeholder {  /* Firefox 19+ */
           font-style : italic;
        }

        :-ms-input-placeholder {  
           font-style : italic;
        }  
        
        .fc-event{
            cursor: pointer;
        }
        
        .logo {
            float : left;
            margin-top:-15px;
        }
        
        .logo img {
            height : 50px;
        }
        
    </style>
</head>
<body class="body-striped">

	<!-- BEGIN HEADER-->
	<header id="header">
		<!-- BEGIN NAVBAR -->
		<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="header-navbar-collapse">
                <div>                    
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/images/Logo_ESDM.png" />
						</div>
						<h3 class="text-logo" style="margin-left : 60px">Aplikasi Sistem Administrasi Pengangkutan Migas</h3>
                </div>
                <div style="float:left;margin-left : -55px; margin ">
                    <div class="nav navbar-nav" style="margin-top: 10px;">
                        <div class="btn-group">
                        <a href="#" class="btn btn-success btn-rounded fileinput-button btn-sm" data-toggle="modal" data-target="#simpleModal" data-original-title="Upload">
                            <span>Upload</span>
                        </a>
                        <a href="#" class="btn btn-info btn-rounded btn-sm" data-toggle="modal" data-target="#preview" data-original-title="Preview">
                            <span>Preview</span>
                        </a>
                        <a href="#" class="btn btn-info btn-rounded btn-sm" data-target="#download" id="download" data-original-title="Download">
                            <span>Download</span>
                        </a>
                        <a href="#" id="csv" class="btn btn-info btn-rounded btn-sm" data-target="#csv" data-original-title="Csv">
                            <span>CSV</span>
                        </a>
                        <a href="#" class="btn btn-warning btn-rounded btn-sm" data-toggle="modal" data-target="#edit" data-original-title="Update">
                            <span>Edit</span>
                        </a>
                        <a href="#" class="btn btn-danger btn-rounded btn-sm" data-toggle="modal" data-target="#delete" data-original-title="Delete">
                            <span>Delete</span>
                        </a>
                        </div>
                    </div>
                    <!--<ul class="nav navbar-nav">
                        <li><a href="#" <?php if($issearch){ echo 'class="disabled" style="color : #d7d7d7"'; } ?>
                               data-toggle="modal" data-target="#simpleModal" data-original-title="Upload">
                            <i class="fa fa-cloud-upload fa-lg"></i> </a></li>
                        <li><span class="navbar-devider"></span></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="#" <?php if(!$isfolder && !$isnotif){ echo 'class="disabled" style="color : #d7d7d7"'; } ?>
                               data-toggle="modal" data-target="#preview" data-original-title="Preview">
                            <i class="fa fa-eye fa-lg"></i></a></li>
                        <li><span class="navbar-devider"></span></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="#" <?php if(!$isfolder && !$isnotif){ echo 'class="disabled" style="color : #d7d7d7"'; } ?>
                               data-target="#download" id="download" data-original-title="Download">
                            <i class="fa fa-download fa-lg"></i></a></li>
                        <li><span class="navbar-devider"></span></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="#" <?php if(!$isfolder && !$isnotif){ echo 'class="disabled" style="color : #d7d7d7"'; } ?>
                               data-toggle="modal" data-target="#edit" data-original-title="Update">
                            <i class="fa fa-edit fa-lg"></i></a></li>
                        <li><span class="navbar-devider"></span></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="#" <?php if(!$isfolder && !$isnotif){ echo 'class="disabled" style="color : #d7d7d7"'; } ?>
                               data-toggle="modal" data-target="#delete" data-original-title="Delete">
                            <i class="fa fa-trash-o fa-lg"></i></a></li>
                        <li><span class="navbar-devider"></span></li>
                    </ul>
                    -->
                    <ul class="nav navbar-nav">
                        <li>
                            <form action="<?php echo base_url() ?>index.php/filemanagers/search/"
                                  class="sidebar-search" role="search" id="search_file"
                                      <?php if(!$isfolder) {echo "style='display:none'"; } ?>
                                  >

                                <div class="form-group" style="width : 250px; margin-top:10px;">
                                    <div class="input-group">
                                        <input type="text" id="file_search" class="form-control navbar-input" placeholder="Search File..." 
                                               style="color : #000000" required="required" value="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-equal" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </li>					
                    </ul><!--end .nav -->
                    <ul class="nav navbar-nav">
                        <li <?php if(!$isfolder) {echo "style='display:none'"; } ?> >
                            <a class="btn btn-outline btn-info" href="<?php echo base_url() ?>index.php/search/"
                               style="color:white;margin-top:10px;padding-top:3px; height:30px; background-color : #3C788F">
                            Advance Search</a></li>
                    </ul>
                </div>
				<ul class="nav navbar-nav navbar-right" style="margin-top: -50px">					
					<li><span class="navbar-devider"></span></li>
					<li class="dropdown" id="notif">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-lg fa-envelope"></i><sup class="badge badge-support2">&nbsp;</sup></a>
						<ul class="dropdown-menu animation-zoom">							
							<li>
								<a class="alert alert-warning" href="javascript:void(0);">
									<img class="pull-right img-circle dropdown-avatar" 
                                         src="<?php echo base_url() ?>assets/images/boostbox/avatar2.jpg@1401441851" />
									<strong>Alex Anistor</strong><br/>
									<small>Testing functionality...</small>
								</a>
                            </li>							
							<li><a href="#">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
						</ul><!--end .dropdown-menu -->
					</li><!--end .dropdown -->
					
					<li><span class="navbar-devider"></span></li>
					<li class="dropdown">
						<a href="javascript:void(0);" class="navbar-profile dropdown-toggle text-bold" data-toggle="dropdown">
							<?php echo $this->session->userdata('fullname') ?><i class="fa fa-fw fa-angle-down"></i>
                            <?php
                                $avatar =  base_url()."assets/images/avatar.png";
                                if($this->session->userdata('avatar') != null) {
                                    $avatar = $this->session->userdata('avatar');
                                }
                            ?> 
						<img class="img-circle" src="<?php echo $avatar ?>" />						
						<ul class="dropdown-menu animation-slide">
							<li style="margin-top:-30px"><a href="<?Php echo site_url('filemanagers/profile') ?>">Profile</a></li>
							<li><a href="<?Php echo site_url('agendas/'); ?>">Agenda</a></li>
                            <li><a href="<?Php echo base_url() ?>index.php/perizinan/">Summary Perizinan</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url() ?>index.php/session/destroy">
                                <i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
						</ul><!--end .dropdown-menu -->
					</li><!--end .dropdown -->
				</ul><!--end .nav -->
			</div><!--end #header-navbar-collapse -->
		</nav>
		<!-- END NAVBAR -->
	</header>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN SIDEBAR-->
		<div id="sidebar">
			<div class="sidebar-back"></div>
			<div class="sidebar-content">
				<div class="nav-brand" style="height : 70px;">
					<a href="<?php echo base_url()?>" class="main-brand" href="<?php echo base_url()?>">
						<h3 class="text-light text-black" style="color:white">
							<span>Sub Direktorat <strong>Pengangkutan Migas</strong> </span>
						</h3>
					</a>
				</div>

				<!-- BEGIN MENU SEARCH -->
				<!--
				<form class="sidebar-search" role="search">
					<a href="javascript:void(0);"><i class="fa fa-search fa-fw search-icon"></i><i class="fa fa-angle-left fa-fw close-icon"></i></a>
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control navbar-input" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-equal" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
				</form> -->
				<!-- END MENU SEARCH -->

				<!-- BEGIN MAIN MENU -->
				<ul class="main-menu">
					<!-- Menu Dashboard -->
                    <?Php if($isfolder){ 
                         if($folder==null) {
                            echo '<p style="margin-left : 20px; margin-top : 30px; font-style:italic">
                                Maaf, Anda Belum Memiliki Folder. Silahkan Menghubungi Admin</p>';
                         } else { ?>
                            <li class="liparent dashboard" id="dashboard">
                                <a class="parent active" href="<?php echo base_url() ?>">
                                    <i class="fa fa-home fa-fw"></i><span class="title">Dashboard</span>
                                </a>
                            </li>
                        <?php foreach($folder as $data){ 
                            $subfolder = $this->folder->getchild($this->session->userdata('id_user'), $data['id']);?>
                            <li class="liparent" id="parent<?Php echo $data['id']; ?>">
                                <a class="parent <?php if($subfolder == null) { echo "child"; }?>
                                          folder<?Php echo $data['id']; ?>" data-id="<?Php echo $data['id']; ?>"
                                   href="<?php if($subfolder != null) {echo "javascript:void(0);"; } else { echo "#"; } ?>">
                                    <?php if($subfolder != null) { ?>
                                    <span class="expand-sign" style="margin-left : -30px">+</span>
                                    <?php } else { ?>
                                    <span class="expand-sign" style="margin-left : -30px">&nbsp;</span>
                                    <?php }?>    
                                    <i class="fa fa-folder fa-lg" style="color :#445358"></i>
                                    <span class="title" >
                                        <?Php echo $data['nama_folder']; ?></span>
                                </a>
                                <?php if($subfolder != null) { ?>
                                    <ul>
                                    <?php foreach($subfolder as $sub) {?>
                                        <!--start submenu -->						
							            <li class="lichild" id="child<?Php echo $sub['id']; ?>" data-id="<?Php echo $sub['id']; ?>">
                                            <a href="#" class="child folder<?Php echo $sub['id']; ?>" data-id="<?Php echo $sub['id']; ?>">
                                            <!-- <i class="fa fa-folder fa-lg" style="color : #445358;float : left;"></i> -->
                                            <div style="margin-left : 20px;"><?Php echo $sub['nama_folder']; ?></div></a>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                 <?php } ?>
                            </li><!--end /menu-item -->
                    <?php } 
                   }?>
                    
					<div class="dd nestable-list" style="display: none">
						<ol class="dd-list" id="top_folder" style="overflow-y : auto;">
                            <?php if($folder==null) { ?>
                            <p style="margin-left : 20px; margin-top : 30px; font-style:italic;color:white;">
                                Maaf, Anda Belum Memiliki Folder. Silahkan Menghubungi Admin</p>
                            <?php } ?>
							<?Php foreach($folder as $data){ ?>
							<li class="dd-item parent" data-id="<?Php echo $data['id']; ?>">
								<div class=" btn btn-default folder<?Php echo $data['id']; ?>">                                    
									<i class="fa fa-folder fa-lg"></i> <?Php echo $data['nama_folder']; ?>                                    
								</div>
                                <?Php $subfolder = $this->folder->getchild($this->session->userdata('id_user'), $data['id']); ?>
									<?Php if (sizeof($subfolder) != 0){ ?>
										<ol class="dd-list">
											<?Php foreach($subfolder as $sub){ ?>
											<li class="dd-item child" data-id="<?Php echo $sub['id']; ?>">
												<div class=" btn btn-default folder<?Php echo $sub['id']; ?>">
                                                    <i class="fa fa-folder fa-lg"></i> <?Php echo $sub['nama_folder']; ?></div>
											</li>
											<?php } ?>
										</ol>
                                <?php } ?>
							</li>
                            <?php } ?>              
						</ol>
					</div><!--end .dd.nestable-list -->
                    <?php } else if($isnotif || $isintegrasi){ ?>
                    <!-- side bar notifikasi -->
                    <div style="margin-top : 30px;">
                        <a href="<?php echo base_url() ?>" class="btn btn-primary btn-block">
                            <i class="fa fa-mail-reply fa-lg" style="margin-left : -50px;"></i>
                            Back To Filemanager</a>
                    </div>
                    <?php } else if($issearch) { ?>
                    <div style="margin-top : 30px;">
                        <a href="<?php echo base_url() ?>" class="btn btn-primary btn-block">
                            <i class="fa fa-mail-reply fa-lg" style="margin-left : -50px;"></i>
                            Back To Filemanager</a>
                    </div>
                    <div>
                        <form action="<?php echo base_url() ?>index.php/search/result/" 
                              class="form-horizontal form-bordered form-validate" method="post">
                            <div class="form-group">
								<div class="col-lg-3 col-sm-2">
									<label for="name" class="control-label" style="color:white">Nama File</label>
								</div>
								<div class="col-lg-9 col-sm-10">
									<input type="text" name="nama" class="form-control" placeholder="Nama File Surat" 
                                           data-rule-minlength="2">
								</div>
							</div>
                            <div class="form-group">
								<div class="col-lg-3 col-sm-2">
									<label for="name" class="control-label" style="color:white">Nomor</label>
								</div>
								<div class="col-lg-9 col-sm-10">
									<input type="text" name="nomor" id="nama" class="form-control" placeholder="Nomor Surat" 
                                           data-rule-minlength="2">
								</div>
							</div>
                            <div class="form-group">
								<div class="col-lg-3 col-sm-2">
									<label for="name" class="control-label" style="color:white">Perihal</label>
								</div>
								<div class="col-lg-9 col-sm-10">
									<input type="text" name="perihal" id="perihal" class="form-control" placeholder="Perihal Surat" 
                                           data-rule-minlength="2">
								</div>
							</div>
                            <div class="form-group">
								<div class="col-lg-3 col-sm-2">
									<label for="search_tanggal" class="control-label" style="color:white">Tanggal</label>
								</div>
								<div class="col-lg-9 col-sm-10">
									<input type="text" name="tanggal" id="search_tanggal" class="form-control" placeholder="Tanggal Surat" 
                                           data-rule-minlength="2">
								</div>
							</div>
                            <div class="form-group">
								<div class="col-lg-3 col-sm-2">
									<label for="name" class="control-label" style="color:white">Asal Surat</label>
								</div>
								<div class="col-lg-9 col-sm-10">
									<input type="text" name="asal" id="asal" class="form-control" placeholder="Asal Surat">
								</div>
							</div>
                            <div class="form-footer col-lg-offset-3 col-sm-offset-2">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
                        </form>
                    </div>
                    <?php } else if(isset($sidebar)){?>
                            <?Php echo $this->load->view("sidebar/".$sidebar); ?>
                    <?Php } else { ?>
                        <div style="margin-top : 30px;">
                            <a href="<?php echo base_url() ?>" class="btn btn-primary btn-block">
                                <i class="fa fa-mail-reply fa-lg" style="margin-left : -50px;"></i>
                                Back To Filemanager</a>
                        </div>
                        <div class="btn-group" style="margin-top:10px;width:100%;background-color:#0A6DA7">
                            <a href="<?php echo base_url()?>index.php/agendas/" style="float:left">
                                <label class="btn btn-info btn-outline <?php if($active=='calendar') {echo "active"; } ?>" 
                                       style="width:100%;  <?php if($active=='list') {echo "color : #b3b3b3"; } ?>">
                                        <i class="fa fa-calendar"></i> Calender View
                                </label>
                            </a>
                            <a href="<?php echo base_url()?>index.php/agendas/lists/">
                                <label class="btn btn-info btn-outline <?php if($active=='list') {echo "active"; } ?>" 
                                       style="width:50%; <?php if($active=='calendar') {echo "color : #b3b3b3"; } ?>">
                                        <i class="fa fa-list"></i> List View
                                </label>                            
                            </a>
						</div>
                         <?php if($active=="calendar") { ?>
                        <div class="box-body box-body-darken small-padding style-support3" >
                           
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-sm btn-support3 active">
                                    <input type="radio" name="calendarMode" value="month">Month
                                </label>
                                <label class="btn btn-sm btn-support3">
                                    <input type="radio" name="calendarMode" value="agendaWeek">Week
                                </label>
                                <label class="btn btn-sm btn-support3">
                                    <input type="radio" name="calendarMode" value="agendaDay">Day
                                </label>
                            </div>
                            <div class="btn-group pull-right">
                                <button id="calender-prev" type="button" class="btn btn-sm btn-equal btn-support3">
                                    <i class="fa fa-chevron-left"></i></button>
                                <button id="calender-next" type="button" class="btn btn-sm btn-equal btn-support3">
                                    <i class="fa fa-chevron-right"></i></button>
                            </div>
                        </div>                        
                        <div class="box-body">
                            <h1 class="text-light selected-day" style="color:white">&nbsp;</h1>
                            <h3 class="text-light selected-date" style="color:white">&nbsp;</h3>
                            <br/><br/>
                            <ul class="list-events list-group">
                                <li class="list-group-header">
                                    
                                </li>
                                
                            </ul>
                        </div>
						<?php } else { ?>
                         <div class="box-head">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active" style="width:100%; background-color:#52A2B2;color:black;">
                                    <a href="#inbox"><i class="fa fa-fw fa-inbox"></i> Semua Agenda</a></li>					
                                <li class="" style="width:100%; background-color:#52A2B2;color:black;">
                                    <a href="#deleted"><i class="fa fa-fw fa-calendar-o"></i> Bulan Ini</a></li>
                                <li class="" style="width:100%; background-color:#52A2B2; color:black;">
                                    <a href="#send"><i class="fa fa-fw fa-forward"></i> Minggu Ini</a></li>
                            </ul>
                        </div>        
                       <?php  } ?>			
                    <?Php } ?>
				</ul><!--end .main-menu -->
				<!-- END MAIN MENU -->
                
			</div>
		</div><!--end #sidebar-->
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			
			<section style="height: 100%; margin-top:40px;">
				<!-- REAL CONTENT START HERE -->
				
				<?Php $this->load->view($view); ?>

				<!-- END REAL CONTENT -->
			</section>
			
		</div><!--end #content-->		
		<!-- END CONTENT -->

	</div><!--end #base-->
	
	<!-- SCRIPT -->
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-migrate-1.2.1.min.js"></script>	
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/BootstrapFixed.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/bootstrap/bootstrap.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/App.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/nestable/jquery.nestable.js"></script>
	<script type = 'text/javascript'>
		$(document).ready(function() {
            
			$('.nestable-list').nestable({});	
			$('.nestable-list').nestable('collapseAll');
			
            $("#top_folder").css("height",$( window ).height()-100 );
            
            //get notification
            $.ajax({
                url: "<?Php echo site_url(); ?>/notifications/get_notification/<?Php echo $this->session->userdata('id_user')?>",                
                success: function(response) {                    
                    $("#notif").html(response);  
                }
            }).done(function() {
                  //do nothing
            });
		});
        
        $('a[data-toggle="modal"]').tooltip({
            animated : 'fade',
            placement : 'bottom',
        })
	</script>
    
    <script src="<?php echo base_url() ?>assets/javascripts/DemoCalendar.js"></script>	
    <style>
        
        a.disabled {
           pointer-events: none;
           cursor: default;
           color : #D7D4D4; 
        }
    </style>
	
	<!-- END SCRIPT -->
</body>
</html>
