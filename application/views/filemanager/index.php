<script>
    var akses = "view,update,delete";
</script>
<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css@1401442111.css" />

<div class="section-body">
	<div class="row">
        <div class="col-lg-6 bataskanan">
            <div id="filemanager" style="overflow:auto; width: 100%">
                <div id="berita" style="margin-bottom:10px;font-weight:bold;font-size:16px;">
                    <?php 
                        $selisih = sizeof($file)-$count_eval;
                        if($file == null) {
                            echo '
                                    <i>Anda Tidak Memiliki File Surat Untuk Dievaluasi Saat Ini.</i>';
                        } else {                            
                                echo '
                                <i>Total Surat yang Dievaluasi : '.sizeof($file).'<br/> Surat Belum Dibaca : '.$count_eval.'<br/> Surat Telah Dibaca : '.$selisih.'</i>';                            
                       }
                    ?>
                </div>
                <table id="filemanager" class="table" style="width: 100%">
                <thead>
                    <tr>
                        <th>Nama File</th>
                        <th>Tanggal</th>
                        <th>Uploader</th>
                    </tr>
                </thead>
                <tbody id="datafile">
                    <?php if(sizeof($file)>0) {                        
                        foreach($file as $rs) {
                            if($rs['status']=='unread'){
                                echo '<tr id="'.$rs['id'].'" style="font-weight : bold"
                                        class="btn-default file" onclick="getmetadata(\''.$rs['id'].'\')">
                                    <td> '.img('assets/images/PDF.png').' '.$rs['nama_files'].' </td>
                                    <td>'.$rs['created_at'].'</td>
                                    <td>'.$rs['uploader'].'</td>
                                </tr>';    
                            } else {
                                echo '<tr id="'.$rs['id'].'" class="btn-default file" onclick="getmetadata(\''.$rs['id'].'\')">
                                        <td> '.img('assets/images/PDF.png').' '.$rs['nama_files'].' </td>
                                        <td>'.$rs['created_at'].'</td>
                                        <td>'.$rs['uploader'].'</td>
                                    </tr>';
                            }
                        }
                    }?>
                </tbody>
            </table>
            
                
            </div>
        </div>
        <div class="col-lg-6">
            
            <div class="box " id="metas" style="overflow:auto; width: 100%">
					
					<div class="box-body no-padding">
                        <div style="height : 40px; background-color : #f4f4f4; border-bottom : solid 1px #b3b3b3 ">
                            <p style="margin-left : 10px; font-size : 13px; line-height : 40px;"><b>Meta Data</b></p>
                        </div>
						<div class="form-horizontal form-bordered" id="metadatadetail">
							<div class="form-group">
								<div class="col-lg-4 col-sm-3">
									<label for="name" class="control-label">&nbsp;</label>
								</div>
								<div class="col-lg-8 col-sm-9">
                                    <p><i>Silahkan memilih file terlebih dahulu.</i></p>
								</div>
							</div>														
						</div>
					</div>
				</div><!--end .box -->
        </div>
	</div>
</div>




<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Upload</h4>
			</div>
			<div class="modal-body">
                <p id="message" style="color : red; display:none;">Jadwal Bentrok Untuk User Berikut : </p>
				<form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" 
                      accept-charset="utf-8" method="post" id="form_uploadfile">
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" onclick="submitform()" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Upload</h4>
			</div>
			<div class="modal-body">
				<form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" accept-charset="utf-8" method="post" id="edit_form_uploadfile">
					
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" onclick="editform()" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width : 900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Preview</h4>
			</div>
			<div class="modal-body">
				<a class="media" href=""></a> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Anda yakin ingin menghapus file ini ?</h4>
			</div>
			<div class="modal-body">
				<a class="media_delete" href=""></a> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="delete_file_btn" data-dismiss="modal">Delete</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->


<script type = 'text/javascript'>
    (function($) {
    $.fn.serializefiles = function() {
        var obj = $(this);
        /* ADD FILE TO PARAM AJAX */
        var formData = new FormData();
        $.each($(obj).find("input[type='file']"), function(i, tag) {
            $.each($(tag)[0].files, function(i, file) {
                formData.append(tag.name, file);
            });
        });
        var params = $(obj).serializeArray();
        $.each(params, function (i, val) {
            formData.append(val.name, val.value);
        });
        return formData;
    };
    })(jQuery);
    
    
    var id_curruent_folder=0;    
    
    function submitform() {
          //pengecekan SP2D
          $("message").css("display", "none");
          if(id_curruent_folder==13){
              var awal = $("#wmulai").val();
              var akhir = $("#wakhir").val();
              var users = $('._pelaksana:checked').map(function() {
                  return this.value;
              }).get();
              
              submitform_sp2d();
          } else if(id_curruent_folder==9){
              var dmoa = $('#dmoa').val();
              var perihal = $('#perihal').val();
              if(perihal=="sp2d" || dmoa!=null || dmoa!= ""){
                  var awal = $("#wmulai").val();
                  var akhir = $("#wakhir").val();
                  var users = $('._pelaksana:checked').map(function() {
                      return this.value;
                  }).get();
                    
                  submitform_sp2d();
              }
          } else {
              $("#form_uploadfile").attr("action","<?Php echo site_url('filemanagers/addfile/'); ?>/"+id_curruent_folder).submit();
          }
          
    };
    
    function submitform_sp2d() {            
        var val = $("#tipe").val();
        if(val==="dinas") {
            //check overlap
            var awal = $("#wmulai").val();
            var akhir = $("#wakhir").val();
            var users = $('._pelaksana:checked').map(function() {
                return this.value;
            }).get();
            $.ajax({
                type: "POST",                
                url: "<?php echo base_url() ?>index.php/agendas/check_overlap_dinas/",
                data: { user: users, awal: awal, akhir: akhir  },
                dataType: "json"
            }).done(function( msg ) {
                console.log(msg.length);
                if(msg.length==0){
                    //$("#registerSubmit").serialize(),
                    console.log("masuk");
                    //var da = $("#form_uploadfile").serialize();
//                     $.ajax({
//                         type: "POST",                
//                         url: "<?Php echo site_url('filemanagers/addfile/'); ?>/"+id_curruent_folder+"?type='json'",
//                         data: da,
//                         dataType: "json"
//                     }).done(function( msg ) {
//                         console.log();
//                         if(msg.status!=0){
//                             $("#form_uploadfile").attr("action","<?Php echo site_url('agendas/addfile/'); ?>/").submit();
//                         } else {
//                             alert("Gagal menambahkan file");
//                         }
//                     });
                    $("#form_uploadfile").attr("action","<?Php echo site_url('filemanagers/addfile/'); ?>/"+id_curruent_folder+"/1").submit();         
                } else {
                    $("#message").css("display",'block');
                    for(i=0;i<msg.length;i++){
                        $("#user_"+msg[i]).css('color','red');
                    }
                }
            });
        } else {
            $("#form_uploadfile").attr("action","<?Php echo site_url('filemanagers/addfile/'); ?>/"+id_curruent_folder).submit();         
        }
    };
    
    function edit_sp2d() {            
        var val = $("#tipe").val();
        if(val==="dinas") {
            //check overlap
            var awal = $("#wmulai_edit").val();
            var akhir = $("#wakhir_edit").val();
            var awal_lama = $("#start_lama").val();
            var akhir_lama = $("#end_lama").val();
            var users = $('._pelaksana_edit:checked').map(function() {
                return this.value;
            }).get();
            $.ajax({
                type: "POST",                
                url: "<?php echo base_url() ?>index.php/agendas/check_overlap_dinas_edit/",
                data: { user: users, awal: awal, akhir: akhir, start_lama: awal_lama, end_lama: akhir_lama  },
                dataType: "json"
            }).done(function( msg ) {
                console.log(msg.length);
                if(msg.length==0){
                    //$("#registerSubmit").serialize(),
                    console.log("masuk");
                   
                    $("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/1").submit();         
                } else {
                    $("#message").css("display",'block');
                    for(i=0;i<msg.length;i++){
                        $("#user_"+msg[i]).css('color','red');
                    }
                }
            });
        } else {
            $("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/1").submit();            
        }
    };
    
    function editform() {
        $("message").css("display", "none");
          if(id_curruent_folder==13){
              var awal = $("#wmulai_edit").val();
              var akhir = $("#wakhir_edit").val();
              var users = $('._pelaksana_edit:checked').map(function() {
                  return this.value;
              }).get();
//               console.log(awal);
//               console.log(akhir);
//               console.log(users);
              edit_sp2d();
          } else if(id_curruent_folder==9){
              var dmoa = $('#dmoa').val();
              var perihal = $('#perihal').val();
              if(perihal=="sp2d" || dmoa!=null || dmoa!= ""){
                  var awal = $("#wmulai_edit").val();
                  var akhir = $("#wakhir_edit").val();
                  var users = $('._pelaksana_edit:checked').map(function() {
                      return this.value;
                  }).get();
                    
                  edit_sp2d();
              }
          } else {
              $("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/").submit();    
          }
        
          //$("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/").submit();          
    };
    $(document).ready(function() {                
        $("#filemanager").css("height",$( window ).height()-160);
        $("#metas").css("height",$( window ).height()-160);        
        
        function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("csv");
        disable_tombol("delete");
        
        //$('a.media').media({width:"100%", height:400});
	    var id_lama=0;
	    var id_folder_parent_lama = 0;
	    var id_folder_child_lama = 0;                
        
        function getfile(){
	        $(".file").click(function(){
                //get folder akses
                console.log(akses);
                if(akses.indexOf('view') > -1) {
                    enable_tombol("preview");
                }
                if(akses.indexOf('update') > -1) {
                    enable_tombol("edit");
                }
                if(akses.indexOf('view') > -1) {
                    enable_tombol("download");
                }
                //enable_tombol("csv");
                if(akses.indexOf('delete') > -1) {
                    enable_tombol("delete");
                }
                
    	        if(id_lama!=0){
    	            $('#'+id_lama).removeClass("btn-primary").addClass("btn-default");
    	        }
    	        var id = $(this).attr("id");
                //console.log(id);
    	        $('#'+id).removeClass("btn-default").addClass("btn-primary");
    	        id_lama = id;
    	        getmetadata(id);
                preview_pdf(id);
                download(id);
                geteditform(id);
                
    	    });
	    }
	    getfile();	     
        

	    
        function geteditform(id)
        {
            $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/getformedit/"+id,
                //dataType: "json",
                success: function(response) {
                   //console.log(response);
                    
                    $('#edit_form_uploadfile').html(response);
                    if(id_curruent_folder==13 || id_curruent_folder==9){
                        
                        $('#edit_form_uploadfile').append('<input type="hidden" name="tipe" value="dinas" id="tipe" />');
                        $('#edit_form_uploadfile').append('<input type="hidden" name="judul" id="judul" />');
                        $('#edit_form_uploadfile').append('<input type="hidden" name="waktuM" id="waktuM" />');
                        $('#edit_form_uploadfile').append('<input type="hidden" name="waktuA" id="waktuA" />');
                        $('#edit_form_uploadfile').append('<input type="hidden" name="pelaksana" id="pelaksana" />');
                        
                    }
                    $('.tanggals').datetimepicker();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
            
        }
        function preview_pdf(id)
        {
            $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/getfilebyid/"+id,
                dataType: "json",
                success: function(response) {
                    var url = response.url;
                    if(id_curruent_folder!=27){
                        url = '<?php echo base_url(); ?>uploads/files/'+url;
                    }
                    var extension = response.url.split(".")[1];
                    var prv = "";
                    if(extension == "jpg" || extension == "jpeg" || extension == "JPEG" || extension == "JPG" || extension == "PNG" || extension =="png"){
                        prv = "<img src = '"+url+"' style='width: 100%' />";
                    } else if(extension == "PDF" || extension == "pdf"){
                        prv = '<a class="media" href='+url+'></a>';
                    } else {
                        prv = "<img src = '<?php echo base_url(); ?>/assets/images/photo.png' style='width: 100%' />";
                    }
                    $("#preview").html('<div class="modal-dialog" style="width : 900px;">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                '<h4 class="modal-title" id="simpleModalLabel">Preview</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                prv +
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                            '</div>'+
                        '</div><!-- /.modal-content -->'+
                    '</div><!-- /.modal-dialog -->');
                    
                    $(".media").attr('href', url);
                    $('a.media').media({width:"100%", height:600});
                    
                    
                    $("#delete").html('<div class="modal-dialog" style="width : 600px;">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                '<h4 class="modal-title" id="simpleModalLabel">Delete</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                "Apakah Anda Yakin Akan Menghapus File ?" +
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                                '<button type="button" class="btn btn-danger" id="delete_file_btn" data-dismiss="modal">Delete</button>'+
                            '</div>'+
                        '</div><!-- /.modal-content -->'+
                    '</div><!-- /.modal-dialog -->');
                    $(".media_delete").attr('href', url);
                    $('a.media_delete').media({width:"100%", height:400});
                    
                    $("#delete_file_btn").click(function(){
                        window.location="<?Php echo site_url("filemanagers/delete_file"); ?>/"+id;
                    });
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
            
        }
        
        function download(id)
        {
            $("#download").attr('href', '<?Php echo site_url('filemanagers/download'); ?>/'+id);
        }
        
	    function getmetadata(id){
            //console.log("<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id);
	        $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id,                
                success: function(response) {
                   //console.log(response);
                    $("#metadatadetail").html(response);
                    //this.css('font-weight','normal');
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
	    }
	    
      $('.parent').click(function(){
          var id = $(this).attr('data-id');
          disable_tombol("preview");
          disable_tombol("edit");
          disable_tombol("download");
          disable_tombol("csv");
          disable_tombol("delete");
        
          //console.log(id);
	      if(id_folder_parent_lama!=0){
	            $('.folder'+id_folder_parent_lama).removeClass("btn-primary").addClass("btn-default");
	            $('.folder'+id_folder_parent_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
                $('.folder'+id_folder_parent_lama).find('.fa').css("color","#445358");
	      }
	      $(".liparent").removeClass("active");
          $(".parent").removeClass("active");
          $("#parent"+id).addClass("active");
	      $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	      $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
          $('.folder'+id).find('.fa').css("color","white");
          //$('.folder'+id_folder_parent_lama).click();
	      //var exp = $('.folder'+id).find('[data-action=expand]').click();
	      //console.log(exp);
	      id_folder_parent_lama = id;
      });
    
      $('.child').click(function(){
          $("#berita").css("display","none");
          var id = $(this).attr('data-id');
          
          //get akses
          $.ajax({
                url: "<?Php echo site_url('directories/get_akses'); ?>/"+id,
                dataType: "json",
                success: function(response) {
                    //console.log(response);
                    akses = response['data'];
                    console.log(akses);
                }
          });
          
          enable_tombol("simpleModal");
          disable_tombol("preview");
          disable_tombol("edit");
          disable_tombol("download");
          disable_tombol("delete");
          enable_tombol("csv");
          $("#csv").attr('href', '<?Php echo site_url('filemanagers/csv'); ?>/'+id);
          console.log(id);
	        if(id_folder_child_lama!=0){
	            $('.folder'+id_folder_child_lama).removeClass("btn-primary active").addClass("btn-default");
	            $('.folder'+id_folder_child_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
	        }	              
	        $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	        $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
            $('.folder'+id).addClass('active');
	        updateform_upload_file(id);
            //updatemetadata(id);
            $('#metadatadetail').html('<div class="form-group">'+
								'<div class="col-lg-4 col-sm-3">'+
									'<label for="name" class="control-label">&nbsp;</label>'+
								'</div>'+
								'<div class="col-lg-8 col-sm-9">'+
                                    '<p><i>Please select a file</i></p>'+
								'</div>' +
							'</div>	');
            id_curruent_folder = id;
	        id_folder_child_lama = id;
            $.ajax({
                url: "<?Php echo site_url('filemanagers/getfile'); ?>/"+id,
                dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $("#datafile").html('');
                    if(data.length > 0) {                        
                        for(var i=0; i < data.length; i++){
                            var current = data[i];
                            $("#datafile").append('<tr id="'+current.id+'" class="btn-default file" ><td>'+
                                                  '<?Php echo img('/assets/images/PDF.png'); ?> '+current.nama_files+'</td>'+
                                                  '<td>'+current.created_at+'</td><td>'+current.uploader+'</td></tr>');
                        }
                    } else {
                         $("#datafile").append('<tr><td colspan="3"><i>Folder ini Kosong. Silahkan Mengungah File Terlebih Dahulu.</i></td>');
                    }
                    getfile();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
      });
        
      
    function updateform_upload_file(id)
    {
            $.ajax({
                url: "<?Php echo site_url(); ?>/filemanagers/getmetafile/"+id,
                //dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response                    
                    var data = response;
                    $('#form_uploadfile').html(data);
                    if(id_curruent_folder==13 || id_curruent_folder == 9){
                        $('#form_uploadfile').append('<input type="hidden" name="tipe" value="dinas" id="tipe" />');
                        $('#form_uploadfile').append('<input type="hidden" name="judul" id="judul" />');
                        $('#form_uploadfile').append('<input type="hidden" name="waktuM" id="waktuM" />');
                        $('#form_uploadfile').append('<input type="hidden" name="waktuA" id="waktuA" />');
                        $('#form_uploadfile').append('<input type="hidden" name="pelaksana" id="pelaksana" />');
                        
                    }
                    $('.tanggals').datetimepicker();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
    }
        
    function updatemetadata(id){
        $('#metadatadetail').html('');
        $.ajax({
                url: "<?Php echo site_url(); ?>/filemanagers/getmetafiledetaile/"+id,
                //dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $('#metadatadetail').html(data);
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
    }
	    /*
	    $('.dd-item').click(function(){
	        var id = $(this).attr('data-id');
          console.log(id);
	        //alert(id);
	        if(id_folder_lama!=0){
	            $('.folder'+id_folder_lama).removeClass("btn-primary").addClass("btn-default");
	            $('.folder'+id_folder_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
	        }
	        
	        $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	        $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
	        
	        id_folder_lama = id;
	        $.ajax({
                url: "/user/getfilelist/"+id,
                dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $("#datafile").html('');
                    for(var i=0; i < data.length; i++){
                        var current = data[i];
                        $("#datafile").append('<tr id="'+current.id+'" class="btn-default file" ><td><?Php echo img('/assets/images/PDF.png'); ?> '+current.nama_files+'</td><td>'+current.updated_at+'</td><td>'+current.uploader+'</td></tr>');
                    }
                    
                    getfile();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
	    });*/
        $( "#search_file" ).submit(function( event ) {
            event.preventDefault();
            var url = this.action;
            var data = $("#file_search").val();
            $.post( url, { str: data }, function( data ) {
                $("#datafile").html(data);
            });
        });
        
        var current = ""+<?php echo "\"".$current."\"" ?>;
        console.log(current);
        if(current!=""){
            $( ".folder"+current ).trigger( "click" );
        }
    });
    
    
    
    function getmetadata(id){
        //console.log("<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id);
        $('.file').removeClass("btn-primary").addClass("btn-default");
        $('#'+id).removeClass("btn-default").addClass("btn-primary");
        
        $.ajax({                
            url: "<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id,                
            success: function(response) {
                //console.log(response);
                $("#metadatadetail").html(response);
                $("#"+id).css('font-weight','normal');
            }
        }).done(function() {
            //$( this ).addClass( "done" );
        });
    }
    
    setInterval(function(){
        if(id_curruent_folder==13 || id_curruent_folder == 9){
            var users = $('._pelaksana:checked').map(function() {
                  return this.value;
              }).get();
            
            $('#judul').val( $('#nama_file').val() );
            $('#waktuM').val( $('#wmulai').val() );
            $('#waktuA').val( $('#wakhir').val() );
            $('#pelaksana').val( users );
            
            $('#judul_edit').val( $('#nama_file_edit').val() );
            $('#waktuM_edit').val( $('#wmulai_edit').val() );
            $('#waktuA_edit').val( $('#wakhir_edit').val() );
            
//             $('#edit_form_uploadfile').append('<input type="hidden" name="tipe" value="dinas" id="tipe" />');
//             $('#edit_form_uploadfile').append('<input type="hidden" name="judul" id="judul" />');
//             $('#edit_form_uploadfile').append('<input type="hidden" name="waktuM" id="waktuM" />');
//             $('#edit_form_uploadfile').append('<input type="hidden" name="waktuA" id="waktuA" />');
//             $('#edit_form_uploadfile').append('<input type="hidden" name="tempat" id="tempat" />');
//             $('#edit_form_uploadfile').append('<input type="hidden" name="pelaksana" id="pelaksana" />');
            
        }
    }, 30);
</script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>