<?php 
    $custom = "";
    foreach($custom_meta as $meta) {
        $custom .= '<option value="'.$meta['id'].'">'.$meta['name'].'</option>';    
    } 
?>
<script>
    var meta_count = 1;
    function remove_meta (id){
        event.preventDefault();
        console.log('remove meta_'+id);
        $( "#meta_"+id ).remove();
    }
    
    function reposition_meta(id){
            event.preventDefault();
            var num = id;
            var prev = num - 1;
            //switch position
            if(num > 1){
                $("#positions_"+num).val(prev);
                $("#positions_"+prev).val(num);
                
                var temp = $("#meta_"+num).html();
                temp = temp.replace("#meta_"+num,"#meta_"+prev);
                temp = temp.replace("metas_"+num,"metas_"+prev);
                temp = temp.replace("positions_"+num,"positions_"+prev);
                temp = temp.replace("remove_meta("+num+")","remove_meta("+prev+")");
                temp = temp.replace("reposition_meta("+num+")","reposition_meta("+prev+")");
                
                var prevs = $("#meta_"+prev).html();
                prevs = prevs.replace("#meta_"+prev,"#meta_"+num);
                prevs = prevs.replace("metas_"+prev,"metas_"+num);
                prevs = prevs.replace("positions_"+prev,"positions_"+num);
                prevs = prevs.replace("remove_meta("+prev+")","remove_meta("+num+")");
                prevs = prevs.replace("reposition_meta("+prev+")","reposition_meta("+num+")");
                
                $("#meta_"+num).html(prevs);
                $("#meta_"+prev).html(temp);
            }
    }
    
    $(".remove-meta").click(function(event){
            event.preventDefault();
            var uri = this.href;
            var id = uri.substr(uri.indexOf("#") + 1);
            console.log(id);
            $("#"+id).remove();                
        });
    
    $( document ).ready(function() {
        console.log( "document loaded" );  
        
        $("#tambah_meta").click(function(event){
            console.log(meta_count);
            meta_count++;
            event.preventDefault();
            $( "#contentmeta" ).append( $( "" +
                '<div id="meta_'+meta_count+'" style="height : 75px;">' +
                    '<div class="metadata">' +
                        '<div class="col-lg-3 col-sm-2">' +
                            '<label for="selector" class="control-label">Nama Metadata</label>' +
                        '</div>' +
                        '<div class="col-lg-8 col-sm-9">' +
                            '<input type="text" name="nama_meta[]" class="form-control" ' +
                                'laceholder="Nama Metadata, Maksimal 50 Karakter" required data-rule-minlength="2" max-length="50">' +
                             '<input type="hidden" name="position[]" '+
                                'id="positions_'+meta_count+'" value="'+meta_count+'" />'+
                        '</div>' +
                    '</div>' +
                    '<div class="metadata">' +
                        '<div class="col-lg-3 col-sm-2">' +
                            '<label for="selector" class="control-label">Tipe Data</label>' +
                        '</div>' +
                        '<div class="col-lg-8 col-sm-9">' +
                            '<select name="tipe_meta[]" id="selector" class="form-control" required>' +
                                '<option value="integer">Integer</option>' +
                                '<option value="string">String</option>'+
                                '<option value="date">Date</option>'+
                                '<option value="datetime">Datetime</option>'+
                                '<option value="user">List User</option>'+
                                '<?php echo $custom; ?>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="action">'+
                        '<a href="#meta_'+meta_count+'" class="btn btn-xs btn-danger btn-equal remove-meta" '+
                            'onClick="remove_meta('+meta_count+')" data-toggle="tooltip" data-placement="top" data-original-title="Delete">'+
                          '<i class="fa fa-trash-o"></i></a>'+    
                        '<a id="metas_'+meta_count+'" class="btn btn-xs btn-danger btn-equal reposition_meta" '+
                            'onClick="reposition_meta('+meta_count+')" data-toggle="tooltip" data-placement="top" '+
                                'style="background-color : blue;border-color:white;">'+
                                        '<i class="fa fa-arrow-up"></i></a>' +
                    '</div>'+
                '</div>'+
            "" ) );
        });
    });
</script>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li><a href="<?php echo base_url() ?>index.php/directories/"><i class="fa fa-fw fa-folder"></i> Directory</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-pencil"></i> Edit Folder</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Tambah Folder
	</h3>
</div>
<div class="row" >
	<div class="col-lg-12">
        <div class="box">
            <div class="col-lg-8">
                <form action="<?php echo base_url() ?>index.php/directories/update" method="post" 
                      class="form-horizontal form-bordered form-validate">
    				<div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Nama Folder</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nama_folder" id="name" class="form-control" value="<?php echo $folder['nama_folder'] ?>"
                                   placeholder="Nama Folder, Maksimal 50 Karakter" 
                                   required data-rule-minlength="2" max-length="50">
                            <input type="hidden" name="id" value="<?php echo $folder['id'] ?>"/>
    					</div>
    				</div>
    				<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Parent</label>
						</div>
						<div class="col-lg-9 col-sm-10">
							<select name="parent" id="selector" class="form-control" required>
                                <option value="0">-- Pilih Parent Folder --</option>
                                <?php foreach($parents as $pr) { ?>
								<option value="<?php echo $pr['id'] ?>" <?php if($pr['id']==$folder['parent_id']){echo "selected";} ?>>
                                    <?php echo $pr['nama_folder'] ?></option>								
                                <?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Hak Akses</label>
						</div>
						<div class="col-md-5 col-sm-5">
                            <div style="margin-right : 20px; float : left;">                                
                                <input type="checkbox" name="hak_akses[]" value="view" 
                                       <?php if(strpos(strtolower($folder['hak_akses']),'iew') != false) { echo 'checked="checked"';} ?>
                                       style="margin-top : 5px;"> View
                            </div>
                            <div style="margin-right : 20px; float : left;">
    						    <input type="checkbox" name="hak_akses[]" value="update"
                                       <?php if(strpos(strtolower($folder['hak_akses']),'update') != false) { echo 'checked="checked"';} ?>
                                       style="margin-top : 5px;"> Update
                            </div>
                            <div style="margin-right : 20px; float : left;">
							    <input type="checkbox" name="hak_akses[]" value="delete" 
                                       <?php if(strpos(strtolower($folder['hak_akses']),'delete') != false) { echo 'checked="checked"';} ?>
                                       style="margin-top : 5px;"> Delete
                            </div>
						</div>
					</div>
					<div class="form-group">
					    <h5>Pengguna Folder</h5>
					    <div class="dd nestable-list col-lg-12" style="float:left; max-height : 300px; overflow-y : auto">
							<ol class="dd-list">
                                <?php foreach($organisasi as $org) {
                                        $users = $this->organisasi->get_member($org['id']);
                                        if(sizeof($users) > 0) {    
                                    ?>
                                        <li class="dd-item" data-id="<?php echo $org['id'] ?>">                                    
                                            <div data-toggle="buttons" class="btn btn-checkbox-gray-inverse" style="float : left; margin : 0px;">
                                                <input type="checkbox" class="check_organisasi" value="<?php echo $org['id'] ?>" > 
                                            </div>                                    
                                            <div class="dd-handle btn btn-default" style="background-color : #c5c5c5">
                                                <?php echo $org['nama_organisasi'] ?>
                                            </div>
                                            <ol class="dd-list" style="">
                                                <?php                                                 
                                                    foreach($users as $us) { ?>
                                                        <li class="dd-item" data-id="<?php echo $org['id'].'564' ?> ">
                                                            <div style="float : left; margin-left : 5px; margin-right : 5px;margin-top:5px;">
                                                                <input type="checkbox" class="organisasi_<?php echo $org['id'] ?>"
                                                                       <?php 
                                                                            if($this->folder->is_user($folder['id'],$us['id'])){
                                                                                echo "checked";
                                                                            }
                                                                       ?>
                                                                       name="user_akses[]" value="<?php echo $us['id'] ?>">
                                                            </div>
                                                            <div class="dd-handle btn btn-default"><?php echo $us['nama_lengkap'] ?></div>
                                                        </li>
                                                <?php } ?>                                                
                                            </ol>                                           
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ol>							    						   
					    </div>
                    </div>
					<div class="form-group">
					    <h5>Metadata</h5>
                         <?php if($folder['status']=='unchangeable'){ ?> 
                            <p style="text-align:center; color:red">
                                <i>Karena Alasan Teknis, Metadata Folder Tidak Dapat Dihapus atau Diubah</i></p>
                        <?php } ?>
					    <div id="contentmeta" style="max-height : 300px; overflow-y : auto;">
                            <?php 
                                $count = 0;
                                $metadata = $this->folder->get_metas($folder['id']);
                                if(sizeof($metadata) > 0) {                                
                                    foreach($metadata as $meta) {
                                        $count++
                                ?>
                            <div id="meta_<?php echo $count?>" style="height : 75px;">
                                <div class="metadata">
                                    <div class="col-lg-3 col-sm-2">
                                        <label for="selector" class="control-label">Nama Metadata</label>
                                    </div>
                                    <div class="col-lg-8 col-sm-9">
                                        <input type="text" name="name_<?php echo $meta['id'] ?>" class="form-control"
                                               value="<?php echo $meta['nama'] ?>"
                                               placeholder="Nama Metadata, Maksimal 50 Karakter" required data-rule-minlength="2" max-length="50"
                                               <?php if($folder['status']=='unchangeable'){ echo "readonly"; } ?> >
                                        <input type="hidden" name="position_<?php echo $meta['id']?>" 
                                               id="positions_<?php echo $count ?>" value="<?php echo $count ?>" />
                                    </div>
                                </div>
                                <div class="metadata">
                                    <div class="col-lg-3 col-sm-2">
                                        <label for="selector" class="control-label">Tipe Data</label>
                                    </div>
                                    <div class="col-lg-8 col-sm-9">
                                        <select name="type_<?php echo $meta['id'] ?>" id="selector" class="form-control" required
                                                <?php if($folder['status']=='unchangeable'){ echo "readonly"; } ?>>
                                            <option value="integer" <?php if($meta['type']=='integer') { echo "selected"; } ?>>Integer</option>
                                            <option value="string" <?php if($meta['type']=='string') { echo "selected"; } ?>>String</option>
                                            <option value="date" <?php if($meta['type']=='date') { echo "selected"; } ?>>Date</option>
                                            <option value="datetime" <?php if($meta['type']=='datetime') { echo "selected"; } ?>>Datetime</option>
                                            <option value="user"  <?php if($meta['type']=='user') { echo "selected"; } ?>>List User</option>
                                            <?php
                                                foreach($custom_meta as $mt) {
                                                    $option = '<option value="'.$mt['id'].'" ';
                                                    if($meta['type']==(string)$mt['id']) {
                                                        $option .= "selected";
                                                    }
                                                    $option .= '>'.$mt['name'].'</option>';
                                                    echo $option;
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="action">
                                    <?php if($folder['status'] !='unchangeable'){ ?>
                                    <a href="#meta_<?php echo $count?>" class="btn btn-xs btn-danger btn-equal remove-meta"  data-toggle="tooltip" 
                                           onClick="remove_meta(<?php echo $count?>)" data-placement="top" data-original-title="Delete">
                                        <i class="fa fa-trash-o"></i></a>
                                    <?php } ?>
                                    <a id="metas_<?php echo $count?>" class="btn btn-xs btn-danger btn-equal reposition_meta" 
                                           onClick="reposition_meta(<?php echo $count?>)" data-toggle="tooltip" data-placement="top" 
                                           style="background-color : blue;border-color:white;">
                                        <i class="fa fa-arrow-up"></i></a>
                                </div>                                
                            </div>
                            <?php ; 
                                    }
                                }
                                echo "<script>meta_count=$count</script>";
                            ?>
						</div>
						<br/>
						<a id="tambah_meta" class="btn btn-danger btn-xs" 
                           style="float:right; margin-right : 80px;margin-top:10px;">Tambah Metadata</a>
					</div>
					<div class="form-footer col-lg-offset-3 col-sm-offset-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
    			</form>
			</div>
        </div>
    </div>
</div>
<script>
    $(".table-user").dataTable({
       "sPaginationType": "full_numbers" 
    });
    $(".check_organisasi").change(function () {        
        id = this.value;       
        console.log(".organisasi_"+id);
        if(this.checked) {
            //$(".organisasi_"+id).prop('checked', true);
            $(".organisasi_"+id).attr('checked','checked');
        } else {
            //$(".organisasi_"+id).prop('checked', false);
            $(".organisasi_"+id).removeAttr('checked');
        }
    });
</script>