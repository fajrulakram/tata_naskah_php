<?php 
    $custom = "";
    foreach($custom_meta as $meta) {
        $custom .= '<option value="'.$meta['id'].'">'.$meta['name'].'</option>';    
    } 
?>
<script>
    var meta_count = 1;
    function remove_meta (id){
        event.preventDefault();
        console.log('remove meta_'+id);
        $( "#meta_"+id ).remove();
    }
    
    $(".remove-meta").click(function(event){
            event.preventDefault();
            var uri = this.href;
            var id = uri.substr(uri.indexOf("#") + 1);
            console.log(id);
            $("#"+id).remove();                
        });
    $( document ).ready(function() {
        console.log( "document loaded" );        
        $("#tambah_meta").click(function(event){
            console.log(meta_count);
            meta_count++;
            event.preventDefault();
            $( "#contentmeta" ).append( $( "" +
                '<div id="meta_'+meta_count+'" style="height : 75px;">' +
                    '<div class="metadata">' +
                        '<div class="col-lg-3 col-sm-2">' +
                            '<label for="selector" class="control-label">Nama Metadata</label>' +
                        '</div>' +
                        '<div class="col-lg-8 col-sm-9">' +
                            '<input type="text" name="nama_meta[]" class="form-control" ' +
                                'laceholder="Nama Metadata, Maksimal 50 Karakter" required data-rule-minlength="2" max-length="50">' +
                        '</div>' +
                    '</div>' +
                    '<div class="metadata">' +
                        '<div class="col-lg-3 col-sm-2">' +
                            '<label for="selector" class="control-label">Tipe Data</label>' +
                        '</div>' +
                        '<div class="col-lg-8 col-sm-9">' +
                            '<select name="tipe_meta[]" id="selector" class="form-control" required>' +
                                '<option value="integer">Integer</option>' +
                                '<option value="string">String</option>'+
                                '<option value="date">Date</option>'+
                                '<option value="datetime">Datetime</option>'+
                                '<option value="user">List User</option>'+
                                '<?php echo $custom; ?>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="action">'+
                        '<a href="#meta_'+meta_count+'" class="btn btn-xs btn-danger btn-equal remove-meta" '+
                            'onClick="remove_meta('+meta_count+')" data-toggle="tooltip" data-placement="top" data-original-title="Delete">'+
                          '<i class="fa fa-trash-o"></i></a>'+
                    '</div>'+
                '</div>'+
            "" ) );
        });
    });
</script>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li><a href="<?php echo base_url() ?>index.php/directories/"><i class="fa fa-fw fa-folder"></i> Directory</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-pencil"></i> Folder Baru</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Tambah Folder
	</h3>
</div>
<div class="row" >
	<div class="col-lg-12">
        <div class="col-lg-8 box box-outlined" style="margin-left : 50px;">
            <div class="col-lg-12">
                <form action="<?php echo base_url() ?>index.php/directories/create" method="post" 
                      class="form-horizontal form-bordered form-validate">
    				<div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Nama Folder</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nama_folder" id="name" class="form-control" placeholder="Nama Folder, Maksimal 50 Karakter" 
                                   required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>
    				<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Parent</label>
						</div>
						<div class="col-lg-9 col-sm-10">
							<select name="parent" id="selector" class="form-control" required>
                                <option value="0">-- Pilih Parent Folder --</option>
                                <?php foreach($parents as $pr) { ?>
								<option value="<?php echo $pr['id'] ?>"><?php echo $pr['nama_folder'] ?></option>								
                                <?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Hak Akses</label>
						</div>
						<div class="col-md-5 col-sm-5">
                            <div style="margin-right : 20px; float : left;">
                                <input type="checkbox" name="hak_akses[]" value="view" style="margin-top : 5px;" checked="checked"> View
                            </div>
                            <div style="margin-right : 20px; float : left;">
    						    <input type="checkbox" name="hak_akses[]" value="update" style="margin-top : 5px;" checked="checked"> Update
                            </div>
                            <div style="margin-right : 20px; float : left;">
							    <input type="checkbox" name="hak_akses[]" value="delete" style="margin-top : 5px;" checked="checked"> Delete
                            </div>
						</div>
					</div>
					<div class="form-group">
					    <h5>Pengguna Folder</h5>
					    <div class="dd nestable-list col-lg-12" style="float:left; max-height : 300px; overflow-y : auto">
							<ol class="dd-list">
                                <?php foreach($organisasi as $org) {
                                        $users = $this->organisasi->get_member($org['id']);
                                        if(sizeof($users) > 0) {    
                                    ?>
                                        <li class="dd-item" data-id="<?php echo $org['id'] ?>">                                    
                                            <div data-toggle="buttons" class="btn btn-checkbox-gray-inverse" style="float : left; margin : 0px;">
                                                <input type="checkbox" class="check_organisasi" value="<?php echo $org['id'] ?>" > 
                                            </div>                                    
                                            <div class="dd-handle btn btn-default" style="background-color : #c5c5c5">
                                                <?php echo $org['nama_organisasi'] ?>
                                            </div>
                                            <ol class="dd-list" style="">
                                                <?php                                                 
                                                    foreach($users as $us) { ?>
                                                        <li class="dd-item" data-id="<?php echo $org['id'].'564' ?> ">
                                                            <div style="float : left; margin-left : 5px; margin-right : 5px;margin-top:5px;">
                                                                <input type="checkbox" class="organisasi_<?php echo $org['id'] ?>" 
                                                                       name="user_akses[]" value="<?php echo $us['id'] ?>">
                                                            </div>
                                                            <div class="dd-handle btn btn-default"><?php echo $us['nama_lengkap'] ?></div>
                                                        </li>
                                                <?php } ?>                                                
                                            </ol>                                           
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ol>							    						   
					    </div>
                    </div>
					<div class="form-group">
					    <h5>Metadata</h5>
					    <div id="contentmeta" style="max-height : 300px; overflow-y : auto;">
                            <div id="meta_1" style="height : 75px;">
                                <div class="metadata">
                                    <div class="col-lg-3 col-sm-2">
                                        <label for="selector" class="control-label">Nama Metadata</label>
                                    </div>
                                    <div class="col-lg-8 col-sm-9">
                                        <input type="text" name="nama_meta[]" class="form-control"
                                            placeholder="Nama Metadata, Maksimal 50 Karakter" required data-rule-minlength="2" max-length="50">
                                    </div>
                                </div>
                                <div class="metadata">
                                    <div class="col-lg-3 col-sm-2">
                                        <label for="selector" class="control-label">Tipe Data</label>
                                    </div>
                                    <div class="col-lg-8 col-sm-9">
                                        <select name="tipe_meta[]" id="selector" class="form-control" required>
                                            <option value="integer">Integer</option>
                                            <option value="string">String</option>
                                            <option value="date">Date</option>
                                            <option value="datetime">Datetime</option>
                                            <option value="user">List User</option>
                                            <?php echo $custom; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="action">
                                    <a href="#meta_1'" class="btn btn-xs btn-danger btn-equal remove-meta"
                                        onClick="remove_meta(1)" data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                      <i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
                            <!--generated-->
						</div>
						<br/>
						<a id="tambah_meta" class="btn btn-danger btn-xs" 
                           style="float:right; margin-right : 80px;margin-top:10px;">Tambah Metadata</a>
					</div>
					<div class="form-footer col-lg-offset-3 col-sm-offset-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
    			</form>
			</div>
        </div>
    </div>
</div>
<script>
    $(".table-user").dataTable({
       "sPaginationType": "full_numbers" 
    });
    $(".check_organisasi").change(function () {        
        id = this.value;       
        console.log(".organisasi_"+id);
        if(this.checked) {
            //$(".organisasi_"+id).prop('checked', true);
            $(".organisasi_"+id).attr('checked','checked');
        } else {
            //$(".organisasi_"+id).prop('checked', false);
            $(".organisasi_"+id).removeAttr('checked');
        }
    });
</script>