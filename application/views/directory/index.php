<ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-folder"></i> Directory</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Data<strong>Directory</strong></h4></header>
    		</div>
    		<div class="row">
        		<div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url()?>index.php/directories/add" class="btn btn-outline btn-primary btn-block btn-labeled">
    					<span><i class="fa fa-folder"></i></span>
    					<div>Tambah Folder</div>
    				</a>
    		    </div>
		    </div>
        	<div class="box-body table-responsive">
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>
        					<th>ID</th>
        					<th>Parent Folder</th>
        					<th>Folder</th>
                            <th style="width : 400px;">Pengakses</th>
        					<th>Hak Akses</th>
        					<th>Aksi</th>
        				</tr>
        			</thead>
        	    	<tbody>
        				<!-- DAFTAR FOLDER -->
                        <?php 
                            $id = 1;
                            foreach($folder as $fd) { ?>
                                <tr>
                                    <td><?php echo $id ?></td>
                                    <td><?php echo $fd['parent'] ?></td>
                                    <td><?php echo $fd['nama_folder'] ?></td>
                                    <td>
                                        <?php 
                                            $user = $this->folder->get_user_folder($fd['id']);
                                            if(sizeof($user)>0) {
                                                foreach($user as $u) {
                                                    echo $u['nama_lengkap']."; ";
                                                }
                                            }
                                        ?>
                                    </td>
                                    <td><?php 
                                            $akses = explode(",",$fd['hak_akses']);
                                            for($i=0;$i<sizeof($akses);$i++) {
                                                echo strtoupper($akses[$i]);
                                                if($i < sizeof($akses)-1) {
                                                    echo ", ";
                                                }
                                            }
                                        ?></td>
                                    <td>
                                        <?php if($fd['status']=="changeable")  {?>
                                        <a href="<?php echo base_url()?>index.php/directories/edit/<?php echo $fd['id'] ?>" 
                                           type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" 
                                                data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                        <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" data-placement="top" 
                                                onclick="openmodalframe('<?php echo base_url() ?>index.php/directories/delete/<?php echo $fd['id'] ;?>', 'Delete Folder')" 
                                                data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                        <?php } else { ?>
                                        <a href="<?php echo base_url()?>index.php/directories/edit/<?php echo $fd['id'] ?>" 
                                           type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" 
                                                data-original-title="Edit"><i class="fa fa-pencil"></i></a>   
                                        <p style="float:right;word-wrap:break-word;width:50px;"><i>Folder Tidak Dapat Dihapus</i></p>
                                        <?php } ?>
                                    </td>
                                </tr>
                        <?php $id++; } ?>
        			</tbody>
        		</table>
        	</div>
        </div>
	</div>
</div>
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px">
        <div class="modal-content" style="width:100%">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    $("#datatable1").dataTable({        
    });
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #user-info');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
</script>