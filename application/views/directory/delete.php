<div id="user-info" style="margin-top:0px; background-color : white; width:auto;">
    <div class="user-avatar" style="height: 100px; width:auto;">
        <div style="background-color : white; color:black; font-size:18px; font-weight : bold; width:auto;">
            Apakah Anda Yakin Akan Menghapus Folder <?php echo $folder['nama_folder'] ?> ? <br/>
            <?php if($folder['jumlah_file']>0) { ?>
            <i>Anda juga akan menghapus <?php echo $folder['jumlah_file'] ?> data didalamnya</i>
            <?php } else { ?>
            <i>Folder Kosong</i>
            <?php } ?>
        </div>        
    </div>    
    <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px; width:auto;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url() ?>index.php/directories/do_delete/<?php echo $folder['id'] ?>" 
           type="button" class="btn btn-warning">Delete</a>
    </div>
</div>