<script>
    var meta_count = 1;
    function remove_meta (id){
        event.preventDefault();
        console.log('remove meta_'+id);
        $( "#meta_"+id ).remove();
    }
    
    $(".remove-meta").click(function(event){
            event.preventDefault();
            var uri = this.href;
            var id = uri.substr(uri.indexOf("#") + 1);
            console.log(id);
            $("#"+id).remove();                
        });
    $( document ).ready(function() {
        console.log( "document loaded" );        
        $("#tambah_meta").click(function(event){
            console.log(meta_count);
            meta_count++;
            event.preventDefault();
            $( "#contentmeta" ).append( $( "" +
                '<div id="meta_'+meta_count+'" style="height : 40px;">' +
                    '<div class="metadata">' +
                        '<div class="col-lg-3 col-sm-2">' +
                            '<label for="selector" class="control-label">Value Meta</label>' +
                        '</div>' +
                        '<div class="col-lg-8 col-sm-9">' +
                            '<input type="text" name="value_meta[]" class="form-control" ' +
                                'placeholder="Value Metadata, Maksimal 50 Karakter" required data-rule-minlength="2" max-length="50">' +
                        '</div>' +
                    '</div>' +                    
                    '<div class="action">'+
                        '<a href="#meta_'+meta_count+'" class="btn btn-xs btn-danger btn-equal remove-meta" '+
                            'onClick="remove_meta('+meta_count+')" data-toggle="tooltip" data-placement="top" data-original-title="Delete">'+
                          '<i class="fa fa-trash-o"></i></a>'+
                    '</div>'+
                '</div>'+
            "" ) );
        });
    });
</script>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li><a href="<?php echo base_url() ?>index.php/metas/"><i class="fa fa-fw fa-code"></i> Metadata</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-pencil"></i> Tambah Meta Data</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Tambah Folder
	</h3>
</div>
<div class="row" >
	<div class="col-lg-12">
        <div class="box">
            <div class="col-lg-8">
                <form action="<?php echo base_url() ?>index.php/metas/create" method="post" 
                      class="form-horizontal form-bordered form-validate">
    				<div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Nama Meta Data</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nama_meta" id="name" class="form-control" 
                                   placeholder="Nama Meta Data, Maksimal 50 Karakter" 
                                   required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>    														
					<div class="form-group">
					    <h5>Value Metadata</h5>
					    <div id="contentmeta" style="max-height : 300px; overflow-y : auto;">
                            <div id="meta_1" style="height : 40px;">
                                <div class="metadata">
                                    <div class="col-lg-3 col-sm-2">
                                        <label for="selector" class="control-label">Value Meta</label>
                                    </div>
                                    <div class="col-lg-8 col-sm-9">
                                        <input type="text" name="value_meta[]" class="form-control" 
                                               placeholder="Value Metadata, Maksimal 50 Karakter" 
                                               required data-rule-minlength="2" max-length="50">
                                    </div>
                                </div>                                
                                <div class="action">
                                    <a href="#meta_1" class="btn btn-xs btn-danger btn-equal remove-meta"  data-toggle="tooltip" 
                                           onClick="remove_meta(1)" data-placement="top" data-original-title="Delete">
                                        <i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
						</div>
						<br/>
						<a id="tambah_meta" class="btn btn-danger btn-xs" 
                           style="float:right; margin-right : 80px;margin-top:10px;">Tambah Value Metadata</a>
					</div>
					<div class="form-footer col-lg-offset-3 col-sm-offset-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
    			</form>
			</div>
        </div>
    </div>
</div>
<script>
    $(".table-user").dataTable({
       "sPaginationType": "full_numbers" 
    });
    $(".check_organisasi").change(function () {        
        id = this.value;       
        console.log(".organisasi_"+id);
        if(this.checked) {
            //$(".organisasi_"+id).prop('checked', true);
            $(".organisasi_"+id).attr('checked','checked');
        } else {
            //$(".organisasi_"+id).prop('checked', false);
            $(".organisasi_"+id).removeAttr('checked');
        }
    });
</script>