<ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-code"></i> Metadata</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Meta<strong>Data</strong></h4></header>
    		</div>
    		<div class="row">
        		<div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url()?>index.php/metas/add" class="btn btn-outline btn-primary btn-block btn-labeled">
    					<span><i class="fa fa-code"></i></span>
    					<div>Tambah Meta Data</div>
    				</a>
    		    </div>
		    </div>
        	<div class="box-body table-responsive">
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>
        					<th>ID</th>
        					<th>Nama Meta</th>
        					<th>Value</th>                                    					
        					<th>Aksi</th>
        				</tr>
        			</thead>
        	    	<tbody>
        				<!-- DAFTAR FOLDER -->
                        <?php 
                            $id = 1;
                            foreach($metas as $mt) { ?>
                                <tr>
                                    <td><?php echo $id ?></td>
                                    <td><?php echo $mt['name'] ?></td>
                                    <td>
                                        <?php 
                                            $query = $this->meta->get_value($mt['id']);
                                            $value = array();
                                            foreach($query as $q){
                                                $value[sizeof($value)] = $q['value'];
                                            }
                                                    
                                            echo json_encode($value);
                                        ?>
                                    </td>                                                                        
                                    <td>
                                        <a href="<?php echo base_url()?>index.php/metas/edit/<?php echo $mt['id'] ?>" 
                                           type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" 
                                                data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                        <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" data-placement="top" 
                                                onclick="openmodalframe('<?php echo base_url() ?>index.php/metas/delete/<?php echo $mt['id'] ;?>', 'Delete Metadata')" 
                                                data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                        <?php $id++; } ?>
        			</tbody>
        		</table>
        	</div>
        </div>
	</div>
</div>
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px">
        <div class="modal-content" style="width:100%">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    $("#datatable1").dataTable({        
    });
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #user-info');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
</script>