<div id="user-info" style="margin-top:0px; background-color : white; width:auto;">
    <div class="user-avatar" style="height: 100px; width:auto;">
        <div style="background-color : white; color:black; font-size:18px; font-weight : bold; width:auto;">
            Apakah Anda Yakin Akan Menghapus Metadata <?php echo $meta['name'] ?> ? <br/>
            <?php if($meta['jumlah_folder']>0) { ?>
            <i>Terdapat <?php echo $meta['jumlah_folder'] ?> Folder yang menggunakan meta ini</i>
            <?php } else { ?>
            <i>Meta tidak digunakan</i>
            <?php } ?>
        </div>        
    </div>    
    <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px; width:auto;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url() ?>index.php/metas/do_delete/<?php echo $meta['id'] ?>" 
           type="button" class="btn btn-warning">Delete</a>
    </div>
</div>