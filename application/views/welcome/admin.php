<?php 
    $months = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
?>
<ol class="breadcrumb">
	<li class="active"><a href="#"><i class="fa fa-fw fa-home"></i> Home</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Dashboard <small>Selamat Datang <?php echo $this->session->userdata('fullname') ?></small>
	</h3>
</div>

<!-- DATA USER -->
<div class="row" >
	<div class="col-lg-12">
	    <div id="user" class="col-lg-6" style="float : left;">
            <div class="box">
    		    <div class="box-head">
    			    <header><h4 class="text-light">Data<strong>User</strong></h4></header>
    			</div>
        		<div class="box-body table-responsive">
        		    <a href="<?php echo base_url() ?>index.php/users/" class="btn btn-info btn-xs" 
                       style="float : right; margin-bottom : 10px;">Lihat Selengkapnya</a>
        			<table class="table table-bordered table-hover">
        				<thead>
        					<tr>
        					    <th>Username</th>
        						<th>Nama Lengkap</th>
        						<th>Organisasi</th>
        					</tr>
        				</thead>
        		    	<tbody>
        					<!-- ISI LOG -->        					
        					<?php
        						foreach($user as $us) { ?>
        							<tr>
		        				        <td><?php echo $us['username'] ?></td>
		        					    <td><?php echo $us['nama_lengkap'] ?></td>
		        					    <td><?php echo $us['nama_organisasi'] ?></td>
		        					</tr>	
        					<?php } ?>        					
        				</tbody>
        			</table>
        		</div>
        	</div>
		</div>
	    <div class="col-lg-6" style="float : right;">
            <div class="box">
    	        <div class="box-head">
    		        <header><h4 class="text-light">Data<strong>Folder</strong></h4></header>
    		    </div>
        	    <div class="box-body table-responsive">
        	            <a href="<?php echo base_url()?>index.php/directories" class="btn btn-info btn-xs" 
                           style="float : right; margin-bottom : 10px;">Lihat Selengkapnya</a>
        			<table class="table table-bordered table-hover">
        				<thead>
        					<tr>
        					    <th>Waktu Pembuatan</th>
                                <th>Parent</th>
        						<th>Nama Folder</th>                                
        					</tr>
        				</thead>
        		    	<tbody>
        					<!-- ISI FOLDER -->
                            <?php foreach ($folder as $fd) { ?>
        			    	<tr>
        				        <td><?php 
                                        $datetime = explode(" ",$fd['created_at']);
                                        $date = $datetime[0];
                                        $time = $datetime[1];
                                        $detail = explode("-",$date);
                                        $tahun = $detail[0];
                                        $bulan = $months[(int)$detail[1]-1];
                                        $tanggal = $detail[2];
                                        echo $tanggal." ".$bulan." ".$tahun." ".$time;
                                    ?>
                                </td>
                                <td><?php echo $fd['parent'] ?></td>
        					    <td><?php echo $fd['nama_folder'] ?></td>
        					</tr>
                            <?php } ?>
        				</tbody>
        			</table>
        		</div>
        	</div>
    	</div>
    </div>
</div>
<!-- END DATA USER -->

<!-- DATA LOG -->
<div class="row">
	<div class="col-lg-12">
        <div class="box">
		    <div class="box-head">
			    <header><h4 class="text-light">Data<strong>Log</strong></h4></header>
			</div>
		<div class="box-body table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Waktu Log</th>
                        <th>User</th>
						<th>Aktivitas</th>
					</tr>
				</thead>
		    	<tbody>
					<!-- ISI LOG -->
                    <?php foreach($logs as $l){ ?>
			    	<tr>
				        <td><?php echo date('d-M-Y H:i:s', strtotime($l['waktu'])) ?></td>
					    <td><?php echo $l['nama_lengkap'] ?></td>
                        <td><?php echo $l['kegiatan'] ?></td>
					</tr>					
                    <?php } ?>
				</tbody>
			</table>
		</div><!--end .box-body -->
	</div><!--end .box -->
</div><!--end .col-lg-12 -->
<!-- END DATA LOG -->