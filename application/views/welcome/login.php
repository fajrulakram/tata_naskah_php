<!DOCTYPE html>
<html lang="en">

	<head>
		<title>Tata Naskah Ditjen Migas - Login</title>
		
		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
		<link href='<?php echo base_url() ?>assets/stylesheets/bootstrap.css@1401441895.css' rel='stylesheet' type='text/css'/>		
		<link href='<?php echo base_url() ?>assets/stylesheets/boostbox.css@1401441893.css' rel='stylesheet' type='text/css'/>
		<link href='<?php echo base_url() ?>assets/stylesheets/boostbox_responsive.css@1401441893.css' rel='stylesheet' type='text/css'/>
		<link href='<?php echo base_url() ?>assets/stylesheets/font-awesome.min.css@1401441895.css' rel='stylesheet' type='text/css'/>
		<link href='<?php echo base_url() ?>assets/stylesheets/libs/jquery-ui/jquery-ui-boostbox.css@1401442122.css' 
              rel='stylesheet' type='text/css'/>
		<link href='<?php echo base_url() ?>assets/stylesheets/libs/fullcalendar/fullcalendar.css@1401442122.css' 
              rel='stylesheet' type='text/css'/>			
		<!-- END STYLESHEETS -->	

		<style>
			body {
				overflow : hidden;
				overflow-x : hidden;
				overflow-y : hidden;
			}
			
			.Logo {
				height : 65px;
			}
		</style>
	</head>	
					
	<body class="body-dark">
	
		<!-- START LOGIN BOX -->
		<div class="box-type-login" >
			<div class="box text-center" >
				<div class="box-head" style="height : 335px; top:-300px;">
					<center style="margin-top: 0px;">
						<img class="Logo" src="<?php echo base_url() ?>assets/images/Logo_ESDM.png" />
					</center>
					<h2 class="text-light text-white" style="font-size : 28px; margin-top : 5px; margin-bottom :10px;">
                        Aplikasi Sistem Administrasi<strong>Pengangkutan Migas</strong></h2>                    
					<h4 class="text-light text-inverse-alt" style="font-size : 15px; margin-top : 0px;">
                        <p style="margin-top:5px; margin-bottom:0px;">Sub Direktorat Pengangkutan Migas</p>
                        <p style="margin-top:5px; margin-bottom:0px;">Direktorat Jenderal Minyak dan Gas Bumi</p>
                        <p style="font-size: 18px; margin-top:5px; font-weight : bold; color : #ffffff">
                            Kementerian Energi dan Sumber Daya Mineral Republik Indonesia<p></h4>
				</div>
				<div class="box-body box-centered style-inverse" style="height : 240px">
                    <h5 style="color : red"><?php echo $this->session->flashdata('message'); ?></h5>
					<h2 class="text-light" style="margin-top:0px">Sign in to your account</h2>
					<br/>
					<form action="<?php echo base_url() ?>index.php/session/create/" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" class="form-control" name="username" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control" name="password" placeholder="Password">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 text-left">
								<div data-toggle="buttons">
									<label class="btn checkbox-inline btn-checkbox-primary-inverse">
										<input type="checkbox" value="default-inverse1"> Remember me
									</label>
								</div>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-primary" type="submit"><i class="fa fa-key"></i> Sign in</button>
							</div>
						</div>
					</form>
				</div><!--end .box-body -->
				<div class="box-footer force-padding text-white">
					<a class="text-primary-alt" href="#">No account yet?</a> Or did you 
					<a class="text-primary-alt" href="#">forgot your password?</a>
				</div>
			</div>
		</div>
		<!-- END LOGIN BOX -->


	<!-- SCRIPT -->
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-1.11.0.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery/jquery-migrate-1.2.1.min.js"></script>	
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/BootstrapFixed.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/bootstrap/bootstrap.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/jquery.dataTables.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/ColVis/js/ColVis.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/libs/DataTables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script data-turbolinks-track="true" src="<?php echo base_url() ?>/assets/javascripts/App.js"></script>			
	<!-- END SCRIPT -->
	
	</body>
</html>