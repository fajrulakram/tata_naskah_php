<div id="user-info" style="margin-top:0px; background-color : white;">
    <div class="user-avatar" style="height: 100px">
        <div style="background-color : white; color:black; font-size:18px; font-weight : bold;">
            Apakah Anda Yakin Akan Menghapus Organisasi <?php echo $org['nama_organisasi'] ?> ?
        </div>        
    </div>    
    <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url() ?>index.php/organisasis/do_delete/<?php echo $org['id'] ?>" type="button" class="btn btn-warning">Delete</a>
    </div>
</div>