<style>
    #message {
        -webkit-animation-duration: 3s;
        -webkit-animation-delay: 2s;  
        -moz-animation-duration: 3s;
        -moz-animation-delay: 2s;  
    }
</style>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-cogs"></i> Organisasi</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Data<strong>Organisasi</strong></h4></header>
    		</div>
    		<div class="row">
        		<div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url()?>index.php/organisasis/add" class="btn btn-outline btn-primary btn-block btn-labeled">
    					<span><i class="fa fa-gears"></i></span>
    					<div>Tambah Organisasi</div>
    				</a>
    		    </div>
		    </div>
        	<div class="box-body table-responsive">
                 <center id="message" class="animated fadeOut" style="color: red; font-style:italic;">
                    <?php echo $this->session->flashdata('message'); ?>
                </center>
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>
        					<th>ID</th>
        					<th>Parent</th>
        					<th>Nama Organisasi</th>
        					<th>Aksi</th>
        				</tr>
        			</thead>
        	    	<tbody>
        				<!-- DAFTAR ORGANISASI -->
        				<?php  $id = 1;
        				    foreach($organisasi as $org) { ?>
        		    	<tr>
        			        <td><?php echo $id ?></td>
        				    <td><?php echo $org['parent'] ?></td>
        				    <td><?php echo $org['nama_organisasi'] ?></td>
        				    <td>
        				        <a href="<?php echo base_url() ?>index.php/organisasis/edit/<?php echo $org['id'] ?>" 
                                    type="button" class="btn btn-xs btn-inverse btn-equal" 
                                    data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
								<button  
                                   type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip"
                                        onclick="openmodalframe('<?php echo base_url() ?>index.php/organisasis/delete/<?php echo $org['id'];?>', 
                                                 'Delete Dialog')"
                                       data-placement="top" data-original-title="Delete" data-original-title="Delete">
                                    <i class="fa fa-trash-o"></i></button>
        				    </td>
        				</tr>
        				<?php $id += 1;
                    } ?>
        			</tbody>
        		</table>
        	</div>
        </div>
	</div>
</div>
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>        	
<script>
    $("#datatable1").dataTable();
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #user-info');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
</script>