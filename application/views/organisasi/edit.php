<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li><a href="<?php echo base_url() ?>index.php/organisasis/"><i class="fa fa-fw fa-cogs"></i> Organisasi</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-pencil"></i> Edit Organisasi</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Tambah Organisasi
	</h3>
</div>
<div class="row" >
	<div class="col-lg-12">
        <div class="box">
            <div class="col-lg-8">
            <form action="<?php echo base_url() ?>index.php/organisasis/update" method="post" class="form-horizontal form-bordered form-validate">
    				<div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Nama Organisasi</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nama" id="name" value="<?php echo $current['nama_organisasi']?>" 
                                   class="form-control" placeholder="Nama Organisasi, Maksimal 50 Karakter" 
                                   required data-rule-minlength="2" max-length="50">
                            <input type="hidden" name="id" value="<?php echo $current['id'] ?>" />
    					</div>
    				</div>
    				<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Parent</label>
						</div>
						<div class="col-lg-9 col-sm-10">
							<select name="parent" id="selector" class="form-control" required>
								<option value="0">&nbsp;</option>
                                <?php foreach($all as $org) { ?>
                                <option value="<?php echo $org['id'] ?>" <?php if($org['id'] == $current['organisasi_id']){ echo "selected";} ?>>
                                    <?php echo $org['nama_organisasi'] ?>
                                </option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-footer col-lg-offset-3 col-sm-offset-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
    			</form>
			</div>
        </div>
    </div>
</div>