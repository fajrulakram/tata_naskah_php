<?php date_default_timezone_set('Asia/Jakarta'); ?>
<!-- START BASIC TABLE -->
<div class="row" style="margin-top:5px">
    <div class="col-lg-12">
	    <div class="box">		    
            <div class="box-body">
                <form class="form-horizontal form-bordered" role="form">
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Nama Badan Usaha</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['nama_badan_usaha'] ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Alamat Badan Usaha</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <textarea type="email" class="form-control" style="cursor : default" readonly><?php echo $data['alamat_badan_usaha'] ?></textarea>
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">No. Telepon</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['telp'] ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">No. Fax</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['fax'] ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Tanggal Pengajuan</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo date('d-M-Y', strtotime($data['tgl_pengajuan'])) ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Bahan Bakar</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['bahan_bakar'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Media Pengangkutan</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['media_pengangkutan'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Wilayah Operasi</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['wilayah_operasi'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3">
						    <label for="email3" class="control-label">Kasubdit Penyetuju</label>
						</div>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" value="<?php echo $data['penyetuju'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3">
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label for="email3" class="control-label">Waktu Persetujuan</label>
						</div>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" 
                                   value="<?php echo date('d-M-Y H:i:s', strtotime($data['waktu_persetujuan'])) ?>" 
                                   style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Kasie Pendistribusi</label>
						</div>
                        <div class="col-lg-5 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['pendistribusi'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3">
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label for="email3" class="control-label">Waktu Distribusi</label>
						</div>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" 
                                   value="<?php echo date('d-M-Y H:i:s', strtotime($data['waktu_distribusi'])) ?>" 
                                   style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Staff Verifikator</label>
						</div>
                        <div class="col-lg-5 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['verifikator'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                     <div class="form-group">
					    <div class="col-lg-3">
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label for="email3" class="control-label">Waktu Verifikasi</label>
						</div>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" 
                                   value="<?php echo date('d-M-Y H:i:s', strtotime($data['waktu_verifikasi'])) ?>" 
                                   style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Revisi</label>
                            <a href="<?php echo base_url() ?>index.php/perizinan/add_revisi/<?php echo $data['id_summary'] ?>" 
                                   type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" 
                                       data-placement="top" data-original-title="Add Revisi" style="float:right"><i class="fa fa-plus"></i></a>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <?php if(sizeof($revisi)=="0") { ?>
                                <input type="email" class="form-control" value="-"  
                                    style="cursor : default" readonly>
                            <?php } else { 
                                    $count = 0;?>
                                <table class="col-lg-12 table table-hover table-striped no-margin">
                                    <thead>
                                        <tr>
                                            <th>No. Revisi</th>
                                            <th>Waktu Masuk</th>
                                            <th>Evaluasi Selesai</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($revisi as $rev) { ?>
                                            <tr>
                                                <td><?php echo ++$count ?></td>
                                                <td><?php $tgl = date('d-M-Y H:i:s', strtotime($rev['tanggal_masuk']));
                                                            if(strpos($tgl,"1970")) {
                                                                echo "-";
                                                            } else {
                                                                echo $tgl;
                                                            }
                                                    ?> </td>
                                                <td><?php $tgl = date('d-M-Y H:i:s', strtotime($rev['tanggal_keluar']));
                                                            if(strpos($tgl,"1970")) {
                                                                echo "-";
                                                            } else {
                                                                echo $tgl;
                                                            }
                                                    ?></td>
                                                <td><?php echo $rev['keterangan'] ?> </td>
                                                <td>
                                                    <a href="<?php echo base_url() ?>index.php/perizinan/edit_revisi/<?php echo $rev['id'] ?>" 
                                   type="button" 
                                       class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" 
                                       data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                            <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/perizinan/delete_revisi/<?php echo $rev['id'] ;?>', 'Delete Revisi')" 
                                                        data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">No. Kode Izin Usaha</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['no_izin_usaha'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">No. Izin Usaha dari DJM</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo $data['izin_usaha_djm'] ?>"  
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Tanggal Penerbitan</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo date('d-M-Y', strtotime($data['tgl_terbit'])) ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Tanggal Mulai</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo date('d-M-Y', strtotime($data['tgl_mulai'])) ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label for="email3" class="control-label">Tanggal Berakhir</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="email" class="form-control" value="<?php echo date('d-M-Y', strtotime($data['tgl_akhir'])) ?>" 
                                style="cursor : default" readonly>
                        </div>
                    </div>
                </form>
            </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
</div>
<!-- END BASIC TABLE -->
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px">
        <div class="modal-content" style="width:100%">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>


<script>
    function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Add Agenda</span>");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
        disable_tombol("csv");
    
    $("#datatable1").dataTable();
    
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #metadatadetail');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
</script>