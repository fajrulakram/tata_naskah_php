<?php date_default_timezone_set('Asia/Jakarta'); ?>
<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css@1401442111.css" />
<!-- START BASIC TABLE -->
<div class="row" style="margin-top:5px">
    <div class="col-lg-12">
	    <div class="box">		    
            <div class="box-body">
                <form action="<?php echo base_url()?>index.php/perizinan/do_revisi/" 
                      method="post" class="form-horizontal form-bordered" role="form">
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Tanggal Masuk</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" id="tgl_masuk" name="masuk">
                            <input type="hidden" class="form-control" id="id_summary" name="id" value="<?php echo $id ?>">
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Evaluasi Selesai</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" id="tgl_keluar" name="keluar">
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Keterangan</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" name="keterangan">
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">&nbsp;</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <button class="btn btn-equal" type="submit" class="col-lg-4" style="width:200px">Submit</button>
                        </div>
                    </div>	
                </form>
            </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
</div>
<!-- END BASIC TABLE -->

<script>
    function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Add Agenda</span>");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("csv");
        disable_tombol("delete");
    
    $("#datatable1").dataTable();
    
    $(document).ready(function() {                        
        $('#tgl_masuk').datetimepicker();
        $('#tgl_keluar').datetimepicker();
    });
</script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>