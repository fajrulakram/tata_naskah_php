<?php date_default_timezone_set('Asia/Jakarta'); ?>
<!-- START BASIC TABLE -->
<div class="row" style="margin-top:5px">
    <div class="col-lg-12">
	    <div class="box">		    
            <div class="box-body">
                <table id="datatable1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nama Badan Usaha</th>
                            <th>Alamat Badan Usaha</th>
                            <th>No. Kode Izin Usaha</th>
                            <th>No. Izin Usaha dari DJM</th>
                            <th>Tanggal Pengajuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data as $izin) {?>
                        <tr>
                            <td><?php echo $izin['nama_badan_usaha'] ?></td>
							<td><?php echo $izin['alamat_badan_usaha'] ?></td>
                            <td><?php echo $izin['no_izin_usaha'] ?></td>
                            <td><?php echo $izin['izin_usaha_djm'] ?></td>
                            <td><?php echo date('d-m-Y', strtotime($izin['tgl_pengajuan'])) ?></td>
                            <td style="width:70px">
                                <a href="<?php echo base_url() ?>index.php/perizinan/detail/<?php echo $izin['id_summary'] ?>" 
                                   type="button" 
                                       class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" 
                                       data-placement="top" data-original-title="View"><i class="fa fa-eye"></i></a>
                                <a href="<?php echo base_url() ?>index.php/perizinan/add_revisi/<?php echo $izin['id_summary'] ?>" 
                                   type="button" 
                                       class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" 
                                       data-placement="top" data-original-title="Add Revisi"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>								
                        <?php } ?>
                    </tbody>
                </table>
            </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
</div>
<!-- END BASIC TABLE -->

<script>
     function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Add Agenda</span>");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
        //disable_tombol("csv");
        $("#csv").attr('href', '<?Php echo site_url('perizinan/export_csv/'); ?>/');
        $("#csv").html("<span>Export Summary</span>");
    
    $("#datatable1").dataTable();
</script>