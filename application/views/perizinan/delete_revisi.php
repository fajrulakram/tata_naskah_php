<?php date_default_timezone_set('Asia/Jakarta'); ?>
<div class="section-body">
	<div class="row" id="metadatadetail">
        <div class="col-lg-12">
            <div style="background-color : white; color:black; font-size:18px; font-weight : bold;">
                Apakah Anda Yakin Akan Menghapus Agenda ini ?
            </div>
            <div class="form-horizontal form-bordered">
                <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Tanggal Masuk</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" id="tgl_masuk" name="masuk" 
                                   value="<?php echo date('m/d/Y g:i A', strtotime($data['tanggal_masuk'])) ?>">
                            <input type="hidden" class="form-control" id="id_summary" name="id" value="<?php echo $id ?>">
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Tanggal Keluar</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" id="tgl_keluar" name="keluar"
                                   value="<?php echo date('m/d/Y g:i A', strtotime($data['tanggal_keluar'])) ?>">
                        </div>
                    </div>	
                    <div class="form-group">
					    <div class="col-lg-3 col-md-2 col-sm-3">
						    <label class="control-label">Keterangan</label>
						</div>
                        <div class="col-lg-9 col-md-10 col-sm-9">
                            <input type="text" class="form-control" name="keterangan" value="<?php echo $data['keterangan'] ?>">
                        </div>
                    </div>	
            <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="<?php echo base_url() ?>index.php/perizinan/do_delete_revisi/<?php echo $data['id'] ?>" type="button" class="btn btn-warning">Delete</a>
            </div>
        </div>        
    </div>
</div>