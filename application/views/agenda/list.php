<div class="row" style="margin-top:20px">
    <div class="col-md-12">
	    <div class="box">
		    <div class="box-head" style="display:none">
			    <ul class="nav nav-tabs" data-toggle="tabs">
				    <li class="active"><a href="#inbox"><i class="fa fa-fw fa-inbox"></i> Semua Agenda</a></li>					
					<li class=""><a href="#deleted"><i class="fa fa-fw fa-calendar-o"></i> Bulan Ini</a></li>
                    <li class=""><a href="#send"><i class="fa fa-fw fa-forward"></i> Minggu Ini</a></li>
                </ul>
            </div>
            <div class="box-body tab-content">
			    <div class="tab-pane active" id="inbox">
                    <div class="box-body table-responsive">
                        <table id="datatable1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tanggal Awal</th>
                                    <th>Tanggal Akhir</th>
                                    <th>Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tipe</th>        					
                                    <th style="width : 80px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- DAFTAR FOLDER -->
                                <?php 
                                    $id = 1;
                                    foreach($all_agenda as $ag) { ?>
                                        <tr>
                                            <td><?php echo $ag['start'] ?></td>
                                            <td><?php echo $ag['end'] ?></td>
                                            <td><?php echo $ag['title'] ?></td>
                                            <td><?php echo $ag['description'] ?></td>
                                            <td><?php echo $ag['tipe'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-info btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/view/<?php echo $ag['id_agenda'] ;?>', 'View Agenda')" 
                                                        data-original-title="Detail"><i class="fa fa-eye"></i></button>
                                                <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/delete/<?php echo $ag['id_agenda'] ;?>', 'Delete Agenda')" 
                                                        data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                <?php $id++; } ?>
                            </tbody>
                        </table>
                    </div>
			    </div>
                <div class="tab-pane" id="send">    
                    <div class="box-body table-responsive">
                        <table id="datatable2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tanggal Awal</th>
                                    <th>Tanggal Akhir</th>
                                    <th>Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tipe</th>        					
                                    <th style="width : 80px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- DAFTAR FOLDER -->
                                <?php 
                                    $id = 1;
                                    foreach($weeks as $ag) { ?>
                                        <tr>
                                            <td><?php echo $ag['start'] ?></td>
                                            <td><?php echo $ag['end'] ?></td>
                                            <td><?php echo $ag['title'] ?></td>
                                            <td><?php echo $ag['description'] ?></td>
                                            <td><?php echo $ag['tipe'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-info btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/view/<?php echo $ag['id_agenda'] ;?>', 'View Agenda')" 
                                                        data-original-title="Detail"><i class="fa fa-eye"></i></button>
                                                <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/delete/<?php echo $ag['id_agenda'] ;?>', 'Delete Agenda')" 
                                                        data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                <?php $id++; } ?>
                            </tbody>
                        </table>
                    </div>
			    </div>
				<div class="tab-pane" id="deleted">
                   <div class="box-body table-responsive">
                        <table id="datatable3" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tanggal Awal</th>
                                    <th>Tanggal Akhir</th>
                                    <th>Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tipe</th>        					
                                    <th style="width : 100px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- DAFTAR FOLDER -->
                                <?php 
                                    $id = 1;
                                    foreach($months as $ag) { ?>
                                        <tr>
                                            <td><?php echo $ag['start'] ?></td>
                                            <td><?php echo $ag['end'] ?></td>
                                            <td><?php echo $ag['title'] ?></td>
                                            <td><?php echo $ag['description'] ?></td>
                                            <td><?php echo $ag['tipe'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-info btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/view/<?php echo $ag['id_agenda'] ;?>', 'View Agenda')" 
                                                        data-original-title="Detail"><i class="fa fa-eye"></i></button>
                                                <button type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip" 
                                                        data-placement="top" onclick="openmodalframe('<?php echo base_url() ?>index.php/agendas/delete/<?php echo $ag['id_agenda'] ;?>', 'Delete Agenda')" 
                                                        data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                <?php $id++; } ?>
                            </tbody>
                        </table>
                    </div>
			    </div>
            </div>            
        </div>
    </div>
</div>

<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Upload</h4>
            </div>
            <div class="modal-body">
                <center id="message2" style="color : red; display:none;">&nbsp;</center>
                <form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" 
                      accept-charset="utf-8" method="post" id="form_uploadfile">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Undangan</label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" name="file" class="form-control" placeholder="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Tipe Agenda</label>
                        </div>
                        <div class="col-md-8">
                            <select class="col-md-12 form-control" name="tipe" id="tipe" onchange="tipe_change()">
                                <option value="umum">Umum</option>
                                <option value="presentasi">Presentasi</option>
                                <option value="dinas">Dinas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Judul</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="judul" class="form-control" placeholder="Judul">
                        </div>
                    </div>
                    <div class="form-group" id="pelaksana" style="display : none;">
                        <div class="col-md-4">
                            <label class="control-label">Pelaksana</label>
                        </div>
                        <div class="col-md-8">
                            <p id="message" style="color : red; display:none;">Jadwal Bentrok Untuk User Berikut : </p>
                            <?php foreach($user as $us) { 
                                if($us['role']==1) {?>
                                <input type="checkbox" name="pelaksana[]" 
                                       class="_pelaksana" value="<?php echo $us['id'] ?>" >
                                    <span id="user_<?php echo $us['id'] ?>"><?php echo $us['nama_lengkap'] ?></span><br/>
                            <?php } } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Tempat</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="tempat" class="form-control" placeholder="Tempat">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Waktu Mulai</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="waktuM" class="form-control" id='wmulai' placeholder="Waktu Mulai"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Waktu Akhir</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="waktuA" class="form-control" id="wakhir" placeholder="Waktu Akhir">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Deskripsi</label>
                        </div>
                        <div class="col-md-8">
                            <textarea class="col-md-12" name="isi"></textarea>
                        </div>
                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" onclick="submitform()" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
        
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px">
        <div class="modal-content" style="width:100%">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function() {    
        var id = <?php echo $this->session->userdata("id_user") ?>;
        $("#csv").attr('href', '<?Php echo site_url('agendas/export_csv/'); ?>/'+id);
        $("#csv").html("<span>Export Jadwal Pribadi</span>");
    });
    
    function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Add Agenda</span>");
        }
        enable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
    $("#datatable1").dataTable();
    $("#datatable2").dataTable();
    $("#datatable3").dataTable();
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #metadatadetail');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
</script>