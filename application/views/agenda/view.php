<div class="section-body">
	<div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal form-bordered" id="metadatadetail">
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Judul</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['title'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Tempat</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['tempat'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Waktu Mulai</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['start'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Waktu Selesai</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['end'] ?></p>
                    </div>
                </div>		        
                <?php if(sizeof($pelaku)>0) { ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Pelaksana</label>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <?php foreach($pelaku as $us) {
                                echo $us['nama_lengkap']."<br/>";
                             } ?>
                        </p>
                    </div>
                </div>		        
                <?php } ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Deskripsi</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['description'] ?></p>
                    </div>
                </div>	
            </div>
        </div>
    </div>
</div>