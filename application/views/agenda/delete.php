<div class="section-body">
	<div class="row" id="metadatadetail">
        <div class="col-lg-12">
            <div style="background-color : white; color:black; font-size:18px; font-weight : bold;">
                Apakah Anda Yakin Akan Menghapus Agenda ini ?
            </div>
            <div class="form-horizontal form-bordered">
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Judul</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['title'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Tempat</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['tempat'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Waktu Mulai</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['start'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Waktu Selesai</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['end'] ?></p>
                    </div>
                </div>		        
                <?php if(sizeof($pelaku)>0) { ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Pelaksana</label>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <?php foreach($pelaku as $us) {
                                echo $us['nama_lengkap']."<br/>";
                             } ?>
                        </p>
                    </div>
                </div>		        
                <?php } ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Deskripsi</label>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $agenda['description'] ?></p>
                    </div>
                </div>	
            </div>
            <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="<?php echo base_url() ?>index.php/agendas/do_delete/<?php echo $agenda['id_agenda'] ?>" type="button" class="btn btn-warning">Delete</a>
            </div>
        </div>        
    </div>
</div>