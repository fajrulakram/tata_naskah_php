<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css@1401442111.css" />

<div class="section-body">
    <div class="row">
	    <div class="col-lg-12" id="kalender" style="overflow-y : auto;">
		    <div class="box box-tiles style-support3">
			    <div class="row">
                    <!-- START CALENDAR SIDEBAR -->									
                    <!-- END CALENDAR SIDEBAR -->
                    <!-- START CALENDAR -->
                    <div class="col-md-12 style-white">
					    <div id="calendar"></div>
					</div><!--end .col-md-9 -->
					<!-- END CALENDAR -->
                </div><!--end .row -->
            </div><!--end .box -->
        </div><!--end .col-lg-12 -->
    </div><!--end .row -->
</div><!--end .section-body -->

<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Upload</h4>
            </div>
            <div class="modal-body">
                <center id="message2" style="color : red; display:none;">&nbsp;</center>
                <form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" 
                      accept-charset="utf-8" method="post" id="form_uploadfile">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Undangan</label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" name="file" class="form-control" placeholder="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Tipe Agenda</label>
                        </div>
                        <div class="col-md-8">
                            <select class="col-md-12 form-control" name="tipe" id="tipe" onchange="tipe_change()">
                                <option value="umum">Umum</option>
                                <option value="presentasi">Presentasi</option>
                                <option value="dinas">Dinas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Judul</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="judul" class="form-control" placeholder="Judul">
                        </div>
                    </div>
                    <div class="form-group" id="pelaksana" style="display : none;">
                        <div class="col-md-4">
                            <label class="control-label">Pelaksana</label>
                        </div>
                        <div class="col-md-8">
                            <p id="message" style="color : red; display:none;">Jadwal Bentrok Untuk User Berikut : </p>
                            <?php foreach($user as $us) { 
                                if($us['role']==1) {?>
                                <input type="checkbox" name="pelaksana[]" 
                                       class="_pelaksana" value="<?php echo $us['id'] ?>" >
                                    <span id="user_<?php echo $us['id'] ?>"><?php echo $us['nama_lengkap'] ?></span><br/>
                            <?php } } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Tempat</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="tempat" class="form-control" placeholder="Tempat">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Waktu Mulai</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="waktuM" class="form-control" id='wmulai' placeholder="Waktu Mulai"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Waktu Akhir</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="waktuA" class="form-control" id="wakhir" placeholder="Waktu Akhir">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Deskripsi</label>
                        </div>
                        <div class="col-md-8">
                            <textarea class="col-md-12" name="isi"></textarea>
                        </div>
                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" onclick="submitform()" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Upload</h4>
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" 
                      accept-charset="utf-8" method="post" id="edit_form_uploadfile">
                    
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" onclick="editform()" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width : 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Preview</h4>
            </div>
            <div class="modal-body ">
                <div class="form-horizontal form-banded form-bordered" id="konten_media"></div>
                <a class="media" href=""></a> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->

<script type = 'text/javascript'>
    var id_curruent_folder=0;
    
    function tipe_change(){
        var val = $("#tipe").val();
        if(val === "dinas") {
            $("#pelaksana").css("display","table");
        } else {
            $("#pelaksana").css("display","none");
        }
    }
    
    function submitform() {            
        var val = $("#tipe").val();
        if(val==="dinas") {
            //check overlap
            var awal = $("#wmulai").val();
            var akhir = $("#wakhir").val();
            var users = $('._pelaksana:checked').map(function() {
                return this.value;
            }).get();
            $.ajax({
                type: "POST",                
                url: "<?php echo base_url() ?>index.php/agendas/check_overlap_dinas/",
                data: { user: users, awal: awal, akhir: akhir  },
                dataType: "json"
            }).done(function( msg ) {
                console.log(msg.length);
                if(msg.length==0){
                    $("#form_uploadfile").attr("action","<?Php echo site_url('agendas/addfile/'); ?>/").submit();                  
                } else {
                    $("#message").css("display",'block');
                    for(i=0;i<msg.length;i++){
                        $("#user_"+msg[i]).css('color','red');
                    }
                }
            });
        } else if(val==="presentasi") {
            var awal = $("#wmulai").val();
            var akhir = $("#wakhir").val();
            var dt1 = new Date(awal);
            var dt2 = new Date(akhir);
            var hours = Math.abs(dt1 - dt2) / 36e5;
            if(dt2 < dt1){
                $("#message2").html("Maaf, Input Salah Waktu Awal > Waktu Akhir");
                $("#message2").css("display",'block');
            } else if(hours > 3) {
                $("#message2").html("Maaf, Waktu Presentasi Tidak Dapat Melebihi 3 Jam");
                $("#message2").css("display",'block');
            } else {
                $.ajax({
                    type: "POST",                
                    url: "<?php echo base_url() ?>index.php/agendas/check_overlap_presentasi/",
                    data: { awal: awal, akhir: akhir  },
                    dataType: "html"
                }).done(function( msg ) {
                    console.log(msg);
                    console.log(typeof msg);
                    var con = "conflict";
                    var over = "overflow";
                    if(msg===con){
                        $("#message2").html("Maaf, Telah Ada Presentasi Pada Waktu yang Diinginkan");
                        $("#message2").css("display",'block');
                    } else if(msg===over){
                        $("#message2").html("Maaf, Jumlah Presentasi Pada Hari yang Diinginkan Telah Mencapai Jumlah Maksimum (3)");
                        $("#message2").css("display",'block');
                    } else {
                        $("#form_uploadfile").attr("action","<?Php echo site_url('agendas/addfile/'); ?>/").submit();
                    }
                });
            }
        } else {
            $("#form_uploadfile").attr("action","<?Php echo site_url('agendas/addfile/'); ?>/").submit();          
        }
    };
    function editform() {
          // $("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/").submit();          
    };
    $(document).ready(function() {    
        var id = <?php echo $this->session->userdata("id_user") ?>;
        $("#csv").attr('href', '<?Php echo site_url('agendas/export_csv/'); ?>/'+id);
        $("#csv").html("<span>Export Jadwal Pribadi</span>");
        
        function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Add Agenda</span>");
        }
        enable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
        
        $("#kalender").css("height",$( window ).height()-135);
        $('.tanggals').datetimepicker();
        $('#wmulai').datetimepicker();
        $('#wakhir').datetimepicker();
        //$('a.media').media({width:"100%", height:400});
	    var id_lama=0;
	    var id_folder_parent_lama = 0;
	    var id_folder_child_lama = 0;                
        
        function getfile(){
	        // $(".file").click(function(){                
    	    //     if(id_lama!=0){
    	    //         $('#'+id_lama).removeClass("btn-primary").addClass("btn-default");
    	    //     }
    	    //     var id = $(this).attr("id");
         //        console.log(id);
    	    //     $('#'+id).removeClass("btn-default").addClass("btn-primary");
    	    //     id_lama = id;
    	    //     getmetadata(id);
         //        preview_pdf(id);
         //        download(id);
         //        geteditform(id);
    	    // });
	    }
	    getfile();	          
	    
        function geteditform(id)
        {
            
            
        }
        function preview_pdf(id)
        {
            console.log("test");
            
        }
        
        function download(id)
        {
            $("#download").attr('href', '<?Php echo site_url('filemanagers/download'); ?>/'+id);
            
            
        }
        
	    function getmetadata(id){
            //console.log("<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id);
	        
	    }
	    
        
      
    function updateform_upload_file(id)
    {
            
    }
        
    function updatemetadata(id){
        $('#metadatadetail').html('');
        
    }
	    /*
	    $('.dd-item').click(function(){
	        var id = $(this).attr('data-id');
          console.log(id);
	        //alert(id);
	        if(id_folder_lama!=0){
	            $('.folder'+id_folder_lama).removeClass("btn-primary").addClass("btn-default");
	            $('.folder'+id_folder_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
	        }
	        
	        $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	        $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
	        
	        id_folder_lama = id;
	        $.ajax({
                url: "/user/getfilelist/"+id,
                dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $("#datafile").html('');
                    for(var i=0; i < data.length; i++){
                        var current = data[i];
                        $("#datafile").append('<tr id="'+current.id+'" class="btn-default file" ><td><?Php echo img('/assets/images/PDF.png'); ?> '+current.nama_files+'</td><td>'+current.updated_at+'</td><td>'+current.uploader+'</td></tr>');
                    }
                    
                    getfile();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
	    });*/
        $( "#search_file" ).submit(function( event ) {
            event.preventDefault();
            var url = this.action;
            var data = $("#file_search").val();
            // $.post( url, { str: data }, function( data ) {
            //     $("#datafile").html(data);
            // });
        });
    });
    
    
</script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/fullcalendar/fullcalendar.min.js"></script>
