<?php date_default_timezone_set('Asia/Jakarta'); ?>
<!-- START BASIC TABLE -->
<div class="row" style="margin-top:0px">
    <div class="col-lg-12">
	    <div class="box">		    
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Waktu Notifikasi</th>
                            <th>Asal Notifikasi</th>
                            <th>Pesan</th>
                            <th>Status</th>                                                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($notification as $notif) {?>
                        <tr>
                            <td><?php echo date('d M Y H:i:s', strtotime($notif['created_at'])) ?></td>
							<td><?php echo $notif['nama_lengkap'] ?></td>
							<td><?php echo $notif['pesan'] ?></td>
                            <td><?php if($notif['status']==0) {echo "Belum di Baca";} else {echo "Sudah di Baca"; } ?></td>
                        </tr>								
                        <?php } ?>
                    </tbody>
                </table>
            </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
</div>
<!-- END BASIC TABLE -->