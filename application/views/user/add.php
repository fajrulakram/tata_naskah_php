<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li><a href="<?php echo base_url() ?>index.php/users/"><i class="fa fa-fw fa-user"></i> Pengguna</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-pencil"></i> Registrasi Pengguna</a></li>
</ol>
<div class="section-header">
	<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> 
	    Tambah Pengguna
	</h3>
</div>
<div class="row" >
	<div class="col-lg-12">
        <div class="box">
            <div class="col-lg-8">
                
                <form id="adduser_form" action="" class="form-horizontal form-bordered form-validate" 
                      method="post" accept-charset="utf-8" enctype="multipart/form-data">                                           
                    <div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Username</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" id="uname" name="username" class="form-control" placeholder="Username, Maksimal 50 Karakter" 
                               required data-rule-minlength="2" max-length="50">
                            <p id="message" style="color:red;font-style:italic;display:none">username telah digunakan</p>
    					</div>
    				</div>
                    <div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Password</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
        						<input type="password" name="password" class="form-control" 
                               required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>
                    <div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Peran</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<select name="peran" id="selector" class="form-control" required>
                                <option value="1">User Biasa</option>
								<option value="0">Admin</option>                                
							</select>
    					</div>
    				</div>
    				<div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="name" class="control-label">Nama Lengkap</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nama" id="name" class="form-control" 
                                   placeholder="Nama Organisasi, Maksimal 50 Karakter" 
                               required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>
                    <div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="email" class="control-label">E-Mail</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="email" id="email" class="form-control" 
                                   placeholder="Email Pengguna, Maksimal 50 Karakter" 
                               required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>
    				<div class="form-group">
						<div class="col-lg-3 col-sm-2">
							<label for="selector" class="control-label">Organisasi</label>
						</div>
						<div class="col-lg-9 col-sm-10">
							<select name="organisasi" id="selector" class="form-control" required>								
                                <?php foreach($organisasi as $org) { ?>
								    <option value="<?php echo $org['id'] ?>"><?php echo $org['nama_organisasi'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
                    <div class="form-group">
    				    <div class="col-lg-3 col-sm-2">
    						<label for="jabatan" class="control-label">NIP</label>
    					</div>
    					<div class="col-lg-9 col-sm-10">
    						<input type="text" name="nip" id="jabatan" class="form-control" placeholder="NIP, Maksimal 50 Karakter" 
                               required data-rule-minlength="2" max-length="50">
    					</div>
    				</div>
                    <div class="col-lg-12">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <div class="col-lg-3 col-sm-2">
    						<label for="jabatan" class="control-label">Avatar Pengguna</label>
    					</div>
						<div class="btn-group">
							<span class="btn btn-success fileinput-button button-xs" 
                                  style="margin-top:5px; height : 30px; margin-left : 10px">
						    	<i class="glyphicon glyphicon-plus"></i>
								<span>Add files...</span>
								<input type="file" name="userfile" >
							</span>							
						</div>
                    </div> <br/><br/>
					<div class="form-footer col-lg-offset-3 col-sm-offset-2">
						<button type="submit" id="submits" class="btn btn-primary btn-block" 
                                style="height : 30px; width : 100px">
                            <i class="fa fa-fw fa-cloud-upload"></i> 
                            Submit</button>
					</div>
    			</form>
			</div>
        </div>
    </div>
</div>
<script>
    
    $('document').ready(function(){
        $('#uname').bind('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
               event.preventDefault();
               return false;
            }
        });
        
        $("#submits").click(function (){
            event.preventDefault();

            var username = $("#uname").val();
            var email = $("#email").val();
            console.log(username);
            console.log(email);

            $.ajax({
                type: "POST",                
                url: "<?php echo base_url() ?>index.php/users/is_exist/",
                data: { username: username, email:email  },
                dataType: "json"
            }).done(function( msg ) {
                console.log(msg);
                if(msg['isexist'] == 0){
                    $("#adduser_form").attr("action","<?Php echo base_url() ?>index.php/users/create/").submit();          
                } else {
                    $("#uname").css('border-color','red');
                    $("#message").css('display','block');
                    $('#uname').goTo();
                }
            });

        });

        (function($) {
            $.fn.goTo = function() {
                $('html, body').animate({
                    scrollTop: $(this).offset().top + 'px'
                }, 'fast');
                return this; // for chaining...
            }
        })(jQuery);
    });
    
</script>