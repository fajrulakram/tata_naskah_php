<div id="user-info">
  <div class="user-avatar">
    <div class="user-avatar-avatar"><img src="<?php if ($user['avatar'] == null)  {
            echo base_url() . 'assets/images/avatar.png';
        } else {
            echo base_url() .$user['avatar'];
        }?>"></div>
    <div class="user-username-container"><div class="user-username"><?php echo $user['username']; ?></div></div>
  </div>
  <div class="user-name">
    <div class="user-fullname"><?php echo $user['nama_lengkap']; ?></div>
    <div class="user-jabatan"><?php echo $user['nip']; ?></div>
    <div class="user-organisasi"><?php echo $user['nama_organisasi']; ?></div>
  </div>
  <div class="user-contact">
    <div class="user-role"><?php echo ($user['role'] == '0') ? 'admin&nbsp;&nbsp;<i class="fa fa-key"></i>' : 'user biasa&nbsp;&nbsp;<i class="fa fa-users"></i>'; ?></div>
    <div class="user-email"><a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?>&nbsp;&nbsp;<i class="fa fa-envelope-o"></i></a></div>
  </div>
</div>