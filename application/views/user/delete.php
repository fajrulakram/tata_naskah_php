<div id="user-info" style="margin-top:0px;">
    <div class="user-avatar">
        <div style="background-color : white; color:black; font-size:18px; font-weight : bold;">
            Apakah Anda Yakin Akan Menghapus User ini ?
        </div>
        <div class="user-avatar-avatar"><img src="<?php if ($user['avatar'] == null)  {
                echo base_url() . 'assets/images/avatar.png';
            } else {
                echo base_url() .$user['avatar'];
            }?>">
        </div>
        <div class="user-username-container">
            <div class="user-username">
                <?php echo $user['username']; ?>
            </div>
        </div>
    </div>
    <div class="user-name">
        <div class="user-fullname"><?php echo $user['nama_lengkap']; ?></div>
        <div class="user-organisasi"><?php echo $user['nama_organisasi']; ?></div>
    </div>
    <div class="user-contact">
        <div class="user-role">
                <?php echo ($user['role'] == '0') ? 'admin&nbsp;&nbsp;<i class="fa fa-key"></i>' : 
                        'user biasa&nbsp;&nbsp;<i class="fa fa-users"></i>'; ?></div>
        <div class="user-email">
                <a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?>&nbsp;&nbsp;<i class="fa fa-envelope-o"></i></a>
        </div>
    </div>
    <div class="modal-footer" style="background-color:white;margin-right:-20px;border:none; height: 40px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url() ?>index.php/users/do_delete/<?php echo $user['id'] ?>" type="button" class="btn btn-warning">Delete</a>
    </div>
</div>