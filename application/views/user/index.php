<style>
    #message {
        -webkit-animation-duration: 3s;
        -webkit-animation-delay: 2s;  
        -moz-animation-duration: 3s;
        -moz-animation-delay: 2s;  
    }
</style>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url() ?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-user"></i> Pengguna</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Data<strong>Pengguna</strong></h4></header>
    		</div>
    		<div class="row">
        		<div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url() ?>index.php/users/add/" class="btn btn-outline btn-primary btn-block btn-labeled">
    					<span><i class="fa fa-group"></i></span>
    					<div>Tambah Pengguna</div>
    				</a>
    		    </div>
		    </div>
        	<div class="box-body table-responsive">
                <center id="message" class="animated fadeOut" style="color: red; font-style:italic;">
                    <?php echo $this->session->flashdata('message'); ?>
                </center>
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>
        					<th>ID</th>
        					<th>Nama</th>
        					<th>Email</th>
        					<th>Username</th>
        					<th>Organisasi</th>
        					<th>Aksi</th>
        				</tr>
        			</thead>
        	    	<tbody>
        				<!-- DAFTAR USER -->
        				<?php $id = 1;
        				    foreach($user as $us) { ?>
        		    	<tr>
        			        <td><?php echo $id ?></td>
        				    <td><?php echo $us['nama_lengkap'] ?></td>
        				    <td><?php echo $us['email'] ?></td>
        				    <td><?php echo $us['username'] ?></td>
        				    <td><?php echo $us['nama_organisasi'] ?></td>
        				    <td>
                                 <button type="button" class="btn btn-xs btn-info btn-equal" data-toggle="tooltip" 
                                        data-placement="top" data-original-title="View" 
                                        onclick="openmodalframe('<?php echo base_url() ?>index.php/users/view/<?php echo $us['id'] ;?>', 
                                                 '<?php echo $us['username'] ?>')">
                                    <i class="fa fa-eye" data-toggle="modal" data-target="#usermodal"></i>
                                </button>
        				        <a href="<?php echo base_url() ?>index.php/users/edit/<?php echo $us['id'] ?>" type="button" 
                                       class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" 
                                       data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
								<button  
                                   type="button" class="btn btn-xs btn-danger btn-equal" data-toggle="tooltip"
                                        onclick="openmodalframe('<?php echo base_url() ?>index.php/users/delete/<?php echo $us['id'] ;?>', 
                                                 'Delete Dialog')"
                                       data-placement="top" data-original-title="Delete" data-original-title="Delete">
                                    <i class="fa fa-trash-o"></i></button>
        				    </td>
        				</tr>
                

        				<?php $id += 1;
                            } ?>        				        				
        			</tbody>
        		</table>
                <div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                          </div>
                      <div class="modal-body">

                      </div>
                    </div>
                  </div>
                 </div>
        	</div>
        </div>
	</div>
</div>


<script>
    function openmodalframe(url, name) {
        $('#usermodal .modal-body').load(url + ' #user-info');
        $('#usermodal .modal-title').html(name);
        $('#usermodal').modal('show');
    }
    
    $(document).ready(function () {
        var $myDialog = $('<div></div>')
            .html('Apakah anda yakin ?')
            .dialog({
            autoOpen: false,
            title: 'Delete Users',
            buttons: {
                "Delete": function () {
                    $(this).dialog("close");            
                    return true;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                    return false
                }
            }
      });

      $('.btndelete').click(function (event) {
          event.preventDefault();
          return $myDialog.dialog('open'); //replace the div id with the id of the button/form
      });
    });
    
    $("#datatable1").dataTable();
</script>