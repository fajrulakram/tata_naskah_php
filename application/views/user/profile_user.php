
<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<div class="section-body">
	<div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal form-bordered" id="metadatadetail">
                <div class="form-group">
                    <div class="col-lg-4 col-sm-3">
                        <label for="name" class="control-label">Username</label>
                    </div>
                    <div class="col-lg-8 col-sm-9">
                        <p><?Php echo $data->username; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-4 col-sm-3">
                        <label for="name" class="control-label">Email</label>
                    </div>
                    <div class="col-lg-8 col-sm-9">
                        <p><?Php echo $data->email; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-4 col-sm-3">
                        <label for="name" class="control-label">Nama Lengkap</label>
                    </div>
                    <div class="col-lg-8 col-sm-9">
                        <p><?Php echo $data->nama_lengkap; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-4 col-sm-3">
                        <label for="name" class="control-label">NIP</label>
                    </div>
                    <div class="col-lg-8 col-sm-9">
                        <p><?Php echo $data->nip; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-4 col-sm-3">
                        <label for="name" class="control-label">Organisasi</label>
                    </div>
                    <div class="col-lg-8 col-sm-9">
                        <p><?Php echo $data->nama_organisasi; ?></p>
                    </div>
                </div>
                
            </div>
        </div>
	</div>
</div>

  
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
            <form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" accept-charset="utf-8" method="post" id="edit_form_uploadfile">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">Foto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-lg-4 col-sm-3">
                            <label for="name" class="control-label">Foto</label>
                        </div>
                        <div class="col-lg-8 col-sm-9">
                            <p><input type="file" name="file" /></p>
                        </div>
                    </div>
                </div>
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">Informasi personal</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-4 col-sm-3">
                                <label for="name" class="control-label">Email</label>
                            </div>
                            <div class="col-lg-8 col-sm-9">
                                <p><input type="text" name="email" value="<?Php echo $data->email; ?>" /></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4 col-sm-3">
                                <label for="name" class="control-label">Nama Lengkap</label>
                            </div>
                            <div class="col-lg-8 col-sm-9">
                                <p><input type="text" name="nama_lengkap" value="<?Php echo $data->nama_lengkap; ?>" /></p>
                            </div>
                        </div>


                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">Ganti Password</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-4 col-sm-3">
                                <label for="name" class="control-label">Password</label>
                            </div>
                            <div class="col-lg-8 col-sm-9">
                                <p><input type="password" name="password" /></p>
                            </div>
                        </div>
                        
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="editform()" class="btn btn-primary">Save changes</button>
                </div>
            </form>
				
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
    function editform() {
          $("#edit_form_uploadfile").attr("action","<?Php echo site_url('users/editaction/'); ?>/").submit();
          
    };
    
$(document).ready(function() {                
        $("#filemanager").css("height",$( window ).height()-100);
        
       function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
            $("a[data-target='#"+$idbutton+"']").css("display", "none");
            //$("a[data-target='#"+$idbutton+"']").html("Edit Profile");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
            $("a[data-target='#"+$idbutton+"']").html("<span>Edit Profile</span>");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        enable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
        disable_tombol("csv");
    
        
});    
</script>	


