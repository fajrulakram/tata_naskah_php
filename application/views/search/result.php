<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css@1401442111.css" />

<div class="section-body">
	<div class="row">
        <div class="col-lg-6 bataskanan">
            <div id="filemanager" style="overflow:auto; width: 100%">
                <table id="filemanager" class="table" style="width: 100%">
                <thead>
                    <tr>
                        <th>Nama File</th>
                        <th>Tanggal</th>
                        <th>Uploader</th>
                    </tr>
                </thead>
                <tbody id="datafile">
                  <?php if($file == null) {
                        echo '<tr><td colspan="3" style="text-align : center">
                                <i>Surat Tidak Ditemukan!</i></td>';
                    } else {                        
                        foreach($file as $rs) {                            
                                echo '<tr id="'.$rs['id'].'" class="btn-default file" onclick="getmetadata(\''.$rs['id'].'\')">
                                        <td> '.img('assets/images/PDF.png').' '.$rs['nama_files'].' </td>
                                        <td>'.$rs['created_at'].'</td>
                                        <td>'.$rs['uploader'].'</td>
                                    </tr>';                            
                        }
                    }?>
                </tbody>
            </table>
            
                
            </div>
        </div>
        <div class="col-lg-6">            
            <div class="box" id="metas" style="overflow:auto; width: 100%">					
					<div class="box-body no-padding">
                        <div style="height : 40px; background-color : #f4f4f4; border-bottom : solid 1px #b3b3b3 ">
                            <p style="margin-left : 10px; font-size : 13px; line-height : 40px;"><b>Meta Data</b></p>
                        </div>
						<div class="form-horizontal form-bordered" id="metadatadetail">
							<div class="form-group">
								<div class="col-lg-4 col-sm-3">
									<label for="name" class="control-label">&nbsp;</label>
								</div>
								<div class="col-lg-8 col-sm-9">
                                    <p><i>Silahkan memilih file terlebih dahulu.</i></p>
								</div>
							</div>														
						</div>
					</div>
				</div><!--end .box -->
        </div>
	</div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Upload</h4>
			</div>
			<div class="modal-body">
				<form action="" class="form-horizontal form-banded form-bordered" enctype="multipart/form-data" accept-charset="utf-8" method="post" id="edit_form_uploadfile">
					
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" onclick="editform()" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width : 900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Preview</h4>
			</div>
			<div class="modal-body">
				<a class="media" href=""></a> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Anda yakin ingin menghapus file ini ?</h4>
			</div>
			<div class="modal-body">
				<a class="media_delete" href=""></a> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="delete_file_btn" data-dismiss="modal">Delete</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->
<script>
    $(document).ready(function() {                        
        $('#search_tanggal').datetimepicker();
    });
</script>
<script type = 'text/javascript'>
    var id_curruent_folder=0;
    
    function submitform() {
          //pengecekan SP2D
          if(id_curruent_folder==13){
              
          }
          $("#form_uploadfile").attr("action","<?Php echo site_url('filemanagers/addfile/'); ?>/"+id_curruent_folder).submit();          
    };
    function editform() {
          $("#edit_form_uploadfile").attr("action","<?Php echo site_url('filemanagers/editfile/'); ?>/").submit();          
    };
    $(document).ready(function() {                
        $("#filemanager").css("height",$( window ).height()-160);
        $("#metas").css("height",$( window ).height()-160);
        
         function disable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").addClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "color: #d7d7d7");
        }
        
        function enable_tombol($idbutton)
        {
            //class="disabled" style="color : #d7d7d7"
            $("a[data-target='#"+$idbutton+"']").removeClass("disabled");
            $("a[data-target='#"+$idbutton+"']").attr("style", "");
        }
        disable_tombol("simpleModal");
        disable_tombol("preview");
        disable_tombol("edit");
        disable_tombol("download");
        disable_tombol("delete");
        
        //$('a.media').media({width:"100%", height:400});
	    var id_lama=0;
	    var id_folder_parent_lama = 0;
	    var id_folder_child_lama = 0;                
        
        function getfile(){
	        $(".file").click(function(){    
                enable_tombol("preview");
                enable_tombol("edit");
                enable_tombol("download");
                enable_tombol("delete");
                
    	        if(id_lama!=0){
    	            $('#'+id_lama).removeClass("btn-primary").addClass("btn-default");
    	        }
    	        var id = $(this).attr("id");
                //console.log(id);
    	        $('#'+id).removeClass("btn-default").addClass("btn-primary");
    	        id_lama = id;
    	        getmetadata(id);
                preview_pdf(id);
                download(id);
                geteditform(id);
                
    	    });
	    }
	    getfile();	     
        

	    
        function geteditform(id)
        {
            $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/getformedit/"+id,
                //dataType: "json",
                success: function(response) {
                   //console.log(response);
                    
                    $('#edit_form_uploadfile').html(response);
                    
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
            
        }
        function preview_pdf(id)
        {
            $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/getfilebyid/"+id,
                dataType: "json",
                success: function(response) {
                    $("#preview").html('<div class="modal-dialog" style="width : 900px;">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                '<h4 class="modal-title" id="simpleModalLabel">Preview</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                '<a class="media" href=<?Php echo base_url(); ?>uploads/files/'+response.url+'></a>'+
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                            '</div>'+
                        '</div><!-- /.modal-content -->'+
                    '</div><!-- /.modal-dialog -->');
                    //console.log('<?Php echo base_url(); ?>uploads/files/'+response.url);                    
                    $(".media").attr('href', '<?Php echo base_url(); ?>uploads/files/'+response.url);
                    $('a.media').media({width:"100%", height:600});
                    
                    
                    $("#delete").html('<div class="modal-dialog" style="width : 600px;">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                '<h4 class="modal-title" id="simpleModalLabel">Delete</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                '<a class="media_delete" href=<?Php echo base_url(); ?>uploads/files/'+response.url+'></a>'+
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                                '<button type="button" class="btn btn-danger" id="delete_file_btn" data-dismiss="modal">Delete</button>'+
                            '</div>'+
                        '</div><!-- /.modal-content -->'+
                    '</div><!-- /.modal-dialog -->');
                    $(".media_delete").attr('href', '<?Php echo base_url(); ?>uploads/files/'+response.url);
                    $('a.media_delete').media({width:"100%", height:400});
                    
                    $("#delete_file_btn").click(function(){
                        window.location="<?Php echo site_url("filemanagers/delete_file"); ?>/"+id;
                    });
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
            
        }
        
        function download(id)
        {
            $("#download").attr('href', '<?Php echo site_url('filemanagers/download'); ?>/'+id);
        }
        
	    function getmetadata(id){
            preview_pdf(id);
            download(id);
            geteditform(id);
	        $.ajax({                
                url: "<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id,                
                success: function(response) {
                   //console.log(response);
                    $("#metadatadetail").html(response);                    
                }
            }).done(function() {
                
            });
	    }
	    
      $('.parent').click(function(){
          var id = $(this).attr('data-id');
          disable_tombol("preview");
          disable_tombol("edit");
          disable_tombol("download");
          disable_tombol("delete");
        
          //console.log(id);
	      if(id_folder_parent_lama!=0){
	            $('.folder'+id_folder_parent_lama).removeClass("btn-primary").addClass("btn-default");
	            $('.folder'+id_folder_parent_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
	      }
	      $(".liparent").removeClass("active");
          $(".parent").removeClass("active");
          $("#parent"+id).addClass("active");
	      $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	      $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
          //$('.folder'+id_folder_parent_lama).click();
	      //var exp = $('.folder'+id).find('[data-action=expand]').click();
	      //console.log(exp);
	      id_folder_parent_lama = id;
      });
    
      $('.child').click(function(){
          var id = $(this).attr('data-id');
          enable_tombol("simpleModal");
          disable_tombol("preview");
          disable_tombol("edit");
          disable_tombol("download");
          disable_tombol("delete");
        
          console.log(id);
	        if(id_folder_child_lama!=0){
	            $('.folder'+id_folder_child_lama).removeClass("btn-primary active").addClass("btn-default");
	            $('.folder'+id_folder_child_lama).find('.fa').removeClass("fa-folder-open").addClass("fa-folder");
	        }	              
	        $('.folder'+id).removeClass("btn-default").addClass("btn-primary");
	        $('.folder'+id).find('.fa').removeClass("fa-folder").addClass("fa-folder-open");
            $('.folder'+id).addClass('active');
	        updateform_upload_file(id);
            //updatemetadata(id);
            $('#metadatadetail').html('<div class="form-group">'+
								'<div class="col-lg-4 col-sm-3">'+
									'<label for="name" class="control-label">&nbsp;</label>'+
								'</div>'+
								'<div class="col-lg-8 col-sm-9">'+
                                    '<p><i>Please select a file</i></p>'+
								'</div>' +
							'</div>	');
            id_curruent_folder = id;
	        id_folder_child_lama = id;
            $.ajax({
                url: "<?Php echo site_url('filemanagers/getfile'); ?>/"+id,
                dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $("#datafile").html('');
                    if(data.length > 0) {                        
                        for(var i=0; i < data.length; i++){
                            var current = data[i];
                            $("#datafile").append('<tr id="'+current.id+'" class="btn-default file" ><td>'+
                                                  '<?Php echo img('/assets/images/PDF.png'); ?> '+current.nama_files+'</td>'+
                                                  '<td>'+current.created_at+'</td><td>'+current.uploader+'</td></tr>');
                        }
                    } else {
                         $("#datafile").append('<tr><td colspan="3"><i>Folder ini Kosong. Silahkan Mengungah File Terlebih Dahulu.</i></td>');
                    }
                    getfile();
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
      });
        
      
    function updateform_upload_file(id)
    {
            $.ajax({
                url: "<?Php echo site_url(); ?>/filemanagers/getmetafile/"+id,
                //dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response                    
                    var data = response;
                    $('#form_uploadfile').html(data);
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
    }
        
    function updatemetadata(id){
        $('#metadatadetail').html('');
        $.ajax({
                url: "<?Php echo site_url(); ?>/filemanagers/getmetafiledetaile/"+id,
                //dataType: "json",
                //context: document.body
                success: function(response) {
                    // e.g. filter the response
                    
                    var data = response;
                    $('#metadatadetail').html(data);
                }
            }).done(function() {
                  //$( this ).addClass( "done" );
            });
    }	   
    $( "#search_file" ).submit(function( event ) {
            event.preventDefault();
            var url = this.action;
            var data = $("#file_search").val();
            $.post( url, { str: data }, function( data ) {
                $("#datafile").html(data);
            });
        });
               
    });
    function getmetadata(id){
        //console.log("<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id);
        $('.file').removeClass("btn-primary").addClass("btn-default");
        $('#'+id).removeClass("btn-default").addClass("btn-primary");
        
        $.ajax({                
            url: "<?php echo base_url(); ?>index.php/filemanagers/get_file_meta_data/"+id,                
            success: function(response) {
                //console.log(response);
                $("#metadatadetail").html(response);
                $("#"+id).css('font-weight','normal');
            }
        }).done(function() {
            //$( this ).addClass( "done" );
        });
    }
</script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>