<link href='<?php echo base_url() ?>assets/stylesheets/libs/filemanager/filemanager.css' rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css@1401442111.css" />

<div class="section-body">
	<div class="row">
        <div class="col-lg-6 bataskanan">
            <div id="filemanager" style="overflow:auto; width: 100%">
                <table id="filemanager" class="table" style="width: 100%">
                <thead>
                    <tr>
                        <th>Nama File</th>
                        <th>Tanggal</th>
                        <th>Uploader</th>
                    </tr>
                </thead>
                <tbody id="datafile">
                  <tr><td colspan="3" style="text-align : center">
                        <i>Belum ada file yang dicari</i></td>                    
                </tbody>
            </table>
            
                
            </div>
        </div>
        <div class="col-lg-6">            
            <div class="box ">					
					<div class="box-body no-padding">
                        <div style="height : 40px; background-color : #f4f4f4; border-bottom : solid 1px #b3b3b3 ">
                            <p style="margin-left : 10px; font-size : 13px; line-height : 40px;"><b>Meta Data</b></p>
                        </div>
						<div class="form-horizontal form-bordered" id="metadatadetail">
							<div class="form-group">
								<div class="col-lg-4 col-sm-3">
									<label for="name" class="control-label">&nbsp;</label>
								</div>
								<div class="col-lg-8 col-sm-9">
                                    <p><i>Silahkan memilih file terlebih dahulu.</i></p>
								</div>
							</div>														
						</div>
					</div>
				</div><!--end .box -->
        </div>
	</div>
</div>
<script>
    $(document).ready(function() {                        
        $('#search_tanggal').datetimepicker();
    });
</script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/javascripts/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>