<ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>"><i class="fa fa-fw fa-home"></i> Home</a></li>
	<li class="active"><a href="#"><i class="fa fa-fw fa-wrench"></i> Pemeliharaan</a></li>
</ol>
<div class="row" style="margin-top: 20px;">
	<div class="col-lg-12">
        <div class="box">
            <div class="box-head">
    		    <header><h4 class="text-light">Data<strong>Directory</strong></h4></header>
    		</div>
    		<div class="row">
        		<div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url() ?>index.php/backups/do_db_backup/" class="btn btn-info btn-block btn-labeled">
    					<span><i class="fa fa-code"></i></span>
    					<div>Backup Database</div>
    				</a>
    		    </div>
                <div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url() ?>index.php/backups/do_file_backup/" class="btn btn-success btn-block btn-labeled">
    					<span><i class="fa fa-file"></i></span>
    					<div>Backup Files</div>
    				</a>
    		    </div>
                <div class="col-lg-2" style="margin-left : 20px;">
    				<a href="<?php echo base_url() ?>index.php/backups/do_app_backup/" class="btn btn-default btn-block btn-labeled">
    					<span><i class="fa fa-desktop"></i></span>
    					<div>Backup Sistem</div>
    				</a>
    		    </div>
		    </div>
        	<div class="box-body table-responsive">
                <div class="box-head" style="margin-left : -20px">
                    <header><h4 class="text-light">Log<strong>Pemeliharaan</strong></h4></header>
                </div>
        		<table id="datatable1" class="table table-bordered table-hover">
        			<thead>
        				<tr>        					
        					<th>Waktu Backup</th>
        					<th>User</th>                                    					
        					<th>Kegiatan</th>
        				</tr>
        			</thead>
        	    	<tbody>
        				<?php foreach($log as $l) { ?>
                        <tr>
                            <td><?php echo $l['waktu'] ?></td>
                            <td><?php echo $l['nama_lengkap'] ?></td>
                            <td><?php echo $l['kegiatan'] ?></td>
                        </tr>
                        <?php } ?>
        			</tbody>
        		</table>
        	</div>
        </div>
	</div>
</div>
<script>
    $("#datatable1").dataTable({        
    });
</script>