<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class metas extends CI_Controller {
       public function __construct(){
            parent::__construct();                        
            $this->load->model('meta');            
            $this->load->model('log');
        }
        
        public function index(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Metadata';
                $parser['aktif'] = 'folder';
				$parser['view'] = 'metas/index';		
                $parser['metas'] = $this->meta->get();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function add(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Add Metadata';
                $parser['aktif'] = 'folder';
				$parser['view'] = 'metas/add';		                
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function create(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $nama = $this->input->post("nama_meta");
                $value = $this->input->post("value_meta");
                $id = $this->meta->create($nama);
                if($id!= 0 && $id!=null){
                    foreach($value as $v){
                        $this->meta->insert_value($id,$v);
                    }
                }
                 
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Membuat Metadata Baru dengan Nama '.$nama,0);
                 
                redirect(base_url()."index.php/metas/");
            } else {
                redirect(base_url());
            }
        }
        
        public function edit($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $metas = $this->meta->get_id($id);
                if($metas != null) {
                    $parser['title'] = 'Edit Metadata';
                    $parser['aktif'] = 'folder';
                    $parser['view'] = 'metas/edit';		
                    $parser['metas'] = $metas;
                    $parser['value'] = $this->meta->get_value($id);
                    $this->load->view('layout/admin',$parser);   
                } else {
                    show_404();
                }			
            } else {
                redirect(base_url());
            }
        }
        
        public function update(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $id = $this->input->post("id");
                $name = $this->input->post('nama_meta');
                $value = $this->input->post("value_meta");
                $cur_value = $this->meta->get_value($id);
                
                $this->meta->update($id,$name);
                
                //edited or deleted values
                foreach($cur_value as $cv){
                    $new_val = $this->input->post('value_meta_'.$cv['id']);
                    if($new_val != '' && $new_val != null) {
                        $this->meta->update_value($cv['id'],$new_val);
                    } else {
                        $this->meta->delete_value($id,$cv['id']);
                    }
                }
                
                //new values
                foreach($value as $v){
                    $this->meta->insert_value($id,$v);
                }
                
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Mengubah Metadata dengan Nama '.$name,0);
                
                redirect(base_url()."index.php/metas/");
            } else {
                redirect(base_url());
            }
        }
        
        public function delete($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $metas = $this->meta->get_id($id);
                if($metas != null) {
                    $parser['title'] = 'Edit Metadata';
                    $parser['aktif'] = 'folder';
                    $parser['view'] = 'metas/delete';		
                    $parser['meta'] = $metas;                    
                    $this->load->view('layout/admin',$parser);   
                }			
            }
        }
        
        public function do_delete($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $metas = $this->meta->get_id($id);
                $this->meta->delete($id);
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Menghapus Metadata dengan Nama '.$metas['name'],0);
                redirect(base_url()."index.php/metas/");
            }
        }
    }
?>