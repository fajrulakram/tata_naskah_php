<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class notifications extends CI_Controller {
        
        public function __construct(){
            parent::__construct();            
            $this->load->model('notification');
        }
        
        public function index(){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Notification';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = true;
                $parser['isintegrasi'] = false;
                $parser['view'] = 'notification/index';
                $parser['notification'] = $this->notification->get_all($this->session->userdata('id_user'));
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function view($id){
            if($this->session->userdata('id_user')!=null) {
                $notif = $this->notification->view($id,$this->session->userdata('id_user'));
                if($notif != null) {
                    $uri = $notif['url'];
                    if(strpos($uri,'folder') !== false){
                        $ids = substr($uri,strpos($uri,'folder')+6);
                        redirect(base_url()."index.php/filemanagers/index/".$ids);
                    } else {
                        redirect(base_url());
                    }
                } else {
                    show_404();
                }
            } else {
                redirect(base_url());
            }
        }
        
        public function get_notification($id){
            $notification = $this->notification->get($id);
            echo '<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-lg fa-envelope"></i><sup class="badge badge-support2">'.sizeof($notification).'</sup></a>
						<ul class="dropdown-menu animation-zoom" id="notif">';
            if(sizeof($notification)>0){
                foreach($notification as $notif) {
                    $avatar = base_url().$notif['avatar'];
                    if($notif['avatar']=="" || $notif['avatar']==null){
                        $avatar =  base_url()."assets/images/avatar.png";
                    }
                    echo '<li>
								<a class="alert alert-warning" href="'.base_url().'index.php/notifications/view/'.$notif['id'].'">
									<img class="pull-right img-circle dropdown-avatar" style="height : 40px; width : 40px;" 
                                         src="'.$avatar.'" />
									<strong>'.$notif['nama_lengkap'].'</strong><br/>
									<div>
                                        <small style="word-break: break-word;white-space:normal;">'.$notif['pesan'].'</small></div>
								</a>
							</li>';
                }
            } else {
                echo "<li>Tidak Ada Notifikasi Aktif</li>";
            }
            echo '<li><a href="'.base_url().'index.php/notifications/">View all messages <span class="pull-right">
                    <i class="fa fa-arrow-right"></i></span></a></li></ul>';
        }
        
    }
?>