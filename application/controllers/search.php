<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class search extends CI_Controller {
       public function __construct(){
            parent::__construct();
            //$this->load->model('organisasi');		
            $this->load->model('agenda');
            $this->load->model('berkasfile');
            $this->load->model('folder');
            $this->load->library('encrypt');
            if($this->session->userdata('id_user')==null){
                redirect('/', 'Location', 303);
            }
        }
        
        public function index(){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Advance Search';                
                $parser['issearch'] = true;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['file'] = null;
                $parser['view'] = 'search/index';                
                
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function result(){
            if($this->session->userdata('id_user')!=null) {                                
                date_default_timezone_set('Asia/Jakarta');
                $nama = $this->input->post("nama");
                $nomor = $this->input->post("nomor");
                $hal = $this->input->post("perihal");                
                $asal = $this->input->post("asal");                
                $tgl = date('Y-m-d',strtotime($this->input->post("tanggal")));                
                
                $parser['title'] = 'Advance Search - Result';
                $parser['issearch'] = true;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['file'] = $this->berkasfile->advance_search($nama, $nomor,$hal,$tgl,$asal);
                $parser['view'] = 'search/result';                
                
                $this->load->view('layout/user',$parser);                                
            } else {
                redirect(base_url());
            }
        }
        
    }
?>