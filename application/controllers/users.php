<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users extends CI_Controller {
	public function __construct(){
        parent::__construct();
	   	$this->load->model('user');		
        $this->load->model('organisasi');		
        $this->load->model('log');
  	}  		
	
	public function index(){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
			$parser['title'] = 'Pengguna';
			$parser['aktif'] = 'user';
			$parser['view'] = 'user/index';
			$parser['user'] =  $this->user->get();
			$this->load->view('layout/admin',$parser);
        } else {
            redirect(base_url());
        }
    }
    
    public function add(){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {    			
            $parser['title'] = 'Tambah Pengguna';
            $parser['aktif'] = 'user';
            $parser['view'] = 'user/add';
            $parser['organisasi'] =  $this->organisasi->get();
            $this->load->view('layout/admin',$parser);
        } else {
            redirect(base_url());
        }
    }
  
    public function view($id){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
            $userdata = $this->user->get($id)[0];
            $parser['title'] = 'Pengguna: ' . $userdata['username'];
            $parser['aktif'] = 'user';
            $parser['view'] = 'user/view';
            $parser['user'] =  $userdata;
            $this->load->view('layout/admin',$parser);        
        }
    }
    
    public function create(){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
            $username = $this->input->post("username");
            $password = md5($this->input->post("password"));
            $role = $this->input->post("peran");
            
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $organisasi = $this->input->post("organisasi");
            $nip = $this->input->post("nip");
            
            $config['upload_path'] = './uploads/avatars/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';   
            $config['encrypt_name'] = 'true';
            
            $this->load->library('upload', $config);
            $attachment = "";
            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());                
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());                
                $data = array('upload_data' => $this->upload->data());
				$attachment .= 'uploads/avatars/'.$data['upload_data']['file_name'];
            }
            
            $this->user->create($username,$password,$role,$nama,$email,$organisasi,$nip,$attachment);
            //add log
            $this->log->insert($this->session->userdata('id_user'),'Menambah pengguna baru dengan username '.$username,0);            
            
            redirect(base_url()."index.php/users/");
        } else {
            redirect(base_url());
        }
    }
    
    public function edit($id){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {             
            $current = $this->user->get($id);
            if(sizeof($current) > 0) {
                $parser['title'] = 'Edit Pengguna';
                $parser['aktif'] = 'user';
                $parser['view'] = 'user/edit';
                $parser['organisasi'] =  $this->organisasi->get();
                $parser['current'] = $current[0];
                $this->load->view('layout/admin',$parser);
            } else {
                show_404();
            }
        } else {
            redirect(base_url());
        }
    }
    
    public function update(){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
            $id = $this->input->post("id");            
            $username = $this->input->post("username");
            $cur_pwd = $this->input->post("cur_pwd");
            if($cur_pwd == $this->input->post("password")) {
                $password = $this->input->post("password");    
            } else {
                $password = md5($this->input->post("password"));
            }
            $role = $this->input->post("peran");
            
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $organisasi = $this->input->post("organisasi");
            $nip = $this->input->post("nip");
            
            $config['upload_path'] = './uploads/avatars/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';   
            $config['encrypt_name'] = 'true';
            
            $this->load->library('upload', $config);
            $attachment = "";
            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());                
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());                
                $data = array('upload_data' => $this->upload->data());
				$attachment .= 'uploads/avatars/'.$data['upload_data']['file_name'];
            }
            
            $this->user->update($id,$username,$password,$role,$nama,$email,$organisasi,$nip,$attachment);
            //add log
            $this->log->insert($this->session->userdata('id_user'),'Mengedit pengguna dengan username '.$username,0);            
            
            redirect(base_url()."index.php/users/");
        } else {
            redirect(base_url());
        }
    }
    
    public function delete($id){
         if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
            $userdata = $this->user->get($id)[0];
            $parser['title'] = 'Pengguna: ' . $userdata['username'];
            $parser['aktif'] = 'user';
            $parser['view'] = 'user/delete';
            $parser['user'] =  $userdata;
            $this->load->view('layout/admin',$parser);        
        }
    }
    
    public function do_delete($id){
        if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
            //get data
            $userdata = $this->user->get($id)[0];
            //do delete
            $this->user->delete($id);
            //add log
            $this->log->insert($this->session->userdata('id_user'),'Menghapus pengguna dengan username '.$userdata['username'],0);            
            
            $this->session->set_flashdata('message', 'Pengguna dengan username '.$userdata['username'].' Telah Dihapus!');
            redirect(base_url()."index.php/users/");
        }
    }
    
    public function editaction(){
        if($this->session->userdata('id_user')!=null){
            $id = $this->session->userdata("id_user");
            $this->load->library('image_lib');
			$config['upload_path']='./uploads/avatars/';
			$config['allowed_types']='PDF|pdf|jpg';
			$config['max_size']='100000';
            $config['max_width']='0';
			$config['max_height']='0';
			$config['overwrite']=TRUE;
            $config['encrypt_name'] = 'true';
            
			$this->load->library('upload',$config);
			
			if($this->upload->do_upload("file")){
                $gambar=$this->upload->data();
                $update = array("email" => $this->input->post("email"),
                               "nama_lengkap" => $this->input->post("nama_lengkap"),
                               "avatar" => "uploads/avatar/".$gambar['file_name']);
                if($this->input->post("password")!=null && $this->input->post("password")!=""){
                    $password = md5($this->input->post("password"));
                    $update["password"] = $password;
                    
                }
                $this->user->update_data_by_id($id, $update);
            } else {
                $update = array("email" => $this->input->post("email"),
                                "nama_lengkap" => $this->input->post("nama_lengkap")
                                );
                if($this->input->post("password")!=null && $this->input->post("password")!=""){
                    $password = md5($this->input->post("password"));
                    $update["password"] = $password;

                }
                $this->user->update_data_by_id($id, $update);
            }
        
            redirect("/filemanagers/profile", "location", 303);
        } else {
            redirect(base_url());
        }
    }
    
    public function is_exist(){
        $username = $this->input->post('username');
        $email = $this->input->post("email");
        $data['isexist'] = 0;
        if($this->user->is_exist($username,$email)){
            $data['isexist'] = 1;
        }
        
        echo json_encode($data);
    }
}