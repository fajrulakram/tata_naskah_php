<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class session extends CI_Controller {
	  public function __construct(){
		  parent::__construct();
	   	$this->load->model('user');		
  	}  		
	
	public function create(){
		$username = $this->input->post("username");
		$password = md5($this->input->post("password"));
		
		$user = $this->user->authenticate($username,$password);
		if($user != null) {
            //check double login
            if(!$this->user->is_already_login($user['username'])){
                $this->session->set_userdata('id_user',$user['id']);
                $this->session->set_userdata('username',$user['username']);
                $this->session->set_userdata('fullname',$user['nama_lengkap']);
                if($user['avatar'] != null) {
                    $this->session->set_userdata('avatar',base_url().$user['avatar']);
                } else {
                    $this->session->set_userdata('avatar',null);
                }			
                $this->session->set_userdata('role',$user['role']);					
            } else {
                $user = null;
                $this->session->set_flashdata('message', 'Maaf! Anda Tidak Bisa Login dengan Account ini');    
            }
		} else {
            $this->session->set_flashdata('message', 'Maaf! Username atau Password yang Anda Masukkan Salah');
        }
    
        if($user['role'] == 1){
            redirect('/filemanagers', 'Location', 303);
        } 

        redirect(base_url(), 'Location', 303);
    }
    
    public function destroy(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}