<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class logs extends CI_Controller {
       public function __construct(){
            parent::__construct();                        
            $this->load->model('log');            
        }
        
        public function index(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Logs';
                $parser['aktif'] = 'logs';
				$parser['view'] = 'logs/index';		
                $parser['logs'] = $this->log->get_user();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function folder(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Logs';
                $parser['aktif'] = 'logs';
				$parser['view'] = 'logs/folder';		
                $parser['logs'] = $this->log->get_folder();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
    }
?>