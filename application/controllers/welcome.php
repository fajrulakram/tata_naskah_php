<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('user');		
        $this->load->model('folder');	
        $this->load->model('log');
	}

	public function index()
	{
		if($this->session->userdata('id_user')!=null) {
			if($this->session->userdata('role')==0) {				
				$parser['title'] = 'Dashboard';
				$parser['aktif'] = 'dashboard';
				$parser['view'] = 'welcome/admin';
				$parser['user'] =  $this->user->get_limit();
                $parser['folder'] = $this->folder->get_limit();
                $parser['logs'] = $this->log->get_limit();
				$this->load->view('layout/admin',$parser);
			} else {
                redirect(base_url()."index.php/filemanagers/");
            }
		} else {
			$this->load->view('welcome/login');
		}
	}
}