<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class organisasis extends CI_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->model('organisasi');		
            $this->load->model('log');
        }  		  		
        
        public function index(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Organisasi';
                $parser['aktif'] = 'organisasi';
				$parser['view'] = 'organisasi/index';
				$parser['organisasi'] =  $this->organisasi->get();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }  
        
        public function add(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Tambah Organisasi';
				$parser['aktif'] = 'organisasi';
				$parser['view'] = 'organisasi/add';
				$parser['organisasi'] =  $this->organisasi->get();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
      
        public function create(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $nama_organisasi = $this->input->post("nama");
                $parent = $this->input->post("parent");
                if($parent == 0) {
                    $this->organisasi->create($nama_organisasi);
                } else {
                    $this->organisasi->create($nama_organisasi,$parent);
                }
                
                //add log
                $this->log->insert($this->session->userdata('id_user'),'Menambahkan organisasi baru dengan nama '.$org['nama_organisasi'],0);
                
                redirect(base_url()."index.php/organisasis/");
            } else {
                redirect(base_url());
            }
        }
        
        public function edit($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $current = $this->organisasi->get_id($id);
                if($current != null) {
                    $parser['title'] = 'Edit Organisasi';
                    $parser['aktif'] = 'organisasi';
                    $parser['view'] = 'organisasi/edit';
                    $parser['current'] =  $this->organisasi->get_id($id);
                    $parser['all'] =  $this->organisasi->get();
                    $this->load->view('layout/admin',$parser);
                } else {
                    show_404();
                }
            } else {
                redirect(base_url());
            }
        }
        
        public function update(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $id = $this->input->post("id");
                $nama_organisasi = $this->input->post("nama");
                $parent = $this->input->post("parent");
                if($parent == 0) {
                    $this->organisasi->update($id,$nama_organisasi);
                } else {
                    $this->organisasi->update($id,$nama_organisasi,$parent);
                }
                
                //add log
                $this->log->insert($this->session->userdata('id_user'),'Mengedit organisasi '.$org['nama_organisasi'],0);            
                
				redirect(base_url()."index.php/organisasis/");
            } else {
                redirect(base_url());
            }
        }
        
        public function delete($id){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $current = $this->organisasi->get_id($id);
                if($current != null) {
                    $parser['title'] = 'Delete Organisasi';
                    $parser['aktif'] = 'organisasi';
                    $parser['view'] = 'organisasi/delete';
                    $parser['org'] =  $this->organisasi->get_id($id);                    
                    $this->load->view('layout/admin',$parser);
                }
            }
        }
        
        public function do_delete($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $org = $this->organisasi->get_id($id);
                $this->organisasi->delete($id);
                //add log
                $this->log->insert($this->session->userdata('id_user'),'Menghapus organisasi '.$org['nama_organisasi'],0);            
                //set message
                $this->session->set_flashdata('message', 'Folder '.$org['nama_organisasi'].' Telah Dihapus!');
                
                redirect(base_url()."index.php/organisasis/");
            }
        }
    }
?>