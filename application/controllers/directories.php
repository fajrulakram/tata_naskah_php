<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class directories extends CI_Controller {
        public function __construct(){
            parent::__construct();            
            $this->load->model('folder');
            $this->load->model('organisasi');
            $this->load->model('notification');
            $this->load->model('log');
            $this->load->model('berkasfile');
            $this->load->model('meta');
            $this->load->model('user');
        }  	
        
        public function index(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Directory';
                $parser['aktif'] = 'folder';
				$parser['view'] = 'directory/index';		
                $parser['folder'] = $this->folder->get_all();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function add(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Directory Baru';
                $parser['aktif'] = 'folder';
				$parser['view'] = 'directory/new';				                    
                $parser['parents'] = $this->folder->get_all_root();
                $parser['organisasi'] = $this->organisasi->get();
                $parser['custom_meta'] = $this->meta->get();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function create(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $nama = $this->input->post("nama_folder");
                $parent = $this->input->post("parent");
                $akses = $this->input->post("hak_akses");
                $user = $this->input->post("user_akses");
                $meta_field = $this->input->post("nama_meta");
                $meta_tipe = $this->input->post("tipe_meta");
               
                $meta = array();
                $idx = 0;
                foreach($meta_field as $mf) {
                    $temp['name'] = $mf;
                    $temp['type'] = $meta_tipe[$idx];                    
                    $idx++;
                    $meta[sizeof($meta)] = $temp;
                }
                
                $hak_akses = "";
                for($i=0;$i<sizeof($akses);$i++) {                
                    $hak_akses .= $akses[$i];
                    if($i<sizeof($akses)-1) {
                        $hak_akses .= ",";
                    }
                }                
                
                $id_folder = $this->folder->create($nama,$parent,$hak_akses);                
                if($id_folder != null && $id_folder != 0) {
                    //insert metas
                    $position = 1;
                    foreach($meta as $mt){
                        $this->folder->set_meta($id_folder,$mt['name'],$mt['type'],$position);
                        $position++;
                    }
                    //insert user
                    foreach($user as $us){
                        $this->folder->set_akses_folder($id_folder,$us);
                        //send notification to users
                        $this->notification->add($this->session->userdata('id_user'),$us,
                                                 "Menambahkan anda ke Folder <b>".$nama."</b>","#folder".$id_folder);
                        
                        $tujuans = $this->user->get($us);
                        $emails = "";
                        foreach($tujuans as $em){
                            $emails = $em['email'];
                        }
                        //send email       
                        if($emails!="") {
                            //send email       
                            $config['mailtype'] = "html";
                            $this->load->library('email',$config);

                            $this->email->from('support@torche.co.id', 'Admin Adminstrasi Pengangkutan Migas');
                            $this->email->to($emails);                         
                            $this->email->subject('Email Test');
                            $this->email->message('
                            <div>                    
                                <div class="logo" style="height : 50px; float : left; margin-top : 5px; max-height:50px">
                                    <img style="height:50px;max-height:50px;" src="'.base_url().'/assets/images/Logo_ESDM.png">
                                </div>
                                <div style="height:50px;margin-left : 60px; width : 600px;">
                                    <h2 class="text-logo" style="vertical-align : middle;">
                                        Aplikasi Sistem Administrasi Pengangkutan Migas</h2>
                                    <h3>Sub Direktorat Pengangkutan Migas</h3>
                                <div>
                            </div>
                            <div>    
                                <h2> Admin Aplikasi Pengangkutan Migas Menambahkan anda ke Folder <b>'.$nama.'</b> <h2>
                            </div>');	

                            $this->email->send();
                        }
                    }
                }
                
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Membuat Folder Baru dengan Nama '.$nama,0);
                            
                redirect(base_url()."index.php/directories/");
            } else {
                redirect(base_url());
            }
        }
        
        public function edit($id){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $folder = $this->folder->detail($id) ;
                if($folder != null)  {
                    
                        $parser['title'] = 'Edit Directory';
                        $parser['aktif'] = 'folder';
                        $parser['view'] = 'directory/edit';				    
                        $parser['parents'] = $this->folder->get_all_root();
                        $parser['organisasi'] = $this->organisasi->get();
                        $parser['folder'] = $folder;
                        $parser['users'] = $this->folder->get_user_folder($id);
                        $parser['custom_meta'] = $this->meta->get();
                        $this->load->view('layout/admin',$parser);                    
                    
                } else {
                    show_404();
                }
            } else {
                redirect(base_url());
            }
        }
        
        public function update(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $id = $this->input->post("id");
                $nama = $this->input->post("nama_folder");
                $parent = $this->input->post("parent");
                $akses = $this->input->post("hak_akses");
                $user = $this->input->post("user_akses");
                
                $hak_akses = "";
                for($i=0;$i<sizeof($akses);$i++) {                
                    $hak_akses .= $akses[$i];
                    if($i<sizeof($akses)-1) {
                        $hak_akses .= ",";
                    }
                }
                //update basic information
                $this->folder->update($id,$nama,$parent,$hak_akses);
                //send notification to new users
                foreach($user as $us){
                    //check if new
                    if(!$this->folder->is_user($id,$us)){
                        //send notification
                        $this->notification->add($this->session->userdata('id_user'),$us,
                                                 "Menambahkan anda ke Folder <b>".$nama."</b>","#folder".$id);
                    }
                }
                //update user access                
                $this->folder->remove_akses_folder($id);
                foreach($user as $us){
                    //give parent akses
                    if($parent != null && $parent != 0) {
                        $this->folder->set_akses_parent($parent,$us);
                    }
                    //give folder akses
                    $this->folder->set_akses_folder($id,$us);
                    
                    $tujuans = $this->user->get($us);
                    $emails = "";
                    foreach($tujuans as $em){
                        $emails = $em['email'];
                    }
                    
                    //send email       
                    if($emails!="") {
                        $config['mailtype'] = "html";
                        $this->load->library('email',$config);
                        
                        $this->email->from('support@sagara.asia', 'Admin Adminstrasi Pengangkutan Migas');
                        $this->email->to($emails);                         
                        $this->email->subject('Email Test');
                        $this->email->message('
                        <div>                    
						    <div class="logo" style="height : 50px; float : left; margin-top : 5px; max-height:50px">
							    <img style="height:50px;max-height:50px;" src="'.base_url().'/assets/images/Logo_ESDM.png">
						    </div>
						    <div style="height:50px;margin-left : 60px; width : 600px;">
							    <h2 class="text-logo" style="vertical-align : middle;">
                                    Aplikasi Sistem Administrasi Pengangkutan Migas</h2>
                                <h3>Sub Direktorat Pengangkutan Migas</h3>
						    <div>
                        </div>
                        <div>    
                            <h3> Admin Aplikasi Pengangkutan Migas Menambahkan anda ke Folder <b>'.$nama.'</b> <h3>
                        </div>');	

                        $this->email->send();
                    }
                }                
                
                //update metafield
                //get current meta
                $meta = $this->folder->get_metas($id);
                //check for update
                foreach($meta as $mt){
                    $name = $this->input->post('name_'.$mt['id']);
                    $type = $this->input->post('type_'.$mt['id']);
                    $position = $this->input->post('position_'.$mt['id']);
                    if($name == null || $name == ""){
                        //meta deleted
                        $this->folder->delete_meta($mt['id']);
                    } else {
                        //meta updated
                        $this->folder->update_meta($mt['id'],$name,$type,$position);
                    }
                }
                
                //add new meta
                $meta_field = $this->input->post("nama_meta");
                $meta_tipe = $this->input->post("tipe_meta");
                $meta_pos = $this->input->post("position");
               
                $meta = array();
                $idx = 0;
                foreach($meta_field as $mf) {   
                    //ad new meta
                    $this->folder->set_meta($id,$mf,$meta_tipe[$idx],$meta_pos[$idx]);
                    $idx++;
                }                                                                                
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Mengubah Folder '.$nama,0);
                
                redirect(base_url()."index.php/directories/");
            } else {
                redirect(base_url());
            }
        }
        
        public function delete($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $folder = $this->folder->detail($id) ;
                if($folder != null)  {
                    if($folder['status']=="changeable") {
                        $parser['title'] = 'Delete Directory';
                        $parser['aktif'] = 'folder';
                        $parser['view'] = 'directory/delete';				                        
                        $parser['folder'] = $folder;
                        $parser['users'] = $this->folder->get_user_folder($id);
                        $this->load->view('layout/admin',$parser);                    
                    }
                }
            }
        }
        
        public function do_delete($id){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {                
                $folder = $this->folder->detail($id);
                $file = $this->berkasfile->get($id);
                $this->folder->remove_akses_folder($id);
                $this->folder->remove_all_meta($id);
                $this->folder->delete($id);
                foreach($file as $f) {
                    $data=$this->berkasfile->getfilebyid($f['id']);
                    if($data!=null){
                        $this->berkasfile->delete($id);
                        $file_name="./uploads/files/".$data->url;
                        if(file_exists($file_name)){
                            unlink($file_name);
                        }
                    }
                }
                //add logs
                $this->log->insert($this->session->userdata('id_user'),'Menghapus Folder '.$folder['nama_folder'],0);
                redirect(base_url()."index.php/directories/");
            }
        }
        
        public function get_akses($id){
            $folder = $this->folder->detail($id);
            $akses['data'] = $folder['hak_akses'];
            
            echo json_encode($akses);
        }
    }
?>