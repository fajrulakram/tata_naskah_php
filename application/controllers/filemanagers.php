<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class filemanagers extends CI_Controller {
        public function __construct(){
            parent::__construct();
            //$this->load->model('organisasi');		
            $this->load->model('berkasfile');
            $this->load->model('folder');
            $this->load->model('meta');
            $this->load->model('user');
            $this->load->model('log');
            $this->load->model('agenda');
            $this->load->library('encrypt');
            if($this->session->userdata('role') != 1){
                redirect('/', 'Location', 303);
            }
        }
        
        public function index($current=NULL){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Filemanager';
                $parser['issearch'] = false;
                $parser['isfolder'] = true;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['view'] = 'filemanager/index';
                $parser['current'] = $current;
                $parser['folder'] =  $this->folder->getroot($this->session->userdata('id_user'));
                $parser['file'] = $this->berkasfile->get_file_evaluator($this->session->userdata('id_user'));
                $parser['count_eval'] = $this->berkasfile->count_file_evaluator($this->session->userdata('id_user'));
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function csv($id_folder=null){
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=data.csv');
            
            $output = fopen('php://output', 'w');
            $folders = $this->folder->get_metas($id_folder);
            $header = [];
            $z=0;
            $h=1;
            $header[0]="Nama file";
            foreach($folders as $f){
                $header[$h]=$f['nama'];
                $h++;
            }
            
            $row = [];
            $i = 0;
            $files = $this->berkasfile->get($id_folder);
            
            
            foreach($files as $file){
                $meta = $this->berkasfile->get_meta_data($file['id']);
                
                $val = [];
                $met = [];
                $row[$i][0]=$file['nama_files'];
                foreach($meta as $m){
                    //$row = [];
                        $val[$m['nama']]="";
                        $met[$m['nama']]="";
//                     if(!$this->subset($m['nama'], $header)){
//                         $header[$z]=$m['nama'];
//                         $key = $this->get_key($m['nama'], $header);
//                         $user = $this->user->get(intval($m['value']));
//                         $row[$i][$key]=$m['value'];
//                         if($m['type']=="user"){
//                             for($l=0; $l < count($user); $l++){
//                                 $val = $val."".$user[$l]['nama_lengkap'];
//                                 if($l != (count($user))){
//                                     $val = $val.", ";
//                                 }
//                             }
//                             $row[$i][$key]=$val;
                        
//                         }
                        
//                         $z++;
//                     } else {
                    if($this->subset($m['nama'], $header)){
                        $user = $this->user->get(intval($m['value']));
                        
                        $key = $this->get_key($m['nama'], $header);
                        $row[$i][$key]=$m['value'];
                        if($m['type']=="user"){
                            for($l=0; $l < count($user); $l++){
                                $val[$m['nama']] = $val[$m['nama']]."".$user[$l]['nama_lengkap'].", ";
                                
                            }
                            $row[$i][$key]=$val[$m['nama']];
                        
                        } else if(is_numeric($m['type']) && $m['value'] != null){
                            $meta_value = $this->meta->get_value_id($m['value']);
                            $met[$m['nama']] = $met[$m['nama']]."".$meta_value['value'];
                            $row[$i][$key]=$met[$m['nama']];
                        }
                        
                    }
                    
                    
                }
                //print_r($row);
                //fputcsv($output, $row);
                $i++;
            }
            fputcsv($output, $header);
            for($int = 0; $int < count($row); $int++){
                fputcsv($output, $row[$int]);
            }
        }
        
        function subset($string, $array){
            $t = false;
            for($i=0; $i < count($array); $i++ ){
                if($array[$i] == $string ){
                    $t = true;
                }
            }
            return $t;
        }
        
        function get_key($string, $array){
            $key = 0;
            foreach (array_values($array) as $i => $value) {
                if($value == $string){
                    $key = $i;
                    //echo "ke ambil $i: $value\n";
                }
              //echo "$i: $value\n";
            }
            return $key;
        }
        
        function download($id_file)
        {
            $this->load->helper('download');
            $path="";
            $name="";
            $data=$this->berkasfile->getfilebyid($id_file);
            if($data!=null){
                $path="/uploads/files/".$data->url;
                $ext = explode(".", $data->url)[1];
                $name=$data->nama_files;
                $pth    =   file_get_contents(base_url()."/uploads/files/$data->url");
                $nme    =   $name.".".$ext;
                force_download($nme, $pth);
            } else {
                echo "data tidak di temukan";
            }
            
            
        }
      
        public function getfolder()
        {
            $data=$this->folder->get($this->session->userdata('id_user'));
            echo json_encode($data);
        }
      
        public function getfile($id_folder)
        {
            $data=$this->berkasfile->getfilefolder($this->session->userdata('id_user'), $id_folder);
            echo json_encode($data);
        }
        
        public function getfilebyid($id_file)
        {
            $data=$this->berkasfile->getfilebyid($id_file);
            echo json_encode($data);
        }
        
        public function getformedit($id_file)
        {
            $file = $this->berkasfile->getfilebyid_array($id_file);          
            if($file!=null){                
                $meta = $this->berkasfile->get_meta_data($id_file);
                echo '<div class="form-group">';
                  echo '<div class="col-md-4">';
                    echo '<label class="control-label">Name</label>';
                  echo '</div>';

                  echo '<div class="col-md-8">';
                    echo '<input type="text" value="'.$file["nama_files"].'" name="nama_file" id="nama_file_edit" class="form-control">';
                  echo '</div>';
                echo '</div>';
                echo form_hidden("folder_id", $file['folder_id']);
                echo form_hidden("file_id", $file['id']);
                echo form_hidden("url", $file['url']);
                echo '<div class="form-group">';
                  echo '<div class="col-md-4">';
                    echo '<label class="control-label">File</label>';
                  echo '</div>';

                  echo '<div class="col-md-8">';
                    echo '<input type="file" name="file" class="form-control">';
                  echo '</div>';
                echo '</div>';                
                if(sizeof($meta) > 0){
                    $isdone = array();
                    foreach($meta as $h){
                        //$h=$hasil[$i];
                        $label=$h['nama'];
                        if($h['type']=="string"){
                            $la = strtolower($label);
                            if(strchr($la, 'perihal') != null ){
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input id="perihal_edit" type="text" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                            } else if(strchr($la, 'dmoa') != null ){
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input id="dmoa_edit" type="text" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                            } else{
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input type="text" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                            }
                            
                        } else if($h['type']=="integer"){                            
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';

                              echo '<div class="col-md-8">';
                                echo '<input type="number" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                class="form-control" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else if($h['type']=="date"){
                            $la = strtolower($label);
                            if(strchr($la, 'berangkat') != null ){
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input type="text" id="wmulai_edit" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control tanggals" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                                echo '<input type="hidden" id="start_lama" name="start_lama" value="'.$h['value'].'" >';
                                
                                //echo form_hidden("start_lama", $h['value']);
                            } else if(strchr($la, 'kembali') != null){
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input type="text" id="wakhir_edit" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control tanggals" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                                echo '<input type="hidden" id="end_lama" name="end_lama" value="'.$h['value'].'" >';
                                
                                //echo form_hidden("end_lama", $h['value']);
                            } else {
                                echo '<div class="form-group">';
                                  echo '<div class="col-md-4">';
                                    echo '<label class="control-label">'.$label.'</label>';
                                  echo '</div>';

                                  echo '<div class="col-md-8">';
                                    echo '<input type="text" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                    class="form-control tanggals" placeholder="'.$label.'">';
                                  echo '</div>';
                                echo '</div>';
                            }
                            
                        } else if($h['type']=="datetime"){                            
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';
                              echo '<div class="col-md-8">';
                                echo '<input type="text" name="value_'.$h['meta_id'].'" value="'.$h['value'].'" 
                                class="form-control tanggals" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else if($h['type']=="user"){
                            if(!in_array($h['meta_id'],$isdone)) {
                                echo '<div class="form-group">';
                                echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                                echo '</div>';
                                echo '<div class="col-md-8">';                            
                                $user = $this->user->get();
                                foreach($user as $us) {
                                    $user = '<input class="_pelaksana_edit" type="checkbox" name="value_'.$h['meta_id'].'[]" value="'.$us['id'].'"';
                                    if($this->berkasfile->is_meta_user($id_file,$h['meta_id'],$us['id'])) {
                                        $user .= " checked ";
                                    }
                                    $user .= '>  '.$us['nama_lengkap'].'<br/>';
                                    echo $user;
                                }
                                echo '</div>';
                                echo '</div>';
                                $isdone[sizeof($isdone)] = $h['meta_id'];
                            }
                        } else {                             
                            if(!in_array($h['meta_id'],$isdone)) {
                                $meta_value = $this->meta->get_value(intval($h['type']));                            
                                if(sizeof($meta_value)>0) {
                                    echo '<div class="form-group">';
                                      echo '<div class="col-md-4">';
                                        echo '<label class="control-label">'.$label.'</label>';
                                      echo '</div>';
                                      echo '<div class="col-md-8">';                                
                                        foreach($meta_value as $mv) {
                                            $custom = '<input type="checkbox" name="value_'.$h['meta_id'].'[]" value="'.$mv['id'].'"';
                                            if($this->berkasfile->is_meta_checked($id_file,$h['meta_id'],$mv['id'])){
                                                $custom .= " checked ";
                                            }
                                            $custom .= '>  '.$mv['value'].'<br/>';
                                            echo $custom;
                                        }
                                      echo '</div>';
                                    echo '</div>';
                                }
                                $isdone[sizeof($isdone)] = $h['meta_id'];
                            }
                        } 
                    }
                }
            }            
        }
      
        public function getmetafile($id_folder){
            $data=$this->folder->get_metas($id_folder);
            //$hasil = json_decode($data->meta_field, true);            
            echo '<div class="form-group">';
              echo '<div class="col-md-4">';
                echo '<label class="control-label">Name</label>';
              echo '</div>';

              echo '<div class="col-md-8">';
                echo '<input type="text" name="nama_file" class="form-control" id="nama_file">';
              echo '</div>';
			echo '</div>';
            
            echo '<div class="form-group">';
              echo '<div class="col-md-4">';
                echo '<label class="control-label">File</label>';
              echo '</div>';

              echo '<div class="col-md-8">';
                echo '<input type="file" name="file" class="form-control">';
              echo '</div>';
			echo '</div>';
            if(sizeof($data)>0){
                foreach($data as $h){
                    //$h=$hasil[$i];
                    $label=$h['nama'];
                    if($h['type']=="string"){     
                        $la = strtolower($label);
                        if(strchr($la, 'perihal') != null ){
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';
                              echo '<div class="col-md-8">';
                                echo '<input id="perihal" type="text" name="value_'.$h['id'].'" class="form-control" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else if(strchr($la, 'dmoa') != null ){
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';
                              echo '<div class="col-md-8">';
                                echo '<input id="dmoa" type="text" name="value_'.$h['id'].'" class="form-control" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else {
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';
                              echo '<div class="col-md-8">';
                                echo '<input type="text" name="value_'.$h['id'].'" class="form-control" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        }
                        
                    } else if($h['type']=="integer"){                        
                        echo '<div class="form-group">';
                          echo '<div class="col-md-4">';
                            echo '<label class="control-label">'.$label.'</label>';
                          echo '</div>';

                          echo '<div class="col-md-8">';
                            echo '<input type="number" name="value_'.$h['id'].'" class="form-control" placeholder="'.$label.'">';
                          echo '</div>';
                        echo '</div>';
                    } else if($h['type']=="date"){                       
                        $la = strtolower($label);
                        if(strchr($la, 'berangkat') != null ){
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';

                              echo '<div class="col-md-8">';
                                echo '<input id="wmulai" type="text" name="value_'.$h['id'].'" class="form-control tanggals" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else if(strchr($la, 'kembali') != null ){
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';

                              echo '<div class="col-md-8">';
                                echo '<input id="wakhir" type="text" name="value_'.$h['id'].'" class="form-control tanggals" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        } else {
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';

                              echo '<div class="col-md-8">';
                                echo '<input type="text" name="value_'.$h['id'].'" class="form-control tanggals" placeholder="'.$label.'">';
                              echo '</div>';
                            echo '</div>';
                        }
                        
                    } else if($h['type']=="datetime"){   
                        echo '<div class="form-group">';
                          echo '<div class="col-md-4">';
                            echo '<label class="control-label">'.$label.'</label>';
                          echo '</div>';
                          echo '<div class="col-md-8">';
                            echo '<input type="text" name="value_'.$h['id'].'" class="form-control tanggals" placeholder="'.$label.'">';
                          echo '</div>';
                        echo '</div>';
                    } else if($h['type']=="user"){
                        echo '<div class="form-group">';
                          echo '<div class="col-md-4">';
                            echo '<label class="control-label">'.$label.'</label>';
                          echo '</div>';
                          echo '<div class="col-md-8">';
                            $user = $this->user->get();
                            foreach($user as $us) {
                                echo '<input class="_pelaksana" type="checkbox" name="value_'.$h['id'].'[]" value="'.$us['id'].'">  '.$us['nama_lengkap'].'<br/>';
                            }
                          echo '</div>';
                        echo '</div>';
                    } else {
                        $meta_value = $this->meta->get_value(intval($h['type']));
                        if(sizeof($meta_value)>0) {
                            echo '<div class="form-group">';
                              echo '<div class="col-md-4">';
                                echo '<label class="control-label">'.$label.'</label>';
                              echo '</div>';
                              echo '<div class="col-md-8">';                                
                                foreach($meta_value as $mv) {
                                    echo '<input type="checkbox" name="value_'.$h['id'].'[]" 
                                        value="'.$mv['id'].'">  '.$mv['value'].'<br/>';
                                }
                              echo '</div>';
                            echo '</div>';
                        }
                    }
                }
            }
        }
        
        public function getmetafiledetaile($id_folder){
            $data=$this->folder->getfolderbyid($id_folder);
            $hasil = json_decode($data->meta_field, true);
            
            if($hasil != null){
                foreach($hasil as $h){
                    //$h=$hasil[$i];
                    if($h['type']=="string" && $h['status']=="1"){
                        $label=$h['name'];
                        $name=str_replace(" ","",$label);
                            echo '<div class="form-group">';
								echo '<div class="col-lg-4 col-sm-3">';
									echo '<label for="name" class="control-label">'.strtoupper($h['name']).'</label>';
								echo '</div>';
								echo '<div class="col-lg-8 col-sm-9">';
									echo '<p id="'.$name.'"> - </p>';
								echo '</div>';
							echo '</div>';
                    } else if($h['type']=="integer" && $h['status']=="1"){
                        $label=$h['name'];
                        $name=str_replace(" ","",$label);
                        echo '<div class="form-group">';
								echo '<div class="col-lg-4 col-sm-3">';
									echo '<label for="name" class="control-label">'.strtoupper($h['name']).'</label>';
								echo '</div>';
								echo '<div class="col-lg-8 col-sm-9">';
									echo '<p id="'.$name.'"> - </p>';
								echo '</div>';
							echo '</div>';
                    } else if($h['type']=="date" && $h['status']=="1"){
                        $label=$h['name'];
                        $name=str_replace(" ","",$label);
                        echo '<div class="form-group">';
								echo '<div class="col-lg-4 col-sm-3">';
									echo '<label for="name" class="control-label">'.strtoupper($h['name']).'</label>';
								echo '</div>';
								echo '<div class="col-lg-8 col-sm-9">';
									echo '<p id="'.$name.'"> - </p>';
								echo '</div>';
							echo '</div>';
                    } else if($h['type']=="datetime" && $h['status']=="1"){
                        $label=$h['name'];
                        $name=str_replace(" ","",$label);
                            echo '<div class="form-group">';
								echo '<div class="col-lg-4 col-sm-3">';
									echo '<label for="name" class="control-label">'.strtoupper($h['name']).'</label>';
								echo '</div>';
								echo '<div class="col-lg-8 col-sm-9">';
									echo '<p id="'.$name.'"> - </p>';
								echo '</div>';
							echo '</div>';
                    }
                }
            }
        }
        
        public function get_file_meta_data($id){
            $metadata = $this->berkasfile->get_meta_data($id);
            //change status for evaluator
            $this->berkasfile->change_eval_status($this->session->userdata('id_user'),$id);
            if(sizeof($metadata) > 0) {                
                $prev = 0;
                foreach($metadata as $mt) {
                    echo 
                        '<div class="form-group">
                            <div class="col-lg-4 col-sm-3">';
                    if($prev != $mt['meta_id']) {
                        echo '<label for="name" class="control-label">'.strtoupper($mt['nama']).'</label>';
                        $prev = $mt['meta_id'];
                    } else {
                        echo '<label for="name" class="control-label">&nbsp;</label>';
                    }
                    echo '</div>
                            <div class="col-lg-8 col-sm-9">';
                    if($mt['type']=="user") {
                        if($mt['value']!="" && $mt['value']!=null) {
                            $user = $this->user->get(intval($mt['value']));
                            echo '<p>'.$user[0]['nama_lengkap'].'</p>';
                        } else {
                            echo '<p>-</p>';
                        }
                    } else if($mt['type'] != "integer" && $mt['type'] != "string" && $mt['type'] != "date" && $mt['type'] != "datetime") {
                        $mv = $this->meta->get_value_id(intval($mt['value']));
                        echo '<p>'.$mv['value'].'</p>';
                    } else {
                        echo '<p>'.$mt['value'].'</p>';
                    }
                    echo ' </div>
                        </div>';                                        
                }
            } else {
                echo "";
            }
        }
        
        public function search(){
            $str = $this->input->post("str");
            $id = $this->session->userdata('id_user');
            
            $result = $this->berkasfile->search($id,$str);
            if(sizeof($result)>0){
                foreach($result as $rs) {
                    echo '<tr id="'.$rs['id'].'" class="btn-default file" onclick="getmetadata(\''.$rs['id'].'\')">
                            <td> '.img('assets/images/PDF.png').' '.$rs['nama_files'].' </td>
                            <td>'.$rs['created_at'].'</td>
                            <td>'.$rs['uploader'].'</td>
                        </tr>';
                }
            } else {
                echo '<tr><td colspan="3"><i>File Not Found. No String Matched.</i></td>';
            }
        }
      
        public function addfile($id_folder, $isagenda=0){
            $nama_file = $this->input->post('nama_file');
            $folder = $this->folder->getfolderbyid($id_folder);
            $name_encryp = $this->encrypt->encode($nama_file);
            $idakses = 0;
            $kembalian=[];
            $name_user = "";
            
            $this->load->library('image_lib');
			$config['upload_path']='./uploads/files/';
			$config['allowed_types']='PDF|pdf|jpg|jpeg|JPG|JPEG|doc|docx|PNG|png|xls|xlsx';
			$config['max_size']='100000';
            $config['file_name']=$name_encryp;			
			$config['max_width']='0';
			$config['max_height']='0';
			$config['remove_spaces']=TRUE;
			$config['overwrite']=FALSE;
			$this->load->library('upload',$config);
			
			if($this->upload->do_upload("file")){
                $gambar=$this->upload->data();
                $hasil = json_decode($folder->meta_field, true);                                
                $insert=array("nama_files" => $nama_file,                             
                             "url" => $gambar['file_name'],
                             "user_id" => $this->session->userdata('id_user'),
                             "folder_id" => $id_folder);
                $idfile = $this->berkasfile->insertberkas($insert);                                                
                //echo $idfile."<br/>";
                $nomor = "";
                if($idfile!=0){
                    //insert meta
                    $meta = $this->folder->get_metas($id_folder);                    
                    foreach($meta as $mt){
                        $value = $this->input->post('value_'.$mt['id']);
                        if(is_array($value)) {                            
                            foreach($value as $v){
                                $this->berkasfile->insert_meta($idfile,$mt['id'],$v);    
                                //check if meta type = user
                                if($mt['type']=='user'){
                                    $name_user = 'value_'.$mt['id'];
                                    $this->berkasfile->insert_eval($idfile,$v);    
                                }
                            }                            
                        } else {
                            if(strcasecmp('nomor',$mt['nama'])==0){
                                $nomor = $value;
                            } else {
                                if(strpos($mt['nama'],'nomor') != false && $nomor != "") {
                                    $nomor = $value;
                                }
                            }
                            $this->berkasfile->insert_meta($idfile,$mt['id'],$value);
                        }
                    }                    
                    //insert akses
                    $insert2 = array("user_id" => $this->session->userdata('id_user'), "berkasfile_id" => $idfile);
                    $idakses = $this->berkasfile->insertakses($insert2);
                    //echo true;
                    $this->session->set_flashdata("success", "Insert Berhasil");
                    $kembalian['pesan']="Insert Berhasil";
                    //add logs
                    if($nomor != "") {
                        $this->log->insert($this->session->userdata('id_user'),'Mengunggah File Baru dengan Nama <b>'
                                       .$nama_file.'</b> dan Nomor Surat <b>'.$nomor.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
                    } else {
                        $this->log->insert($this->session->userdata('id_user'),'Mengunggah File Baru dengan Nama <b>'
                                       .$nama_file.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
                    }
                } else {
                    //echo false;
                    $kembalian['pesan']="Insert database gagal";
                    $this->session->set_flashdata("error", "Insert database gagal");
                }
            } else {
                //echo false;
                $kembalian['pesan']=$this->upload->display_errors('<p>', '</p>');
                $this->session->set_flashdata("error", $this->upload->display_errors('<p>', '</p>'));
            }
            
            if($id_folder==13 || $isagenda==1){
                //echo $kembalian['status']=$idakses;
                if($idakses!=0){
                    //echo "masuk";
                    $this->addfileagenda($name_user);
                }
                redirect('/filemanagers/index/'.$id_folder, 'Location', 303);
                //echo json_encode($kembalian);
            } else {
                redirect('/filemanagers/index/'.$id_folder, 'Location', 303);
            }
            
            
        }
        
        public function addfileagenda($name_user){
            date_default_timezone_set('Asia/Jakarta');
            
            $name_b = strtotime("now");
            $name_encryp = $this->encrypt->encode($name_b);
            
            $this->load->library('image_lib');
            $config2['upload_path']='./uploads/agenda/';
            $config2['allowed_types']='PDF|pdf|jpg';
            $config2['max_size']='100000';
            $config2['file_name']=$name_encryp;          
            $config2['max_width']='0';
            $config2['max_height']='0';
            $config2['remove_spaces']=TRUE;
            $config2['overwrite']=FALSE;
            //$this->load->library('upload',$config);
            $this->upload->initialize($config2);
            
            $tipe = $this->input->post('tipe');
            $date = $this->input->post('waktuM');
            $waktuM = date('Y-m-d H:i:s', strtotime($date));
            
            $date = $this->input->post('waktuA');
            $waktuA = date('Y-m-d H:i:s', strtotime($date));
            
            if($this->upload->do_upload("file")){
                $gambar=$this->upload->data();
                $insert=array("title" => $this->input->post('judul'),                             
                             "file" => $gambar['file_name'],
                             "tempat" => $this->input->post('value_53'),
                             "start" => $waktuM,
                             "end" => $waktuA,
                             "tipe" => $tipe,
                             "description" => $this->input->post('value_54')
                             
                             );
                $idfile = $this->agenda->insert($insert);                                                
                //echo $idfile."<br/>";
                if($idfile!=0){                    
                    //cek tipe
                    if($tipe=="dinas"){
                        $user = $this->input->post($name_user);
                        foreach($user as $us){
                            //insert pelaksana
                            $this->agenda->insert_pelaksana($idfile,$us);
                            //add notification to users
                        }
                    }                    
                    $this->session->set_flashdata("success", "Insert Berhasil");
                } else {
                    $this->session->set_flashdata("error", "Insert database gagal");
                }
            } else {
                //echo $this->upload->display_errors('<p>', '</p>');
                $this->session->set_flashdata("error", $this->upload->display_errors('<p>', '</p>'));
            }
            
            //redirect('/agendas', 'Location', 303);
            
        }
        
        
        public function editfileagenda($name_user, $agenda_sekarang){
            date_default_timezone_set('Asia/Jakarta');
            
            $name_b = strtotime("now");
            $name_encryp = $this->encrypt->encode($name_b);
            
            $this->load->library('image_lib');
            $config2['upload_path']='./uploads/agenda/';
            $config2['allowed_types']='PDF|pdf|jpg';
            $config2['max_size']='100000';
            $config2['file_name']=$name_encryp;          
            $config2['max_width']='0';
            $config2['max_height']='0';
            $config2['remove_spaces']=TRUE;
            $config2['overwrite']=FALSE;
            //$this->load->library('upload',$config);
            $this->upload->initialize($config2);
            
            $tipe = $this->input->post('tipe');
            $date = $this->input->post('waktuM');
            $waktuM = date('Y-m-d H:i:s', strtotime($date));
            
            $date = $this->input->post('waktuA');
            $waktuA = date('Y-m-d H:i:s', strtotime($date));
            
            if($this->upload->do_upload("file")){
                $gambar=$this->upload->data();
                $insert=array("title" => $this->input->post('judul'),                             
                             "file" => $gambar['file_name'],
                             "tempat" => $this->input->post('value_53'),
                             "start" => $waktuM,
                             "end" => $waktuA,
                             "tipe" => $tipe,
                             "description" => $this->input->post('value_54')
                             
                             );
                $idfile = $this->agenda->edit($agenda_sekarang->id_agenda, $insert);                                                
                //echo $idfile."<br/>";
                if($idfile!=0){                    
                    //cek tipe
                    if($tipe=="dinas"){
                        $user = $this->input->post($name_user);
                        foreach($user as $us){
                            //insert pelaksana
                            $this->agenda->delete_pelaksana($agenda_sekarang->id_agenda);
                            $this->agenda->insert_pelaksana($agenda_sekarang->id_agenda,$us);
                            //add notification to users
                        }
                    }                    
                    $this->session->set_flashdata("success", "Insert Berhasil");
                } else {
                    $this->session->set_flashdata("error", "Insert database gagal");
                }
            } else {
                $insert=array("title" => $this->input->post('judul'),                             
                             "tempat" => $this->input->post('value_53'),
                             "start" => $waktuM,
                             "end" => $waktuA,
                             "tipe" => $tipe,
                             "description" => $this->input->post('value_54')
                             
                             );
                $idfile = $this->agenda->edit($agenda_sekarang->id_agenda, $insert);                                                
                //echo $idfile."<br/>";
                if($idfile!=0){                    
                    //cek tipe
                    if($tipe=="dinas"){
                        $user = $this->input->post($name_user);
                        foreach($user as $us){
                            //insert pelaksana
                            $this->agenda->delete_pelaksana($agenda_sekarang->id_agenda);
                            $this->agenda->insert_pelaksana($agenda_sekarang->id_agenda,$us);
                            //add notification to users
                        }
                    }                    
                    $this->session->set_flashdata("success", "Insert Berhasil");
                } else {
                    $this->session->set_flashdata("error", "Insert database gagal");
                }
                //echo $this->upload->display_errors('<p>', '</p>');
                $this->session->set_flashdata("error", $this->upload->display_errors('<p>', '</p>'));
            }
            
            //redirect('/agendas', 'Location', 303);
            
        }
        
        
        public function editfile($isagenda=0){
            $file_id=$this->input->post('file_id');
            $folder_id = $this->input->post('folder_id');
            $url = $this->input->post('url');
            

            $nama_file = $this->input->post('nama_file');
            $folder = $this->folder->getfolderbyid($folder_id);
            $name_encryp = $this->encrypt->encode($nama_file);
            $this->load->library('image_lib');
            $config['upload_path']='./uploads/files/';					//path gambar
            $config['allowed_types']='PDF|pdf|jpg|jpeg|JPG|JPEG|doc|docx|PNG|png|xls|xlsx';
            $config['max_size']='100000';
            $config['file_name']=$url;                
            $config['max_width']='0';
            $config['max_height']='0';
            $config['remove_spaces']=TRUE;
            $config['overwrite']=TRUE;
            $this->load->library('upload',$config);
            
            $nomor = "";

            if($this->upload->do_upload("file")){
                    $gambar=$this->upload->data();
                    $hasil = json_decode($folder->meta_field, true);
                    $metadata=[];
                    if($hasil!=null){
                        foreach($hasil as $h){
                            $name = str_replace(" ","",$h['name']);
                            $metadata[$name]=$this->input->post($name);
                        }
                    }                    
                    $update=array("nama_files" => $nama_file,                                 
                                 "url" => $gambar['file_name']
                                 );
                    //update base information
                    $idfile = $this->berkasfile->updateberkas($update, $file_id);
                    //update metas
                    //get old evaluator
                    $eval = $this->berkasfile->get_eval_list($file_id);
                    //delete old meta
                    $this->berkasfile->delete_meta($file_id);
                    //insert new meta
                    $meta = $this->folder->get_metas($folder_id);                    
                    foreach($meta as $mt){
                        $value = $this->input->post('value_'.$mt['id']);
                        if(is_array($value)) {
                            //get deleted eval
                            if($mt['type']=='user'){
                                foreach($eval as $ev){
                                    if(!in_array($ev['user_id'],$value)){
                                        $this->berkasfile->remove_eval($file_id,$ev['id']);    
                                    }
                                }
                            }
                            foreach($value as $v){
                                $this->berkasfile->insert_meta($file_id,$mt['id'],$v);    
                                //check if meta type = user
                                if($mt['type']=='user'){
                                    //check if user already in eval list
                                    if(!($this->berkasfile->is_eval($file_id,$v))) {
                                        //insert new eval
                                        $this->berkasfile->insert_eval($file_id,$v);    
                                    }
                                }
                            }
                        } else {
                            if(strcasecmp('nomor',$mt['nama'])==0){
                                $nomor = $value;
                            } else {
                                if(strpos($mt['nama'],'nomor') !== false && $nomor != "") {
                                    $nomor = $value;
                                }
                            }
                            $this->berkasfile->insert_meta($file_id,$mt['id'],$value);
                        }
                    }
                    /*
                    $metas = $this->berkasfile->get_meta_data($file_id);
                    foreach($metas as $mt){
                        $value = $this->input->post('value_'.$mt['id']);
                        $this->berkasfile->update_meta($mt['id'],$value);
                    } */                    
                } else {
                    //$gambar=$this->upload->data();
                    $hasil = json_decode($folder->meta_field, true);
                    $metadata=[];
                    if($hasil!=null){
                        foreach($hasil as $h){
                            $name = str_replace(" ","",$h['name']);
                            $metadata[$name]=$this->input->post($name);
                        }
                    }
                    $meta=json_encode($metadata);
                    $update=array("nama_files" => $nama_file,
                                 "metadata" => $meta
                                 );
                    //print_r($update);
                    $idfile = $this->berkasfile->updateberkas($update, $file_id);
                    //update metas
                    //get old evaluator
                    $eval = $this->berkasfile->get_eval_list($file_id);
                    //delete old meta
                    $this->berkasfile->delete_meta($file_id);
                    //insert new meta
                    $meta = $this->folder->get_metas($folder_id);                    
                    foreach($meta as $mt){
                        $value = $this->input->post('value_'.$mt['id']);
                        if(is_array($value)) {
                            //get deleted eval
                            if($mt['type']=='user'){
                                foreach($eval as $ev){
                                    if(!in_array($ev['user_id'],$value)){
                                        $this->berkasfile->remove_eval($file_id,$ev['id']);    
                                    }
                                }
                            }
                            foreach($value as $v){
                                $this->berkasfile->insert_meta($file_id,$mt['id'],$v);    
                                //check if meta type = user
                                if($mt['type']=='user'){
                                    //check if user already in eval list
                                    if(!($this->berkasfile->is_eval($file_id,$v))) {
                                        //insert new eval
                                        $this->berkasfile->insert_eval($file_id,$v);    
                                    }
                                }
                            }
                        } else {
                            if(strcasecmp('nomor',$mt['nama'])==0){
                                $nomor = $value;
                            } else {
                                if(strpos($mt['nama'],'nomor') !== false && $nomor != "") {
                                    $nomor = $value;
                                }
                            }
                            $this->berkasfile->insert_meta($file_id,$mt['id'],$value);
                        }
                    }
                    /*
                    $metas = $this->berkasfile->get_meta_data($file_id);
                    foreach($metas as $mt){
                        $value = $this->input->post('value_'.$mt['id']);
                        $this->berkasfile->update_meta($mt['id'],$value);
                    }*/
                }
            //add logs
            if($nomor != "") {
                $this->log->insert($this->session->userdata('id_user'),'Mengedit File dengan Nama <b>'
                                       .$nama_file.'</b> dan Nomor Surat <b>'.$nomor.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
            } else {
                $this->log->insert($this->session->userdata('id_user'),'Mengedit File dengan Nama <b>'
                                       .$nama_file.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
            }
            if($id_folder==13 || $isagenda==1 ){
                //echo $kembalian['status']=$idakses;
                if($idakses!=0){
                    //echo "masuk";
                    $start_lama = $this->input->post("start_lama");
                    $end_lama = $this->input->post("end_lama");
                    $tipe_surat = $this->input->post("tipe");
                    
                    $agenda_sekarang = $this->agenda->getbytanggal($start_lama, $end_lama, $tipe_surat);
                    if($agenda_sekarang!=null){
                         $this->editfileagenda($name_user, $agenda_sekarang);
                    } else {
                         $this->addfileagenda($name_user);
                    }
                    //$this->agenda->edit($agenda_sekarang->id_agenda, $array);
                   
                }
                redirect('/filemanagers/index/'.$folder_id, 'Location', 303);
                //echo json_encode($kembalian);
            } else {
                redirect('/filemanagers/index/'.$folder_id, 'Location', 303);
            }
            
            
        }
        
        function delete_file($id){
            $data=$this->berkasfile->getfilebyid($id);
            if($data!=null){
                $folder = $this->folder->getfolderbyid($data->folder_id);
                $metas = $this->berkasfile->get_meta_data($id);
                $this->berkasfile->delete_meta($id);
                $this->berkasfile->delete_file_eval($id);
                $this->berkasfile->delete($id);
                $file_name="./uploads/files/".$data->url;
                if(file_exists($file_name)){
                    unlink($file_name);
                }
                
                $nomor = "";
                foreach($metas as $mt){
                    if(strcasecmp('nomor',$mt['nama'])==0){
                         $nomor = $mt['value'];
                    } else {
                        if(strpos($mt['nama'],'nomor') !== false && $nomor != "") {
                            $nomor = $mt['value'];
                        }
                    }
                }
                
                //add logs
                if($nomor != "") {
                    $this->log->insert($this->session->userdata('id_user'),'Menghapus File dengan Nama <b>'
                                       .$nama_file.'</b> dan Nomor Surat <b>'.$nomor.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
                } else {
                    $this->log->insert($this->session->userdata('id_user'),'Menghapus File dengan Nama <b>'
                                       .$data->nama.'</b> Pada Folder <b>'.$folder->nama_folder.'</b>',1);
                }
            }
            redirect('/filemanagers/index/'.$folder->id, 'Location', 303);
        }
        
        function profile(){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Tata Naskah - Profile User';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['sidebar'] = "profile";
                $parser['view'] = 'user/profile_user';
                //$parser['current'] = $current;
                $parser['data'] = $this->user->getdatabyid($this->session->userdata('id_user'));
                //$parser['folder'] =  $this->folder->getroot($this->session->userdata('id_user'));
                //$parser['file'] = $this->berkasfile->search($this->session->userdata('id_user'),"");
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        
    }
?>