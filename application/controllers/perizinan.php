<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class Perizinan extends CI_Controller {
       public function __construct(){
            parent::__construct();
            //$this->load->model('organisasi');		
            $this->load->model('agenda');
            $this->load->model('user');
            $this->load->model('folder');
            $this->load->model('integrasi');
            $this->load->library('encrypt');
            if($this->session->userdata('id_user')==null){
                redirect('/', 'Location', 303);
            }
        }
        
        public function index(){
           if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Data Perizinan';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = true;
                $parser['view'] = 'perizinan/index';
                $parser['data'] = $this->integrasi->get_badan_usaha();
                
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function detail($id){
            if($this->session->userdata('id_user')!=null) {
                $data = $this->integrasi->get_id($id);
                if($data != null) {
                    $parser['title'] = 'Data Perizinan';
                    $parser['issearch'] = false;
                    $parser['isfolder'] = false;
                    $parser['isnotif'] = false;
                    $parser['isintegrasi'] = true;
                    $parser['view'] = 'perizinan/detail';
                    $parser['data'] = $data;
                    $parser['revisi'] = $this->integrasi->get_revisi($id);

                    $this->load->view('layout/user',$parser);
                } else {
                    show_404();
                }
            } else {
                redirect(base_url());
            }
        }
        
        public function add_revisi($id){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Data Perizinan';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = true;
                $parser['view'] = 'perizinan/revisi';
                $parser['id'] = $id;
                
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function do_revisi(){
            if($this->session->userdata('id_user')!=null) {
                date_default_timezone_set('Asia/Jakarta');
                
                $id = $this->input->post("id");
                $tgl_masuk = date('Y-m-d H:i:s', strtotime($this->input->post('masuk')));
                $tgl_keluar = date('Y-m-d H:i:s', strtotime($this->input->post('keluar')));
                $keterangan = $this->input->post('keterangan');
                
                $this->integrasi->add_revisi($id,$tgl_masuk,$tgl_keluar,$keterangan);
                
                redirect(base_url()."index.php/perizinan/detail/".$id);
            } else {
                redirect(base_url());
            }
        }
        
        public function edit_revisi($id){
            if($this->session->userdata('id_user')!=null) {
                $data = $this->integrasi->detail_revisi($id);
                if($data != null){
                    $parser['title'] = 'Data Perizinan';
                    $parser['issearch'] = false;
                    $parser['isfolder'] = false;
                    $parser['isnotif'] = false;
                    $parser['isintegrasi'] = true;
                    $parser['data'] = $data;
                    $parser['view'] = 'perizinan/edit_revisi';
                    $parser['id'] = $id;

                    $this->load->view('layout/user',$parser);
                } else {
                    show_404();
                }
            } else {
                redirect(base_url());
            }
        }
        
        public function do_edit_revisi(){
            if($this->session->userdata('id_user')!=null) {
                date_default_timezone_set('Asia/Jakarta');
                
                $id = $this->input->post("id");
                $tgl_masuk = date('Y-m-d H:i:s', strtotime($this->input->post('masuk')));
                $tgl_keluar = date('Y-m-d H:i:s', strtotime($this->input->post('keluar')));
                $keterangan = $this->input->post('keterangan');
                
                $this->integrasi->edit_revisi($id,$tgl_masuk,$tgl_keluar,$keterangan);
                
                $data = $this->integrasi->detail_revisi($id);
                
                redirect(base_url()."index.php/perizinan/detail/".$data['summary_id']);
            } else {
                redirect(base_url());
            }
        }
        
        public function delete_revisi($id){
            if($this->session->userdata('id_user')!=null) {
                $data = $this->integrasi->detail_revisi($id);
                if($data != null){
                    $parser['title'] = 'Data Perizinan';
                    $parser['issearch'] = false;
                    $parser['isfolder'] = false;
                    $parser['isnotif'] = false;
                    $parser['isintegrasi'] = true;
                    $parser['data'] = $data;
                    $parser['view'] = 'perizinan/delete_revisi';
                    $parser['id'] = $id;

                    $this->load->view('layout/user',$parser);
                }
            }
        }
        
        public function do_delete_revisi($id){
            if($this->session->userdata('id_user')!=null) {
                $data = $this->integrasi->detail_revisi($id);
                $this->integrasi->delete_revisi($id);
                redirect(base_url()."index.php/perizinan/detail/".$data['summary_id']);
            } else {
                redirect(base_url());
            }
                
        }
        
        public function export_csv(){
            if($this->session->userdata('id_user')!=null) {
                date_default_timezone_set('Asia/Jakarta');
            
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=export_summary_perizinan.csv');

                $output = fopen('php://output', 'w');
                $summary = $this->integrasi->get_all();
                
                $header = [ "Nomor","Nama Badan Usaha","Alamat Badan Usaha","No. Telepon","No. Fax", "Tanggal Pengajuan",
                              "Bahan Bakar","Media Pengangkutan","Wilayah Operasi","Kasubdit Penyetuju","Waktu Persetujuan",
                              "Kasie Pendistribusi","Waktu Distribusi","Staff Verfikator","Waktu Verfikasi","No. Kode Izin Usaha",
                              "No. Izin Usaha dari DJM","Tanggal Penerbitan","Tanggal Mulai","Tanggal Berakhir" ];

                //echo json_encode($header);

                fputcsv($output, $header);
                $nomor = 0;
                foreach($summary as $data){

                    $row = [ ++$nomor, $data['nama_badan_usaha'], $data['alamat_badan_usaha'], $data['telp'], $data['fax'],
                               date('d-M-Y', strtotime($data['tgl_pengajuan'])), $data['bahan_bakar'], $data['media_pengangkutan'],
                               $data['wilayah_operasi'], $data['penyetuju'], date('d-M-Y H:i:s', strtotime($data['waktu_persetujuan'])),
                               $data['pendistribusi'], date('d-M-Y H:i:s', strtotime($data['waktu_distribusi'])), $data['verifikator'],
                               date('d-M-Y H:i:s', strtotime($data['waktu_verifikasi'])), $data['no_izin_usaha'], $data['izin_usaha_djm'],
                               date('d-M-Y', strtotime($data['tgl_terbit'])), date('d-M-Y', strtotime($data['tgl_mulai'])),
                               date('d-M-Y', strtotime($data['tgl_akhir'])) ];
                    //echo json_encode($row);
                    fputcsv($output, $row);
                }
            } else {
                redirect(base_url());
            }
        }
    }
?>