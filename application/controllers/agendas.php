<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class Agendas extends CI_Controller {
       public function __construct(){
            parent::__construct();
            //$this->load->model('organisasi');		
            $this->load->model('agenda');
           $this->load->model('user');
            $this->load->model('folder');
            $this->load->library('encrypt');
            if($this->session->userdata('id_user')==null){
                redirect('/', 'Location', 303);
            }
        }
        
        public function index(){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Agenda';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['view'] = 'agenda/index';
                $parser['active'] = 'calendar';
                $parser['user'] = $this->user->get();
                
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function lists(){
            if($this->session->userdata('id_user')!=null) {
                $parser['title'] = 'Agenda';
                $parser['issearch'] = false;
                $parser['isfolder'] = false;
                $parser['isnotif'] = false;
                $parser['isintegrasi'] = false;
                $parser['all_agenda']  = $this->agenda->get_all();
                $parser['weeks']  = $this->agenda->get_this_week();
                $parser['months'] = $this->agenda->get_this_month();
                $parser['view'] = 'agenda/list';
                $parser['active'] = 'list';
                $parser['user'] = $this->user->get();
                
                $this->load->view('layout/user',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function view($id){
            if($this->session->userdata('id_user')!=null) {
                $agenda = $this->agenda->get_detail($id);
                if($agenda!=null){
                    $parser['title'] = 'Agenda';
                    $parser['issearch'] = false;
                    $parser['isfolder'] = false;
                    $parser['isnotif'] = false;
                    
                    $parser['agenda']  = $agenda;                    
                    $parser['pelaku'] = $this->agenda->get_user_agenda($id);
                    $parser['view'] = 'agenda/view';
                    $parser['active'] = 'list';                    

                    $this->load->view('layout/user',$parser);
                }
            }
        }
        
        public function delete($id){
            if($this->session->userdata('id_user')!=null) {
                $agenda = $this->agenda->get_detail($id);
                if($agenda!=null){
                    $parser['title'] = 'Agenda';
                    $parser['issearch'] = false;
                    $parser['isfolder'] = false;
                    $parser['isnotif'] = false;
                    
                    $parser['agenda']  = $agenda;                    
                    $parser['pelaku'] = $this->agenda->get_user_agenda($id);
                    $parser['view'] = 'agenda/delete';
                    $parser['active'] = 'list';                    

                    $this->load->view('layout/user',$parser);
                }
            }
        }
        
        public function do_delete($id){
            if($this->session->userdata('id_user')!=null) {
                $agenda = $this->agenda->get_detail($id);
                if($agenda!=null){
                    $this->agenda->delete($id);
                    unlink('./uploads/agenda/'.$agenda['file']);
                    redirect(base_url().'index.php/agendas/');
                }
            }
        }
        
        public function addfile(){
            date_default_timezone_set('Asia/Jakarta');
            
            $name_encryp = $this->encrypt->encode(strtotime("now"));            
            $this->load->library('image_lib');
            $config['upload_path']='./uploads/agenda/';
            $config['allowed_types']='PDF|pdf|jpg';
            $config['max_size']='4096';
            $config['file_name']=$name_encryp;          
            $config['max_width']='0';
            $config['max_height']='0';
            $config['remove_spaces']=TRUE;
            $config['overwrite']=FALSE;
            $this->load->library('upload',$config);
            
            $tipe = $this->input->post('tipe');
            $date = $this->input->post('waktuM');
            $waktuM = date('Y-m-d H:i:s', strtotime($date));
            
            $date = $this->input->post('waktuA');
            $waktuA = date('Y-m-d H:i:s', strtotime($date));
            
            if($this->upload->do_upload("file")){
                $gambar=$this->upload->data();
                $insert=array("title" => $this->input->post('judul'),                             
                             "file" => $gambar['file_name'],
                             "tempat" => $this->input->post('tempat'),
                             "start" => $waktuM,
                             "end" => $waktuA,
                             "tipe" => $tipe,
                             "description" => $this->input->post('isi')
                             
                             );
                $idfile = $this->agenda->insert($insert);                                                
                //echo $idfile."<br/>";
                if($idfile!=0){                    
                    //cek tipe
                    if($tipe=="dinas"){
                        $user = $this->input->post("pelaksana");
                        foreach($user as $us){
                            //insert pelaksana
                            $this->agenda->insert_pelaksana($idfile,$us);
                            //add notification to users
                            
                        }
                    }                    
                    $this->session->set_flashdata("success", "Insert Berhasil");
                } else {
                    $this->session->set_flashdata("error", "Insert database gagal");
                }
            } else {
                $error = $this->upload->display_errors('<p>', '</p>');
                foreach($error as $er){
                    echo $er;
                }
                //$this->session->set_flashdata("error", $this->upload->display_errors('<p>', '</p>'));
            }
            
            redirect('/agendas', 'Location', 303);
            
        }

        public function getallagenda()
        {
            date_default_timezone_set('Asia/Jakarta');
            $week['Mon'] = 'Senin';
            $week['Tue'] = 'Selasa';
            $week['Wed'] = 'Rabu';
            $week['Thu'] = 'Kamis';
            $week['Fri'] = 'Jumat';
            $week['Sat'] = 'Sabtu';
            $week['Sun'] = 'Minggu';
            
            $bulan = ['January','February','Maret','April','Mei','Juni','Juli','Agustus','Septemper','Oktober','November','Desember'];
            
            $hasil = $this->agenda->get();
            $agenda = array();
            foreach($hasil as $h){                
                $temp['title'] = $h['title'];
                $temp['tempat'] = $h['tempat'];
                $temp['allDay'] = false;
                
                $d = date('D', strtotime($h['start']));
                $hari = $week[$d];
                $b = intval(date('m', strtotime($h['start'])));
                $month = $bulan[$b-1];
                $temp['mulai'] = $hari.", ".date('d', strtotime($h['start']))." ".$month." ".date('Y H:i:s', strtotime($h['start']));
                
                $temp['start'] = $h['start'];                
                
                $d = date('D', strtotime($h['end']));
                $hari = $week[$d];
                $b = intval(date('m', strtotime($h['end'])));
                $month = $bulan[$b-1];
                $temp['akhir'] = $hari.", ".date('d', strtotime($h['end']))." ".$month." ".date('Y H:i:s', strtotime($h['end']));
                $temp['end'] = $h['end'];
                
                $temp['description'] = $h['description'];                
                $temp['file'] = $h['file'];
                $agenda[sizeof($agenda)] = $temp;
            }
            echo json_encode($agenda);
        }
        
        public function check_overlap_dinas(){
            date_default_timezone_set('Asia/Jakarta');
            
            $awal = date('Y-m-d', strtotime($this->input->post("awal")));
            $akhir = date('Y-m-d', strtotime($this->input->post("akhir")));
            $pelaksana = $this->input->post("user");
            
            
            $is_conflict = array();
            foreach($pelaksana as $pl) {
                if($this->agenda->check_overlap($pl,$awal,$akhir)){
                    $is_conflict[sizeof($is_conflict)] = $pl;
                }                
            }
            
            echo json_encode($is_conflict);
        }
        
        public function check_overlap_dinas_edit(){
            date_default_timezone_set('Asia/Jakarta');
            
            $awal = date('Y-m-d', strtotime($this->input->post("awal")));
            $akhir = date('Y-m-d', strtotime($this->input->post("akhir")));
            $pelaksana = $this->input->post("user");
            
            $awal_lama = date('Y-m-d H:i:s', strtotime($this->input->post("start_lama")));
            $akhir_lama = date('Y-m-d H:i:s', strtotime($this->input->post("end_lama")));
            
            $dt = $this->agenda->getbytanggal($awal_lama, $akhir_lama, "dinas");
            if($dt!=null){
                $is_conflict = array();
                foreach($pelaksana as $pl) {
                    if($this->agenda->check_overlap_edit($pl,$awal,$akhir, $dt->id_agenda)){
                        $is_conflict[sizeof($is_conflict)] = $pl;
                    }                
                }

                echo json_encode($is_conflict);
            } else {
                $this->check_overlap_dinas();
            }
            
        }
        
         public function check_overlap_presentasi(){
            date_default_timezone_set('Asia/Jakarta');
            $hari = date('Y-m-d', strtotime($this->input->post("awal")));
            $awal = date('Y-m-d H:i:s', strtotime($this->input->post("awal")));
            $akhir = date('Y-m-d H:i:s', strtotime($this->input->post("akhir")));                        
            if($this->agenda->check_overlap_presentasi($awal,$akhir)){
                echo 'conflict';
            } else if($this->agenda->check_count_presentasi($hari)) {
                echo 'overflow';
            } else {
                echo "not-conflict";
            }
        }
        
        public function check_overlap_presentasi_edit(){
            date_default_timezone_set('Asia/Jakarta');
            $hari = date('Y-m-d', strtotime($this->input->post("awal")));
            $awal = date('Y-m-d H:i:s', strtotime($this->input->post("awal")));
            $akhir = date('Y-m-d H:i:s', strtotime($this->input->post("akhir")));                        
            if($this->agenda->check_overlap_presentasi_edit($awal,$akhir, $id)){
                echo 'conflict';
            } else if($this->agenda->check_count_presentasi($hari)) {
                echo 'overflow';
            } else {
                echo "not-conflict";
            }
        }
        
        public function export_csv($id_user){
            date_default_timezone_set('Asia/Jakarta');
            
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=export_agenda.csv');
            
            $output = fopen('php://output', 'w');
            $agenda = $this->agenda->get_jadwal_user($id_user);
            $header = ["Kegiatan","Waktu Mulai","Waktu Selesai","Tempat","Deskripsi"];
            
            //echo json_encode($header);
            
            fputcsv($output, $header);
            
            foreach($agenda as $ag){
                
                $row = [ $ag['title'], date('d-M-Y H:i:s', strtotime($ag['start'])), 
                             date('d-M-Y H:i:s', strtotime($ag['end'])),$ag['tempat'],$ag['description'] ];
                //echo json_encode($row);
                fputcsv($output, $row);
            }
        }
        
        
    }
?>