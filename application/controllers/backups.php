<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class backups extends CI_Controller {
        public function __construct(){
            parent::__construct();            
            $this->load->model('folder');
            $this->load->model('organisasi');
            $this->load->model('notification');
            $this->load->model('log');
            $this->load->model('berkasfile');
            $this->load->model('meta');
        }  	
        
        public function index(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                $parser['title'] = 'Pemeliharaan';
                $parser['aktif'] = 'backup';
				$parser['view'] = 'backup/index';		
                $parser['log'] = $this->log->get_log_backup();
				$this->load->view('layout/admin',$parser);
            } else {
                redirect(base_url());
            }
        }
        
        public function do_db_backup(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {
                // Load the DB utility class
                $this->load->dbutil();

                // Backup your entire database and assign it to a variable
                $backup =& $this->dbutil->backup(); 

                // Load the file helper and write the file to your server
                $this->load->helper('file');
                write_file('./uploads/backups/ditjen_migas_db.gz', $backup); 
                
                $this->log->insert_log_backup($this->session->userdata('id_user'),"Mengunduh Backup Database Aplikasi Sistem Pengangkutan Migas");

                // Load the download helper and send the file to your desktop
                $this->load->helper('download');
                force_download('ditjen_migas_db.gz', $backup);
                                
                redirect(base_url()."index.php/backups/");
            }
        }
        
        public function do_file_backup(){
             if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {                
                 
                // Get real path for our folder
                $rootPath = realpath('./uploads/files/'); 
                // Initialize archive object
                $zip = new ZipArchive;
                unlink('./uploads/backups/ditjen_migas_files.zip');
                $zip->open('./uploads/backups/ditjen_migas_files.zip', ZipArchive::CREATE);
                
                // Create recursive directory iterator
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $name => $file) {
                    // Get real path for current file
                    $filePath = $file->getRealPath();

                    // Add current file to archive
                    $zip->addFile($filePath);                   
                }

                // close the zip file
                if (!$zip->close()) {
                    echo '<p>There was a problem writing the ZIP archive.</p>';                    
                } else {
                    echo '<p>Successfully created the ZIP Archive!</p>';
                    $this->log->insert_log_backup($this->session->userdata('id_user'),"Mengunduh Backup File Surat-Surat 
                                                Aplikasi Sistem Pengangkutan Migas");
                     // Load the download helper and send the file to your desktop
                    $this->load->helper('download');

                    $data = file_get_contents("./uploads/backups/ditjen_migas_files.zip"); // Read the file's contents
                    $name = 'ditjen_migas_files.zip';

                    force_download($name, $data);                

                    redirect(base_url()."index.php/backups/");
                }                                                                 
             }
        }
        
        public function do_app_backup(){
            if($this->session->userdata('id_user')!=null && $this->session->userdata('role')==0) {                
                 
                // Get real path for our folder
                $rootPath = realpath('./'); 
                // Initialize archive object
                $zip = new ZipArchive;
                unlink('./uploads/backups/ditjen_migas_app.zip');
                $zip->open('./uploads/backups/ditjen_migas_app.zip', ZipArchive::CREATE);
                
                // Create recursive directory iterator
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $name => $file) {
                    // Get real path for current file
                    $filePath = $file->getRealPath();

                    // Add current file to archive
                    $zip->addFile($filePath);                   
                }

                // close the zip file
                if (!$zip->close()) {
                    echo '<p>There was a problem writing the ZIP archive.</p>';                    
                } else {
                    echo '<p>Successfully created the ZIP Archive!</p>';
                    $this->log->insert_log_backup($this->session->userdata('id_user'),"Mengunduh Backup Aplikasi Sistem Pengangkutan Migas");
                     // Load the download helper and send the file to your desktop
                    $this->load->helper('download');

                    $data = file_get_contents("./uploads/backups/ditjen_migas_app.zip"); // Read the file's contents
                    $name = 'ditjen_migas_app.zip';

                    force_download($name, $data);                

                    redirect(base_url()."index.php/backups/");
                }                                                                 
             }
        }
    }
?>