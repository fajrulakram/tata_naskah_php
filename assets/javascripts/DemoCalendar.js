var map_day = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
var base_url = "http://10.1.8.60/administrasifile/";
(function(namespace, $) {
	"use strict";

	var DemoCalendar = function() {
		// Create reference to this instance
		var o = this;
		// Initialize app when document is ready
		$(document).ready(function() {
			o.initialize();
		});

	};
	var p = DemoCalendar.prototype;

	// =========================================================================
	// INIT
	// =========================================================================

	p.initialize = function() {
		this._enableEvents();

		this._initEventslist();
		this._initCalendar();
		this._displayDate();
	};

	// =========================================================================
	// EVENTS
	// =========================================================================

	// events
	p._enableEvents = function() {
		var o = this;

		$('#calender-prev').on('click', function(e) { o._handleCalendarPrevClick(e); });
		$('#calender-next').on('click', function(e) { o._handleCalendarNextClick(e); });
		$('input[name="calendarMode"]').on('change', function(e) { o._handleCalendarMode(e); });
	};

	// =========================================================================
	// CONTROLBAR
	// =========================================================================

	p._handleCalendarPrevClick = function(e) {
		$('#calendar').fullCalendar('prev');
		this._displayDate();
	};
	p._handleCalendarNextClick = function(e) {
		$('#calendar').fullCalendar('next');
		this._displayDate();
	};
	p._handleCalendarMode = function(e) {
		$('#calendar').fullCalendar('changeView', $(e.currentTarget).val());
	};
	
	p._displayDate = function() {
		var selectedDate = $('#calendar').fullCalendar('getDate');
        var date = new Date();
        var day = map_day[date.getDay()];
        
		$('.selected-day').html(day);
		$('.selected-date').html($.fullCalendar.formatDate(selectedDate, "dd MMMM yyyy"));
		$('.selected-year').html($.fullCalendar.formatDate(selectedDate, "yyyy"));
	};
	
	// =========================================================================
	// TASKLIST
	// =========================================================================

	p._initEventslist = function() {
		if (!$.isFunction($.fn.draggable)) {
			return;
		}
		var o = this;

		$('.list-events .list-group-item').each(function() {
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()) // use the element's text as the event title
			};

			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true, // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
		});
	};

	// =========================================================================
	// CALENDAR
	// =========================================================================

	p._initCalendar = function(e) {
		if (!$.isFunction($.fn.fullCalendar)) {
			return;
		}

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$('#calendar').fullCalendar({
			height: $( window ).height()-170,
			header: false,            
			editable: false,
			droppable: false,
            disableDragging: true,
			drop: function(date, allDay) { // this function is called when something is dropped
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');

				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);

				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.allDay = allDay;
				copiedEventObject.className = 'event-danger';

				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			events: {
            	url: base_url+"/index.php/agendas/getallagenda"
            },
            eventClick: function(calEvent, jsEvent, view) {
		        // alert('Event: ' + calEvent.title);
		        // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
		        // alert('View: ' + view.name);
		        var html="";
		        html = html+'<div class="form-group">';
		        html = html+'<div class="col-md-4">';
		        html = html+'<label class="control-label">Judul</label>';
		        html = html+'</div>';
		        html = html+'<div class="col-md-8">';
		        html = html+'<p>'+calEvent.title+'</p>';
		        html = html+'</div>';
		        html = html+'</div>';
                
                html = html+'<div class="form-group">';
		        html = html+'<div class="col-md-4">';
		        html = html+'<label class="control-label">Tempat</label>';
		        html = html+'</div>';
		        html = html+'<div class="col-md-8">';
		        html = html+'<p>'+calEvent.tempat+'</p>';
		        html = html+'</div>';
		        html = html+'</div>';

		        html = html+'<div class="form-group">';
		        html = html+'<div class="col-md-4">';
		        html = html+'<label class="control-label">Waktu Mulai</label>';
		        html = html+'</div>';
		        html = html+'<div class="col-md-8">';
		        html = html+'<p>'+calEvent.start+'</p>';
		        html = html+'</div>';
		        html = html+'</div>';
		        
		        html = html+'<div class="form-group">';
		        html = html+'<div class="col-md-4">';
		        html = html+'<label class="control-label">Sampai</label>';
		        html = html+'</div>';
		        html = html+'<div class="col-md-8">';
		        html = html+'<p>'+calEvent.end+'</p>';
		        html = html+'</div>';
		        html = html+'</div>';
		        
		        html = html+'<div class="form-group">';
		        html = html+'<div class="col-md-4">';
		        html = html+'<label class="control-label">Deskripsi</label>';
		        html = html+'</div>';
		        html = html+'<div class="col-md-8">';
		        html = html+'<p>'+calEvent.description+'</p>';
		        html = html+'</div>';
		        html = html+'</div>';
                
                $("#preview").html('<div class="modal-dialog" style="width : 900px;">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                '<h4 class="modal-title" id="simpleModalLabel">Preview</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                '<div class="form-horizontal form-banded form-bordered" id="konten_media"></div>'+
                                '<a class="media" href="'+base_url+'/uploads/agenda/'+calEvent.file+'"></a>'+
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                            '</div>'+
                        '</div><!-- /.modal-content -->'+
                    '</div><!-- /.modal-dialog -->');
		        
		        $(".media").attr('href', base_url+'/uploads/agenda/'+calEvent.file);
                $('a.media').media({width:"100%", height:400});
                
		        $('#konten_media').html(html);
		        $("#preview").modal("show");
		        // change the border color just for fun
		        // $(this).css('border-color', 'red');

		    },
			eventRender: function(event, element) {
				element.find('#date-title').html(element.find('span.fc-event-title').text());
			}
		});
	};
	
	// =========================================================================
	namespace.DemoCalendar = new DemoCalendar;
}(this.boostbox, jQuery)); // pass in (namespace, jQuery):
